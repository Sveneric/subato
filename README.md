# subato
Subato is a platform for programming assignments and course management. Additionally it has a subsystem for learning with flashcards.  

## Platform architecture

## Local setup
For running/ building the subato platform the following tools are required:
- [golang](https://golang.org)
- [docker](https://www.docker.com)
- [docker-compose](https://docs.docker.com/compose/)
- [scala](https://www.scala-lang.org)

To setup your project run:
````bash
make install
````
To build simply run: 
````bash
make build
````

To start the platform locally run:
````bash
make run
````

After that subato will be available at [localhost:8080/subato](localhost:8080/subato) 
