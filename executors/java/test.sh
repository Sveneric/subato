#!/usr/bin/env bash

#ant test
java --enable-preview -cp /test/lib/junit-4.12.jar:/test/lib/hamcrest-core-1.3.jar:classes -Dorg.schmant.task.junit4.target=junit_report.xml barrypitman.junitXmlFormatter.Runner  `getTestClassnames.sh`

antReturnCode=$?

if [ $antReturnCode -ne 0 ];then
     exit 1;
else
     exit 0;
fi
