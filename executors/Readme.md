# TestExecutors 
TestExecutors provide a generic interface to run tests in a variety of programming languages.

# Interface Definition
Each TestExecutor consists of 5 shell scripts providing the core functionality.

| Script          | Description                                    |
|-----------------|------------------------------------------------|
| compile.sh      | compiles the test resources                    |
| test.sh         | runs the actual unit tests                     |
| approvalTest.sh | used to run additional tests                   |
| results.sh      | outputs test results in junit xml format       |
| checkstyle.sh   | run style checks and output the results as xml |

# Development
Building a test executor for local usage:
```bash
./build.sh <executor_name> <executor_version>
```
This automatically builds the desired docker container and runs all scripts against files placed under testfiles.

Upload to Gitlab:
```bash
./publish.sh <executor_name> <executor_version>
```