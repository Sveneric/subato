module HaskellUnit where

esc '"' = "&quot;"
esc '\'' = "&apos;"
esc '&' = "&amp;"
esc '<' = "&lt;"
esc '>' = "&gt;"
esc c = [c]

xmlEsc = concat.map esc

runTests tests = do
  putStrLn "<?xml version=\"1.0\" ?>"
  putStrLn "<testsuite>"
  xs <- sequence tests
  putStrLn ("  <results errors=\""++show (length (filter not xs))++"\" tests=\""++show (length xs)++"\" failures=\"0\"/>")
  putStrLn "</testsuite>"

testcase name msg expected actual  =
    if (expected == actual)
    then return True
    else do
          putStrLn ("  <testcase name=\""++xmlEsc name++"\">")
          let failMessage = xmlEsc ("expected:&lt;"++show expected++"&gt; but was:&lt;"++show actual++"&gt;")
          putStrLn ("    <failure message=\""++failMessage++"\" type=\"AssertionFailedError\">")
          putStrLn (msg++": "++failMessage)
          putStrLn "    </failure>"
          putStrLn "  </testcase>"
          return False