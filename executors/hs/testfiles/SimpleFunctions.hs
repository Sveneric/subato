module SimpleFunctions where
import Math.NumberTheory.Primes
import System.Random
import Codec.Wav
import Codec.ByteString.Parser
import Data.Audio

import Data.Int
import Data.Array.Unboxed
import Data.Complex


factorial :: (Num a, Eq a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n-1)
