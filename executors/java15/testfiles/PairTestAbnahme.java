import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PairTestAbnahme {
  @Test
  public void test01() {
    var p = new Pair<>(1,41);
    assertEquals( 42, p.e2().intValue()+p.e1().intValue());
  }
}
