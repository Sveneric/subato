#!/usr/bin/env bash
cd ${1}

echo "building docker image"
docker build -t registry.gitlab.com/sveneric/subato/executor_${1}:${2} .

echo "testing docker image"
docker run --name subato_executor_${1} -d registry.gitlab.com/sveneric/subato/executor_${1}:${2} sleep 1000

docker cp testfiles subato_executor_${1}:/test
docker exec subato_executor_${1}  /bin/bash -c "mv /test/testfiles/* /test/"

docker exec subato_executor_${1}  /bin/bash -c "/usr/bin/compile.sh"

docker exec subato_executor_${1}  /bin/bash -c "/usr/bin/test.sh"

docker exec subato_executor_${1}  /bin/bash -c "/usr/bin/results.sh"

docker exec subato_executor_${1}  /bin/bash -c "/usr/bin/checkstyle.sh"

docker kill subato_executor_${1}
docker rm subato_executor_${1}

echo "You can now use this image locally. Just add a testExecutor with image: registry.gitlab.com/sveneric/subato/executor_${1}:${2}"