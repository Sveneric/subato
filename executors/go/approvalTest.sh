#!/usr/bin/env bash

go test -v 2>&1 | go-junit-report > report.xml
antReturnCode=$?

if [ $antReturnCode -ne 0 ];then
     exit 1;
else
     exit 0;
fi
