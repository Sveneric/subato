#!/usr/bin/env bash

find . -type f -name "*.kt"
kotlinc -include-runtime  -cp /test/lib/junit-4.12.jar:/test/lib/hamcrest-core-1.3.jar -d classes `find . -type f -name "*.kt"`
