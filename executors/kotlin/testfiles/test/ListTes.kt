package name.panitz.util

import  org.junit.Assert;

import org.junit.Test;

class ListTest {
    val xs = Cons(1,Cons(2,Cons(3,Cons(4,Cons(5,Cons(6,Cons(7,Cons(8,Cons(9,Cons(10,Nil))))))))))

    val ys = Cons(10,Cons(9,Cons(8,Cons(7,Cons(6,Cons(5,Cons(4,Cons(3,Cons(2,Cons(1,Nil))))))))))

    val zs = Cons(1, Cons(2, Cons(3, Nil)))
    val us = Cons(4,Cons(5,Cons(6,Cons(7,Cons(8,Cons(9,Cons(10,Nil)))))))

    val bs = Cons("Freunde",Cons("Römer",Cons("Landsleute",Nil)))

    @Test
    fun length1() {
        Assert.assertEquals(2, length(Cons(1,Cons(2,Nil))))
    }
    @Test
    fun length2() {
        Assert.assertEquals(0, length(Nil))
    }

    @Test
    fun length3() {
        Assert.assertEquals(2, fold(Cons(1,Cons(2,Nil)), 0, {x:Int,_:Int -> x+1}))
    }
    @Test
    fun length4() {
        Assert.assertEquals(10, length(xs))
    }

    @Test
    fun sum1() {
        Assert.assertEquals(3, sum(Cons(1,Cons(2,Nil))))
    }
    @Test
    fun sum2() {
        Assert.assertEquals(55, sum(xs))
    }


    @Test
    fun reverse1() {
        Assert.assertEquals(Nil, Nil)
    }

    @Test
    fun reverse2() {
        Assert.assertEquals(Cons(3,Cons(2,Cons(1,Nil))), reverse(zs))
    }
    @Test
    fun reverse3() {
        Assert.assertEquals(xs, reverse(ys))
    }

    @Test
    fun last1() {
        Assert.assertEquals(3, last(zs))
    }
    @Test
    fun last2() {
        Assert.assertEquals(10, last(xs))
    }
    @Test
    fun take1() {
        Assert.assertEquals(Nil, take(xs,0))
    }
    @Test
    fun take2() {
        Assert.assertEquals(xs, take(xs,10000))
    }
    @Test
    fun take3() {
        Assert.assertEquals(zs, take(xs,3))
    }
    @Test
    fun append1() {
        Assert.assertEquals(xs, append(xs,Nil))
    }
    @Test
    fun append2() {
        Assert.assertEquals(xs, append(Nil, xs))
    }
    @Test
    fun append3() {
        Assert.assertEquals(xs, append(zs,us))
    }
    @Test
    fun map1() {
        Assert.assertEquals(Cons(hd=1, tl=Cons(hd=4, tl=Cons(hd=9, tl=Nil))), map(zs,{z->z*z}))
    }

    @Test
    fun map2() {
        Assert.assertEquals(Cons(hd=7, tl=Cons(hd=5, tl=Cons(hd=10, tl=Nil))), map(bs,{z->z.length}))
    }
    @Test
    fun forEach1() {
        var i:Int=0;
        forEach(xs,{_->i++})
        Assert.assertEquals(10,i)
    }
    @Test
    fun filter1() {
        Assert.assertEquals(Cons(hd=3, tl=Cons(hd=6, tl=Cons(hd=9, tl=Nil))), filter(xs,{z->z%3==0}))
    }
    @Test
    fun filter2() {
        Assert.assertEquals(Nil, filter(xs,{_->false}))
    }
    @Test
    fun filter3() {
        Assert.assertEquals(Nil, filter(Nil,{_->true}))
    }
    @Test
    fun concat1() {
        Assert.assertEquals(Cons(hd=1, tl=Cons(hd=2, tl=Cons(hd=3, tl=Cons(hd=10, tl=Cons(hd=9, tl=Cons(hd=8, tl=Cons(hd=7, tl=Cons(hd=6, tl=Cons(hd=5, tl=Cons(hd=4, tl=Cons(hd=3, tl=Cons(hd=2, tl=Cons(hd=1, tl=Cons(hd=1, tl=Cons(hd=2, tl=Cons(hd=3, tl=Cons(hd=4, tl=Cons(hd=5, tl=Cons(hd=6, tl=Cons(hd=7, tl=Cons(hd=8, tl=Cons(hd=9, tl=Cons(hd=10, tl=Nil)))))))))))))))))))))))
        , concat(Cons(xs,Cons(ys,Cons(Nil,Cons(zs,Cons(Nil,Nil)))))))
    }
}