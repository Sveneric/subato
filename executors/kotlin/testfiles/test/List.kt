package name.panitz.util

sealed class List<out T>
object Nil: List<Nothing>(){
  override fun toString(): String ="Nil"
}
data class Cons<out T>(val hd:T,val tl:List<T>):List<T>()

fun <A> isEmpty(xs:List<A>):Boolean=xs is Nil

fun <A> length(xs:List<A>):Int =
  when (xs){
    is Nil -> 0
    is Cons -> 1+length(xs.tl)
  }

fun <A> last(xs:List<A>):A =
  when (xs){
    is Cons -> when (xs.tl) {
        is Nil -> xs.hd
        is Cons -> last(xs.tl)
      }
    is Nil -> throw Exception()
  }

fun <A> append(xs:List<A>, ys:List<A>):List<A> =
  when (xs){
    is Cons -> Cons(xs.hd,append(xs.tl,ys))
    is Nil -> ys
  }

fun <A> take(xs:List<A>,n:Int):List<A> =
  when (xs){
    is Nil -> Nil
    is Cons ->
        if (n<=0) Nil
        else Cons(xs.hd,take(xs.tl,n-1))
  }

fun <A,B> map(xs:List<A>,f:(A)->B):List<B> =
  when (xs){
    is Nil -> Nil
    is Cons -> Cons(f(xs.hd),map(xs.tl,f))
  }

fun <A> forEach(xs:List<A>,f:(A)->Unit):Unit =
  when (xs){
    is Cons -> {f(xs.hd);forEach(xs.tl,f)}
    is Nil ->{}
  }

fun <A> filter(xs:List<A>,p:(A)->Boolean):List<A> =
  when (xs){
    is Nil -> Nil
    is Cons ->
        if (p(xs.hd)) Cons(xs.hd,filter(xs.tl,p))
        else filter(xs.tl,p)
  }

fun <A,B> fold(xs:List<A>,s:B,op:(B,A)->B): B =
  when (xs){
    is Nil -> s
    is Cons -> fold(xs.tl,op(s,xs.hd),op)
  }

fun <A> reverse(xs:List<A>):List<A> =
  fold(xs,Nil,{ys:List<A>,y:A -> Cons(y,ys)})

fun <A> concat(xss:List<List<A>>):List<A> =
  fold(xss,Nil,{xs:List<A>,ys:List<A>->append(ys,xs)})

fun sum(xs:List<Int>):Int =
  fold(xs,0,{x,y->x+y})

