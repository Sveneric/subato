# subato 2019

.PHONY: generateGoRPCStubs
generateGoRPCStubs:
	protoc -I rpc rpc/*.proto --go_out=plugins=grpc:test_executor

.PHONY: build
build:
	cd web && sbt thirdPartyJavascript package

.PHONY: run
run:
	sudo docker-compose up --build
