package name.panitz.subato.model

import net.liftweb.mapper.By
import net.liftweb.common.Box
import mapper.{Attendance => MAttendance}
import java.time.LocalDate
import java.time.LocalDateTime

trait Attendance extends Id {
  def uid:String
  def date:LocalDate
  def coursePage:Box[CoursePage]
}

trait Attendances extends Finder[Attendance] {
  def findByCoursePageAndUser(coursePage:CoursePage,uid:String):List[Attendance]
  def createAndSave(uid:String, cp:CoursePage ):Attendance
}


class MapperAttendance(private val mg:MAttendance) extends Attendance with MapperModelDependencies {
  def id = mg.id.get
  def uid = mg.uid.get
  def coursePage = mg.coursePage.map(new MapperCoursePage(_))

  def date:LocalDate={
    val t = mg.datum.get
    if (t == null) return null
    println("data base:" +t)
    t.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
  }
}

object MapperAttendances extends Attendances with MapperFinder[Attendance, MAttendance] {
  override def wrap(in:MAttendance):Attendance = new MapperAttendance(in)
  override def metaMapper = MAttendance


  override def createAndSave(uid:String, coursePage:CoursePage):Attendance = {
    val mg = new MAttendance().uid(uid).coursePage(coursePage.id).datum(new java.util.Date())
    mg.save()
    val res = new MapperAttendance(mg)
    println("created: "+res.date)
    res
  }

  override def findByCoursePageAndUser(coursePage:CoursePage,uid:String):List[Attendance]=
    MAttendance.findAll(By(MAttendance.coursePage, coursePage.id),By(MAttendance.uid, uid)).map(wrap(_))
  
}

