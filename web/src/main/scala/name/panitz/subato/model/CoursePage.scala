package name.panitz.subato.model
import name.panitz.subato.model.mapper.{ CoursePage => MCP }
import name.panitz.subato.model.mapper.{ Course => MC }
import name.panitz.subato.model.mapper.{ Term => MT }
import name.panitz.subato.model.mapper.{ Group => MG }
import name.panitz.subato.model.{Tutor => Tut}

import net.liftweb.common.Box
import net.liftweb.mapper.By

trait CoursePages extends Finder[CoursePage] {
  def find(course: Long,term: Long) :List[CoursePage] 
  def createAndSave(courseId:Long,termId:Long):CoursePage
}

import scala.xml.Node

trait CoursePage extends IdAndNamed {
  def course: Course
  def term: Term
  def description:Node
  def lecturer: String
  def isPublic: Boolean
  def courseExerciseSheets: List[ExerciseSheet]
  def trainingGroups: List[Group]
  def lectureTimes: List[Group]
  def theLecturers: List[String]
  def theTutors: List[Tut]
  def theFlashcardCourses: List[learnsystem.FlashcardCourse]
  def theResources: List[ResourceFileForCourse]
  def theLessons: List[Lesson]
  def member: List[UserInGroup]
  def isMember: Boolean
  def isLecturerIn: Boolean
  def isTutorIn: Boolean

}

object MapperCoursePages extends CoursePages {
  override def find(id: Long) = MCP.find(id).map(new MapperCoursePage(_))
  override def find(course: Long,term: Long)
  = MCP.findAll(By(MCP.course,course),By(MCP.term,term)).map(new MapperCoursePage(_))

  override def findAll = MCP.findAll.map(new MapperCoursePage(_))

  override def createAndSave(courseId:Long,termId:Long):CoursePage = {
    val mg = new MCP().course(courseId).term(termId)
    mg.save()
    val res = new MapperCoursePage(mg)
    res
  }
}

private[model] class MapperCoursePage(private val c: mapper.CoursePage) extends CoursePage with name.panitz.subato.model.MapperModelDependencies {
  override def id = c.id.get
  override def name = course.name +" ("+term.name+")"
  override def course = MC.find(c.course.get).map(new MapperCourse(_)).openOrThrowException(null)
  override def term = MT.find(c.term.get).map(new MapperTerm(_)).openOrThrowException(null)
  override def description = c.description.asHtml
  override def courseExerciseSheets()
   = MC.find(c.course.get).map(_.exerciseSheets(c.term.get).map(new MapperExerciseSheet(_))).openOrThrowException(null)
  override def trainingGroups = groups.findByCourseAndTerm(c.course.get,c.term.get).filter(!_.isLecture)
  override def lectureTimes = groups.findByCourseAndTerm(c.course.get,c.term.get).filter(_.isLecture)
  override def theTutors: List[Tut] = tutors.findByCourseAndTerm(c.course.get,c.term.get)
  override def theFlashcardCourses
  = slsCourseInCourseTerms.findByCourseAndTerm(c.course.get,c.term.get)
    .map(_.flashcardCourse.openOrThrowException(""))
    .filter(_.isPublic_?)
  override def theResources: List[ResourceFileForCourse]
   = resourceFileForCourses.findAll(c.id.get)
  def theLessons: List[Lesson]=lessons.findByCoursePage(id)

  override def isPublic =  c.isPublic.get
  override def lecturer = c.lecturer.get

  override def member: List[UserInGroup] = trainingGroups.map(_.member).flatten
  override def isMember = member.map(_.name).contains(currentUser.uid.openOr(""))
  override def isLecturerIn = currentUser.isLecturerIn(course.id,term.id)
  override def theLecturers: List[String]=trainingGroups.map(_.lecturer).filter(_!="").distinct
  override def isTutorIn = currentUser.isTutorIn(course.id,term.id)

}
