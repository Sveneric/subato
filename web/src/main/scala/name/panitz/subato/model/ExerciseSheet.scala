package name.panitz.subato.model

import name.panitz.subato.model.mapper.{ Term => MT }
import name.panitz.subato.model.mapper.{ Course => MC }
import name.panitz.subato.model.mapper.{ ExerciseSheet => ME }
import name.panitz.subato.model.mapper.{ ExerciseSheetEntry => MESE }
import name.panitz.subato.model.mapper.{ Granted => MG }
import name.panitz.subato.model.mapper.{ Points => MP }

import net.liftweb.common.Box
import net.liftweb.common.Empty
import net.liftweb.common.Full

import net.liftweb.mapper.By

import java.util.Date

trait ExerciseSheets extends Finder[ExerciseSheet] {
  def createAndSave(name: String): ExerciseSheet
  def createAndSave(name:String,number:Int,isPublic:Boolean,description:String,courseId:Long,termId:Long)
  def findByCourseAndTerm(courseId:Long,termId:Long):List[ExerciseSheet]

  def grant(uid:String,lecturer:String,time:Date, exerciseSheet:Long,courseId:Long,termId:Long)
}

import scala.xml.Node

trait ExerciseSheet extends IdAndNamed {
  def name: String
  def description: Node
  def isPublic: Boolean
  def number: Int
  def reachablePoints: Int
  def course: Course
  def term: Term
  def lastSubmission:java.util.Date
  def sheetExercises: List[ExerciseSheetEntry]
  def isGranted(uid:String):Box[String]
  def getPoints(uid:String):Int
  def grantButton(uid:String):Node
}

object MapperExerciseSheets extends ExerciseSheets {
  override def find(id: Long) = ME.find(id).map(new MapperExerciseSheet(_))
  override def findAll = ME.findAll.map(new MapperExerciseSheet(_))
  override def findByCourseAndTerm(courseId:Long,termId:Long)
    = ME.findAll(By(ME.course, courseId),By(ME.term, termId)).map(new MapperExerciseSheet(_))


  override def createAndSave(name:String) = {
    val mc = new ME().name(name)
    mc.save()
    new MapperExerciseSheet(mc)
  }

  override def createAndSave(name:String,number:Int,isPublic:Boolean,description:String,courseId:Long,termId:Long) = {
    val mc = new ME().name(name).number(number).isPublic(isPublic).description(description).course(courseId).term(termId)
    mc.save()
    new MapperExerciseSheet(mc)
  }

  override def grant(uid:String,lecturer:String,time:Date, exerciseSheet:Long,courseId:Long,termId:Long){
    val gr = new mapper.Granted(uid,lecturer,time,exerciseSheet,courseId,termId)
  }
}

private[model] class MapperExerciseSheet(private val c: ME) extends ExerciseSheet {
  override def id = c.id.get
  override def name = c.name.get
  override def description = c.description.asHtml
  override def isPublic =  c.isPublic.get
  override def number =  c.number.get
  override def reachablePoints =  c.reachablePoints.get
  override def lastSubmission =  c.lastSubmission.get
  override def course = MC.find(c.course.get).map(new MapperCourse(_)).openOrThrowException(null)
  override def term = MT.find(c.term.get).map(new MapperTerm(_)).openOrThrowException(null)
  override def sheetExercises = c.sheetExercises.map(new MapperExerciseSheetEntry(_))


  override def grantButton(uid:String):Node={
    var termId = c.term.get
    <span>
    <style>.button {{
      font: bold 11px Arial;
      text-decoration: none;
      background-color: #EEEEEE;
      color: #333333;
      padding: 2px 6px 2px 6px;
      border-top: 1px solid #CCCCCC;
      border-right: 1px solid #333333;
      border-bottom: 1px solid #333333;
      border-left: 1px solid #CCCCCC;
    }}
    </style>
    <script>
       function ajaxcall{uid}{id}(element) {{
         var xhttp = new XMLHttpRequest();
         xhttp.onreadystatechange = function() {{
           if (this.readyState == 4 <![CDATA[&&]]> this.status == 200) {{
             var el = document.createElement( "html" );
             el.innerHTML=this.responseText;
             element.innerText=el.textContent;
         
           }}
         }};
         xhttp.open("GET", "{"/subato/GrantExerciseSheetForStudent/" +id+"/"+termId  + "/" + uid}", true);
         xhttp.send();
      }}</script>
      <span onclick={"ajaxcall"+uid+id+"(this)"} class="button">{isGranted(uid).openOr("grant "+uid)}</span>
    </span>
  }


  override def isGranted(uid:String):Box[String]={
    val grants = MG.findAll(By(MG.uid, uid),By(MG.term, term.id),By(MG.exerciseSheet,id))
    if (grants.isEmpty) Empty
    else {
      val grant = grants(0)
      Full(grant.lecturer.get+ " ("+grant.datetime.get+")")
    }
  }
  override def getPoints(uid:String):Int={
    val grants = MP.findAll(By(MP.uid, uid),By(MP.term, term.id),By(MP.exerciseSheet,id))
    if (grants.isEmpty) 0
    else {
      val grant = grants(0)
      grant.points.get
    }
  }

}


trait ExerciseSheetEntry extends Id {
  def sheet: ExerciseSheet
  def exercise: Exercise
  def number: Int
}

class MapperExerciseSheetEntry(private val c: mapper.ExerciseSheetEntry) extends ExerciseSheetEntry {
  override def id = c.id.get
  override def number = c.number.get
  override def sheet = new MapperExerciseSheet(c.exerciseSheet.obj.openOrThrowException(null))
  override def exercise = new MapperExercise(c.exercise.obj.openOrThrowException(null))
}

trait ExerciseSheetEntries extends Finder[ExerciseSheetEntry] {
  def createAndSave(sheetId:Long, exId:Long,n:Int): ExerciseSheetEntry
}


object MapperExerciseSheetEntries extends ExerciseSheetEntries {
  override def find(id: Long) = MESE.find(id).map(new MapperExerciseSheetEntry(_))
  override def findAll = MESE.findAll.map(new MapperExerciseSheetEntry(_))
  override def createAndSave(sheetId:Long, exId:Long,n:Int): ExerciseSheetEntry={
    val mc = new mapper.ExerciseSheetEntry().exerciseSheet(sheetId).exercise(exId).number(n)
    mc.save()
    new MapperExerciseSheetEntry(mc)

  }
}
