package name.panitz.subato.model

import net.liftweb.mapper.By
import net.liftweb.common.Box
import mapper.{SLSCourseInCourseTerm => MSLSCourseInCourseTerm}

trait SLSCourseInCourseTerm extends Id {
//  def name:String
  def course:Box[Course]
  def courseId:Long
  def term:Box[Term]
  def termId:Long
  def flashcardCourse:Box[learnsystem.FlashcardCourse]
  def flashcardCourseId:Long

}

trait SLSCourseInCourseTerms extends Finder[SLSCourseInCourseTerm] {
  def findByCourseAndTerm(course:Course,term:Term):List[SLSCourseInCourseTerm]
  def findByCourseAndTerm(courseId:Long,termId:Long):List[SLSCourseInCourseTerm]
  //def createAndSave(course:Course,term:Term):SLSCourseInCourseTerm
  def createAndSave(flashcardCourseId:Long,courseId:Long,termId:Long):SLSCourseInCourseTerm
}

private class MapperSLSCourseInCourseTerm(private val mg:MSLSCourseInCourseTerm) extends SLSCourseInCourseTerm {
  def id = mg.id.get
//  def name = mg.name.get
  def course = mg.course.obj.map(new MapperCourse(_))
  def courseId = mg.course.get
  def term = mg.term.obj.map(new MapperTerm(_))
  def termId = mg.term.get
  def flashcardCourse:Box[learnsystem.FlashcardCourse]
    = mg.flashcardCourse.obj.map(new learnsystem.MapperFlashcardCourse(_))
  def flashcardCourseId:Long = mg.flashcardCourse.get


  /*  override def toString:String={
    var result = name
    if (weekday!=null) result = result +" "+weekday
    if (time!=null) result = result +" "+time
    if (room!=null) result = result +" "+room
    if (lecturer!=null) result = result +" ("+lecturer+")"
    result
  }*/
/*  override def equals(that:Any) = {
    if (that.isInstanceOf[SLSCourseInCourseTerm]) {
      that.asInstanceOf[SLSCourseInCourseTerm].name == this.name
    } else {
      false
    }
  }*/
}

object MapperSLSCourseInCourseTerms extends SLSCourseInCourseTerms with MapperFinder[SLSCourseInCourseTerm, MSLSCourseInCourseTerm] {


  override def createAndSave(flashcardCourseId:Long,courseId:Long,termId:Long):SLSCourseInCourseTerm = {
    val mg = new MSLSCourseInCourseTerm().course(courseId).term(termId).flashcardCourse(flashcardCourseId)
    mg.save()
    val res = new MapperSLSCourseInCourseTerm(mg)
    res
  }
  override def wrap(in:MSLSCourseInCourseTerm):SLSCourseInCourseTerm = new MapperSLSCourseInCourseTerm(in)
  override def metaMapper = MSLSCourseInCourseTerm

  override def findByCourseAndTerm(course:Course,term:Term)
    = findByCourseAndTerm(course.id,term.id)

  override def findByCourseAndTerm(courseId:Long,termId:Long)
    = MSLSCourseInCourseTerm.findAll(By(MSLSCourseInCourseTerm.course, courseId),By(MSLSCourseInCourseTerm.term, termId)).map(wrap(_))
}
