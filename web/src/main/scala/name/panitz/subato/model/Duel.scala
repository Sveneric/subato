package name.panitz.subato.model
import net.liftweb.common.Box
import _root_.net.liftweb.mapper.By
import _root_.net.liftweb.mapper.NotBy
import net.liftweb.common._
import net.liftweb.util.Helpers._
import net.liftweb.http.SHtml
import scala.xml.Text
import scala.xml.Node

object Progress extends Enumeration {
  type Progress = Value
  val NotPlayed, Correct, Incorrect, PartialCorrect = Value

  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)
}

import Progress._


trait Duel extends IdAndNamed  {
  def save()

  def htmlDescription: Node

  def course:Course
  def cardBox:CardBox

  def player1:String
  def player2:String
  def player2_=(p:String)

  def delete()
  def card1:Card
  def card2:Card
  def card3:Card
  def card4:Card
  def card5:Card
  def card6:Card
  def card7:Card
  def card8:Card
  def card9:Card
  def card10:Card
  def card11:Card
  def card12:Card
  def card13:Card
  def card14:Card
  def card15:Card

  def cards():Vector[Card]
  def cardsSeter:Vector[(Card) => Unit]

  def card1_=(c:Card)
  def card2_=(c:Card)
  def card3_=(c:Card)
  def card4_=(c:Card)
  def card5_=(c:Card)
  def card6_=(c:Card)
  def card7_=(c:Card)
  def card8_=(c:Card)
  def card9_=(c:Card)
  def card10_=(c:Card)
  def card11_=(c:Card)
  def card12_=(c:Card)
  def card13_=(c:Card)
  def card14_=(c:Card)
  def card15_=(c:Card)


  def pl1Card1:Progress
  def pl1Card2:Progress
  def pl1Card3:Progress
  def pl1Card4:Progress
  def pl1Card5:Progress
  def pl1Card6:Progress
  def pl1Card7:Progress
  def pl1Card8:Progress
  def pl1Card9:Progress
  def pl1Card10:Progress
  def pl1Card11:Progress
  def pl1Card12:Progress
  def pl1Card13:Progress
  def pl1Card14:Progress
  def pl1Card15:Progress

  def pl2Card1:Progress
  def pl2Card2:Progress
  def pl2Card3:Progress
  def pl2Card4:Progress
  def pl2Card5:Progress
  def pl2Card6:Progress
  def pl2Card7:Progress
  def pl2Card8:Progress
  def pl2Card9:Progress
  def pl2Card10:Progress
  def pl2Card11:Progress
  def pl2Card12:Progress
  def pl2Card13:Progress
  def pl2Card14:Progress
  def pl2Card15:Progress

  def pl1Card1_=(p:Progress)
  def pl1Card2_=(p:Progress)
  def pl1Card3_=(p:Progress)
  def pl1Card4_=(p:Progress)
  def pl1Card5_=(p:Progress)
  def pl1Card6_=(p:Progress)
  def pl1Card7_=(p:Progress)
  def pl1Card8_=(p:Progress)
  def pl1Card9_=(p:Progress)
  def pl1Card10_=(p:Progress)
  def pl1Card11_=(p:Progress)
  def pl1Card12_=(p:Progress)
  def pl1Card13_=(p:Progress)
  def pl1Card14_=(p:Progress)
  def pl1Card15_=(p:Progress)

  def pl2Card1_=(p:Progress)
  def pl2Card2_=(p:Progress)
  def pl2Card3_=(p:Progress)
  def pl2Card4_=(p:Progress)
  def pl2Card5_=(p:Progress)
  def pl2Card6_=(p:Progress)
  def pl2Card7_=(p:Progress)
  def pl2Card8_=(p:Progress)
  def pl2Card9_=(p:Progress)
  def pl2Card10_=(p:Progress)
  def pl2Card11_=(p:Progress)
  def pl2Card12_=(p:Progress)
  def pl2Card13_=(p:Progress)
  def pl2Card14_=(p:Progress)
  def pl2Card15_=(p:Progress)

  def p1Scores:Vector[Progress]
  def p2Scores:Vector[Progress]
  val pl1Ids:Vector[String]
  val pl2Ids:Vector[String]

  def p1ScoresSetter:Vector[(Progress)=>Unit]
  def p2ScoresSetter:Vector[(Progress)=>Unit]

  def score1:Int
  def score1_=(i:Int)
  def score2:Int
  def score2_=(i:Int)
  def next1:Int
  def next1_=(i:Int)
  def next2:Int
  def next2_=(i:Int)

  def finished:Boolean
  def userCanPlay(uid:String):Boolean

  def nextCard(uid:String):Card
  def lastCard(uid:String):Card

  def renderForUser(uid:String) :net.liftweb.util.CssSel
}

import name.panitz.subato.model.mapper.{Duel => MD}

class MapperDuel(private val d:MD)
    extends Duel
    with  name.panitz.subato.SWrapperL10n{

  override def save() = d.save
  override def delete() = d.delete_!

  override def id = d.id.get
  override def name = ""
  override def course
   = if (cardBox!=null) cardBox.course
     else if (!d.course.obj.isEmpty)
       new MapperCourse(d.course.obj.openOrThrowException(null))
     else null

  override def cardBox
   = if (!d.cardBox.obj.isEmpty)
      new MapperCardBox(d.cardBox.obj.openOrThrowException(null))
     else null

  override def htmlDescription
   = <span>{course.name}&nbsp;<b>{try{cardBox.name}catch {case _:Throwable => ""}}</b></span>

  override def player1 = d.player1.get
  override def player2 = d.player2.get

  override  def player2_=(p:String) = {d.player2.set(p)}
  def getCard(c:Box[mapper.MultipleCard])
    = if (!c.isEmpty) MapperMultipleCards.mapToModel(c.openOrThrowException(null)) else null

  override def card1 = getCard (d.card1.obj)  
  override def card2 =  getCard  (d.card2.obj)
  override def card3 =  getCard  (d.card3.obj) 
  override def card4 =  getCard  (d.card4.obj) 
  override def card5 =  getCard  (d.card5.obj) 
  override def card6 =  getCard  (d.card6.obj) 
  override def card7 =  getCard  (d.card7.obj) 
  override def card8 =  getCard  (d.card8.obj) 
  override def card9 =  getCard  (d.card9.obj) 
  override def card10 =  getCard  (d.card10.obj)
  override def card11 =  getCard  (d.card11.obj)
  override def card12 =  getCard  (d.card12.obj)
  override def card13 =  getCard  (d.card13.obj)
  override def card14 =  getCard  (d.card14.obj)
  override def card15 =  getCard  (d.card15.obj)
  def cards():Vector[Card]=Vector(card1, card2, card3, card4, card5, card6, card7, card8, card9, card10, card11, card12, card13, card14, card15)

  override def card1_=(c:Card) {d.card1.set(c.id)}
  override def card2_=(c:Card) {d.card2.set(c.id)}
  override def card3_=(c:Card) {d.card3.set(c.id)}
  override def card4_=(c:Card) {d.card4.set(c.id)}
  override def card5_=(c:Card) {d.card5.set(c.id)}
  override def card6_=(c:Card) {d.card6.set(c.id)}
  override def card7_=(c:Card) {d.card7.set(c.id)}
  override def card8_=(c:Card) {d.card8.set(c.id)}
  override def card9_=(c:Card) {d.card9.set(c.id)}
  override def card10_=(c:Card) {d.card10.set(c.id)}
  override def card11_=(c:Card) {d.card11.set(c.id)}
  override def card12_=(c:Card) {d.card12.set(c.id)}
  override def card13_=(c:Card) {d.card13.set(c.id)}
  override def card14_=(c:Card) {d.card14.set(c.id)}
  override def card15_=(c:Card) {d.card15.set(c.id)}

  override def cardsSeter=Vector(card1_= , card2_= , card3_= , card4_= , card5_= , card6_= , card7_= , card8_= , card9_= , card10_= , card11_= , card12_= , card13_= , card14_= , card15_= )


  override def pl1Card1 = d.pl1Card1.get
  override def pl1Card2 = d.pl1Card2.get
  override def pl1Card3 = d.pl1Card3.get
  override def pl1Card4 = d.pl1Card4.get
  override def pl1Card5 = d.pl1Card5.get
  override def pl1Card6 = d.pl1Card6.get
  override def pl1Card7 = d.pl1Card7.get
  override def pl1Card8 = d.pl1Card8.get
  override def pl1Card9 = d.pl1Card9.get
  override def pl1Card10 = d.pl1Card10.get
  override def pl1Card11 = d.pl1Card11.get
  override def pl1Card12 = d.pl1Card12.get
  override def pl1Card13 = d.pl1Card13.get
  override def pl1Card14 = d.pl1Card14.get
  override def pl1Card15 = d.pl1Card15.get

  override def p1Scores = Vector(pl1Card1,pl1Card2,pl1Card3,pl1Card4,pl1Card5,pl1Card6,pl1Card7,pl1Card8,pl1Card9,pl1Card10,pl1Card11,pl1Card12,pl1Card13,pl1Card14,pl1Card15)
  override def p2Scores = Vector(pl2Card1,pl2Card2,pl2Card3,pl2Card4,pl2Card5,pl2Card6,pl2Card7,pl2Card8,pl2Card9,pl2Card10,pl2Card11,pl2Card12,pl2Card13,pl2Card14,pl2Card15)

  override def p1ScoresSetter= Vector(pl1Card1_=,pl1Card2_=,pl1Card3_=,pl1Card4_=,pl1Card5_=,pl1Card6_=,pl1Card7_=,pl1Card8_=,pl1Card9_=,pl1Card10_=,pl1Card11_=,pl1Card12_=,pl1Card13_=,pl1Card14_=,pl1Card15_=)
  override def p2ScoresSetter = Vector(pl2Card1_=,pl2Card2_=,pl2Card3_=,pl2Card4_=,pl2Card5_=,pl2Card6_=,pl2Card7_=,pl2Card8_=,pl2Card9_=,pl2Card10_=,pl2Card11_=,pl2Card12_=,pl2Card13_=,pl2Card14_=,pl2Card15_=)

  override def pl2Card1 = d.pl2Card1.get
  override def pl2Card2 = d.pl2Card2.get
  override def pl2Card3 = d.pl2Card3.get
  override def pl2Card4 = d.pl2Card4.get
  override def pl2Card5 = d.pl2Card5.get
  override def pl2Card6 = d.pl2Card6.get
  override def pl2Card7 = d.pl2Card7.get
  override def pl2Card8 = d.pl2Card8.get
  override def pl2Card9 = d.pl2Card9.get
  override def pl2Card10 = d.pl2Card10.get
  override def pl2Card11 = d.pl2Card11.get
  override def pl2Card12 = d.pl2Card12.get
  override def pl2Card13 = d.pl2Card13.get
  override def pl2Card14 = d.pl2Card14.get
  override def pl2Card15 = d.pl2Card15.get

  override def pl1Card1_=(p:Progress) = d.pl1Card1.set(p)
  override def pl1Card2_=(p:Progress) = d.pl1Card2.set(p)
  override def pl1Card3_=(p:Progress) = d.pl1Card3.set(p)
  override def pl1Card4_=(p:Progress) = d.pl1Card4.set(p)
  override def pl1Card5_=(p:Progress) = d.pl1Card5.set(p)
  override def pl1Card6_=(p:Progress) = d.pl1Card6.set(p)
  override def pl1Card7_=(p:Progress) = d.pl1Card7.set(p)
  override def pl1Card8_=(p:Progress) = d.pl1Card8.set(p)
  override def pl1Card9_=(p:Progress) = d.pl1Card9.set(p)
  override def pl1Card10_=(p:Progress) = d.pl1Card10.set(p)
  override def pl1Card11_=(p:Progress) = d.pl1Card11.set(p)
  override def pl1Card12_=(p:Progress) = d.pl1Card12.set(p)
  override def pl1Card13_=(p:Progress) = d.pl1Card13.set(p)
  override def pl1Card14_=(p:Progress) = d.pl1Card14.set(p)
  override def pl1Card15_=(p:Progress) = d.pl1Card15.set(p)


  override def pl2Card1_=(p:Progress) = d.pl2Card1.set(p)
  override def pl2Card2_=(p:Progress) = d.pl2Card2.set(p)
  override def pl2Card3_=(p:Progress) = d.pl2Card3.set(p)
  override def pl2Card4_=(p:Progress) = d.pl2Card4.set(p)
  override def pl2Card5_=(p:Progress) = d.pl2Card5.set(p)
  override def pl2Card6_=(p:Progress) = d.pl2Card6.set(p)
  override def pl2Card7_=(p:Progress) = d.pl2Card7.set(p)
  override def pl2Card8_=(p:Progress) = d.pl2Card8.set(p)
  override def pl2Card9_=(p:Progress) = d.pl2Card9.set(p)
  override def pl2Card10_=(p:Progress) = d.pl2Card10.set(p)
  override def pl2Card11_=(p:Progress) = d.pl2Card11.set(p)
  override def pl2Card12_=(p:Progress) = d.pl2Card12.set(p)
  override def pl2Card13_=(p:Progress) = d.pl2Card13.set(p)
  override def pl2Card14_=(p:Progress) = d.pl2Card14.set(p)
  override def pl2Card15_=(p:Progress) = d.pl2Card15.set(p)

  override val pl1Ids = Vector("b1_1","b1_2","b1_3","b1_4","b1_5","b1_6","b1_7","b1_8","b1_9","b1_10","b1_11","b1_12","b1_13","b1_14","b1_15"  )
  override val pl2Ids = Vector("b2_1","b2_2","b2_3","b2_4","b2_5","b2_6","b2_7","b2_8","b2_9","b2_10","b2_11","b2_12","b2_13","b2_14","b2_15"  )
  

  override def score1=d.score1.get
  override def score2=d.score2.get

  override def next1=d.next1.get
  override def next2=d.next2.get

  override def score1_=(i:Int) {d.score1.set(i)}
  override def score2_=(i:Int) {d.score2.set(i)}

  override def next1_=(i:Int) {d.next1.set(i)}
  override def next2_=(i:Int) {d.next2.set(i)}

  override def finished= next1+next2 >= 2*cards.size

  override def nextCard(uid:String):Card = cards()(if (player1==uid) (next1) else (next2))
  override def lastCard(uid:String):Card = cards()(if (player1==uid) (next1-1) else (next2-1))


  override def userCanPlay(uid:String):Boolean = 
    if (uid == player1){
      next1 < cards.size && next1/3 <= next2/3
    }else if (uid == player2){
      next2 < cards.size && next2/3 <= next1/3
    }else false


  override def renderForUser(uid:String) :net.liftweb.util.CssSel ={
    def getBox(p:Progress, right:Boolean)= {
      val lr = if (right) "right;" else "left;"
      p match {
        case NotPlayed => <span class="greybox" style={"float: "+lr} >&nbsp;</span>
        case Correct => <span class="greenbox"  style={"float: "+lr}>&nbsp;</span>
        case Incorrect => <span class="redbox"  style={"float: "+lr}>&nbsp;</span>
        case PartialCorrect => <span class="yellowbox" style={"float: "+lr}>&nbsp;</span>
      }
    }

    "#playOrWait *" #>
      (if (finished) Text(locStr("finished"))
       else if (userCanPlay(uid)) <a class="pushButton" href={"/PlayDuel/"+id}>{locStr("play")}</a>
       else <span class="dontPushButton">{Text(locStr("waiting"))}</span> )&
    "#player1 *"    #> player1  &
    "#player2 *"    #> player2  &
    "#points1 *"    #> score1  &
    "#points2 *"    #> score2  &
    (pl1Ids.zip(p1Scores).map( (x) => ("#"+x._1+" *") #> getBox(x._2,false) )
        .foldLeft("#course *" #> htmlDescription)((u,v) => u & v)) &
    (pl2Ids.zip(p2Scores).map( (x) => ("#"+x._1+" *") #> getBox(x._2,true) )
        .foldLeft("#course *" #> htmlDescription)((u,v) => u & v)) &
    "#time *" #> <b>{timeSpan}</b>
  }

  def timeSpan = Text(""+net.liftweb.util.TimeHelpers.now)

}


trait Duels extends Finder[Duel] {
  def create(cId:Long,player1:String):Duel
  def create(cId:Long,cardBoxId:Long,player1:String):Duel
  def findAll:List[Duel]
  def findForUser(uid:String):List[Duel]
  def findForCardBox(cid:Long,uid:String):List[Duel]
  def find(id:Long):Box[Duel]
  def find(cid:Long,uid:String):List[Duel]
  def findContainsCard(iId:Long):List[Duel]
}

object MapperDuels extends Duels with MapperModelDependencies {
  override def findAll = MD.findAll.map(new MapperDuel(_))
  override def find(id:Long) = MD.find(id).map(new MapperDuel(_))

  override def find(cid:Long,uid:String) ={
    MD.findAll(By(MD.course,cid),By(MD.player2,""),NotBy(MD.player1,uid)).map(new MapperDuel(_))
  }

  override def findForCardBox(cid:Long,uid:String) ={
    MD.findAll(By(MD.cardBox,cid),By(MD.player2,""),NotBy(MD.player1,uid)).map(new MapperDuel(_)) 
  }

  def findContainsCard(cId:Long):List[Duel] = {
    (MD.findAll(By(MD.card1,cId)):::
      MD.findAll(By(MD.card2,cId)):::
      MD.findAll(By(MD.card3,cId)):::
      MD.findAll(By(MD.card4,cId)):::
      MD.findAll(By(MD.card5,cId)):::
      MD.findAll(By(MD.card6,cId)):::
      MD.findAll(By(MD.card7,cId)):::
      MD.findAll(By(MD.card8,cId)):::
      MD.findAll(By(MD.card9,cId)):::
      MD.findAll(By(MD.card10,cId)):::
      MD.findAll(By(MD.card11,cId)):::
      MD.findAll(By(MD.card12,cId)):::
      MD.findAll(By(MD.card13,cId)):::
      MD.findAll(By(MD.card14,cId)):::
      MD.findAll(By(MD.card15,cId)))
    .map(new MapperDuel(_))
  }

  override def findForUser(uid:String):List[Duel] =
    (MD.findAll(By(MD.player1,uid))++MD.findAll(By(MD.player2,uid)))
      .map(new MapperDuel(_))



  override def create(cId:Long,player1:String):Duel={
    val result = new MapperDuel(new MD(cId,-1,player1))
    var theCards = multipleCards.findDuelForCourse(cId)
    for ( (s,c) <- result.cardsSeter.zip(theCards)){
      s(c)
    }
    result
  }

  override def create(cId:Long,cardBoxId:Long,player1:String):Duel={
    val result = new MapperDuel(new MD(cId,cardBoxId,player1))

    var theCards
    = scala.util.Random.shuffle(
       cardBoxes
       .find(cardBoxId)
       .openOrThrowException(null)
       .cards  
       .filter((c)=> c.isDuelCard)).take(15)
    for ( (s,c) <- result.cardsSeter.zip(theCards)){
      s(c)
    }
    result
  }


}

