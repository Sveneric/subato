package name.panitz.subato.model
import name.panitz.subato.model.mapper.{Survey => MS}
import name.panitz.subato.model.mapper.{SurveyEntry => MSE}
import name.panitz.subato.model.mapper.{SurveyEntryAnswer => MSEA}
import name.panitz.subato._
import net.liftweb.mapper._

trait Surveys extends Finder[Survey] {
  def findAllForCourse(cid:Long):List[Survey]
  def create(cId:Long, name:String):Survey
  def currentActiveSurveysPage:scala.xml.Node
}

object MapperSurveys
    extends Surveys
    with MapperModelDependencies
    with SWrapperL10n{

  override def findAll = MS.findAll.map( new MapperSurvey(_))
  override def find(id:Long) = MS.find(id).map( new MapperSurvey(_))
  override def findAllForCourse(cid:Long)
  = MS.findAll(By(mapper.Survey.course,cid)).map(new MapperSurvey(_))

  override def create(cId:Long, name:String):Survey = new MapperSurvey( new MS(cId,name))
  override def currentActiveSurveysPage:scala.xml.Node={
    val surveys = name.panitz.subato.model.mapper.Survey
        .findAll(By(mapper.Survey.isActive,true))
        .map(new MapperSurvey(_))
    if (surveys.isEmpty) <div></div>
    else
      <div style="background: #CCCCCC;"><br />
        <link href="/static/card.css" rel="stylesheet" />
	<div class="innercard">
          <h3>{locStr("currentSurveys")}</h3>
          <div class="numberlist">
            <ol>
            {surveys.map((b)=> <li><a href={"/Survey/"+b.id}>{b.name+" ("+b.course.name+")"}</a></li>)}
	    </ol>    
          </div>
        </div>
      </div>
  }
}


trait Survey extends IdAndNamed{
  def id:Long
  def name:String
  def course:Course
  def isPrivate:Boolean
  def isActive:Boolean
  def card:Card
  def delete
  def clear
  def isPrivate_=(b:Boolean) 
  def isActive_=(b:Boolean) 
  def card_=(c:Card)
  def save()
  def addAnswer():SurveyAnswer
  def givenAnswers:List[SurveyAnswer]
}

class MapperSurvey(s:MS)
    extends Survey
    with SWrapperL10n
    with MapperModelDependencies{
  override def id = s.id.get
  override def name = s.name.get
  override def course=courses.find(s.course.get).openOrThrowException("")
  override def isPrivate = s.isPrivate.get
  override def isPrivate_= (b:Boolean) {s.isPrivate.set(b)}
  override def isActive = s.isActive.get
  override def isActive_= (b:Boolean) {s.isActive.set(b)}


  override def save() = s.save

  override def card=multipleCards.find(s.multipleCard.get).openOrThrowException("")

  override def card_=(c:Card) ={ s.multipleCard.set(c.id)}

  override def delete={
    val c = card
    givenAnswers.map(x => x.delete)
    s.delete_!
    c.delete
  }

  override def clear={
    givenAnswers.map(x => x.delete)
  }


  override def addAnswer ={
    val result = new MapperSurveyAnswer( new MSE(id))
    result.save
    result
  }
  override def givenAnswers:List[SurveyAnswer] ={
    MSE.findAll(By(MSE.survey,id)).map(new MapperSurveyAnswer(_))
  }
}

trait SurveyAnswer extends Id{
  def id:Long
  def selectedAnswers:List[String]
  def addAnswer(t:String)
  def save
  def delete
}

class MapperSurveyAnswer(s:MSE)
    extends SurveyAnswer
    with SWrapperL10n
    with MapperModelDependencies{

  override def id = s.id.get

  override def save = s.save

  override def selectedAnswers:List[String]=
    MSEA.findAll(By(MSEA.surveyEntry,id)).map((sea) => sea.text.get)

  override def addAnswer(t:String)={
    val a = new MSEA(id,t)
    a.save
  }

  override def delete= {
    val i = id
    MSEA.findAll(By(MSEA.surveyEntry,i)).map(_.delete_!)
    s.delete_!
  }

}
