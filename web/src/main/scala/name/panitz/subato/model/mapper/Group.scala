package name.panitz.subato.model.mapper

import scala.xml._
import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.util.Helpers._

import java.text.DateFormat
import java.util.Date

import net.liftweb.common.{Full, Box}
import net.liftweb.http.{ SHtml }

import java.time.LocalTime

import name.panitz.subato.model.Weekday

private[model] class Group extends LongKeyedMapper[Group] with IdPK {
  def getSingleton = Group

  object name extends MappedPoliteString(this, 1000)
	
  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(Course.findAll.map(x => (x.id.get, x.name.get)))
    override def asHtml() = {
      Text(Course.find(By(Course.id, this.get)) map {_.name.get} openOr "")
    }
  }
  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def validSelectValues = {
     Full(for (term <- Term.findAll())yield {
          (term.id.get, (term.addendum+" "+term.year))
     })
    }
    override def asHtml() = {
      Text(Term.find(By(Term.id, this.get)) map {(t) => t.addendum.get+" "+t.year.get} openOr "")
    }
  }


  object lecturer extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }

  object weekday extends MappedEnum(this, Weekday) {
    override def defaultValue = Weekday.Mo
  }

  object time extends MappedTime(this) {
    override def parse(input: String): Box[java.util.Date] = (tryo { timef.parse(input) }) match {
      case f@Full(_) => f
      case _ => Full(null)
    }
    override def displayName = "time"
    override def displayHtml = Text(displayName)
    override def asHtml = Text(if (this.get != null) timef.format(this.get) else "")
    val timef = new java.text.SimpleDateFormat("HH:mm");

    override def _toForm: Box[Elem] = {
      var timeString="";
      def trySet(){
        try{
          this.setFromAny(timef.parse(timeString))
        }catch{
          case e:Exception  => println(e)
        }
      }
      Full(<span>
        {SHtml.text({if (get==null) "10:15" else timef.format(this.get)}
          , x=> {timeString=x;trySet()}
          , "type" -> "time")
        }
        </span>
      )
    }
  }

  object room extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }

  object isLecture extends MappedBoolean(this) {
    override def defaultValue = false
  }


  def courseName = Course.find(By(Course.id, course.get)) map {_.name.get} openOr ""

}

import net.liftweb.sitemap.Loc.Hidden

object Group extends Group with LongKeyedMetaMapper[Group]
    with LecturerOnlyCRUDify[Long, Group] {
  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams

}
