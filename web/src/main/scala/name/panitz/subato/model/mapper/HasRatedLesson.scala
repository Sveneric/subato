package name.panitz.subato.model.mapper

import scala.xml._
import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.util.Helpers._

import java.text.DateFormat
import java.util.Date
import net.liftweb.common.Box._

import name.panitz.subato.model.MapperCoursePage

import net.liftweb.http.{ SHtml }

private[model] class HasRatedLesson extends LongKeyedMapper[HasRatedLesson] with IdPK {
  def getSingleton = HasRatedLesson

  object lesson extends MappedLongForeignKey(this, Lesson){
    override def dbIndexed_? = true
  }
  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
    override def _toForm = Full(<span>{this.get}</span>    )
  }
}

import net.liftweb.sitemap.Loc.Hidden

object HasRatedLesson extends HasRatedLesson with LongKeyedMetaMapper[HasRatedLesson]
    with LecturerOnlyCRUDify[Long, HasRatedLesson] {
  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams

}
