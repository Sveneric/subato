package name.panitz.subato.model

import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.common.Box

trait Finder[ModelType <: Id] {
  def find(id:Long):Box[ModelType]
  def findAll:List[ModelType]
}

trait MapperFinder[ModelType <: Id, WrappedType <: LongKeyedMapper[WrappedType]] extends Finder[ModelType] {
  def metaMapper:LongKeyedMetaMapper[WrappedType]
  def wrap(in:WrappedType):ModelType
  def find(id:Long):Box[ModelType] = metaMapper.find(id).map(wrap(_))
  def findAll:List[ModelType] = metaMapper.findAll.map(wrap(_))
}
