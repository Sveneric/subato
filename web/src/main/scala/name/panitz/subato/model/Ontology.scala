package name.panitz.subato.model

import mapper.{Ontology => MOntology}

trait Ontology extends IdAndNamed {
  def name:String
 }

trait Ontologys extends Finder[Ontology] {
  def createAndSave(name:String):Ontology
}

private class MapperOntology(private val mg:MOntology) extends Ontology {
  def id = mg.id.get
  def name = mg.name.get
  override def equals(o: Any): Boolean =
    o.isInstanceOf[Ontology] && this.name==o.asInstanceOf[Ontology].name
  override def hashCode: Int = this.name.hashCode
  override def toString()= this.name

}

object MapperOntologys extends Ontologys with MapperFinder[Ontology, MOntology] {
  override def createAndSave(name:String):Ontology = {
    val mg = new MOntology().name(name)
    mg.save()
    val res = new MapperOntology(mg)
    res
  }
  override def wrap(in:MOntology):Ontology = new MapperOntology(in)
  override def metaMapper = MOntology
}
