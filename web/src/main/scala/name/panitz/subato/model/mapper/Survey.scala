package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.http.UnauthorizedResponse
import net.liftweb.mapper.By
import net.liftweb.http.RedirectResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.sitemap.Loc
import net.liftweb.sitemap.Menu
import net.liftweb.sitemap.Loc.Hidden
import net.liftweb.common.Full
import net.liftweb.common.Empty
import net.liftweb.mapper.MappedTextarea
import net.liftweb.mapper.MappedEnum
import net.liftweb.mapper.MappedLongForeignKey
import net.liftweb.mapper.MappedBoolean
import net.liftweb.mapper.MappedString
import net.liftweb.mapper.MappedInt
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.mapper.CRUDify
import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.IdPK

import scala.xml.Text

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.XmlConfigDependency
import name.panitz.subato.model.MapperModelDependencies


class Survey
    extends LongKeyedMapper[Survey]
    with SWrapperL10n
    with XmlConfigDependency
    with IdPK {

  def getSingleton = Survey

  def this(cId:Long, name:String){
    this()
    this.course.set(cId)
    this.name.set(name)
  }

  object name extends MappedPoliteString(this, 100) 

  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def displayName = locStr("class")

    override def validSelectValues = {
      _root_.name.panitz.subato.model.LdapCurrentUser.uid.map(user =>
	for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user)))	yield {
	  val courseId = iti.course.get;
	  val Full(name) = Course.find(courseId).map(_.name.get)
	  (courseId, name)
	}
      )
    }
  }

  object isPrivate extends MappedBoolean(this){
    override def defaultValue = true
  }

  object isActive extends MappedBoolean(this){
    override def defaultValue = false
  }

  object multipleCard  extends MappedLongForeignKey(this, MultipleCard) {
    override def dbIndexed_? = true
    override def validSelectValues
    = Full(MultipleCard
      .findAll
      .map(x => (x.id.get, x.exerciseText.get.substring(0,Math.min(30,x.exerciseText.get.length))+"" ))
    )
  }

}

object  Survey extends Survey
   with LongKeyedMetaMapper[Survey]
   with LecturerOnlyCRUDify[Long, Survey] {
}

class SurveyEntry  extends LongKeyedMapper[SurveyEntry]
    with SWrapperL10n
    with IdPK {

  def getSingleton = SurveyEntry

  def this(cId:Long){
    this()
    this.survey.set(cId)
  }

  object survey extends MappedLongForeignKey(this, Survey) {
    override def dbIndexed_? = true
  }
}

object  SurveyEntry extends SurveyEntry
   with LongKeyedMetaMapper[SurveyEntry]
   with LecturerOnlyCRUDify[Long, SurveyEntry] {
}
  
class SurveyEntryAnswer  extends LongKeyedMapper[SurveyEntryAnswer]
    with SWrapperL10n
    with IdPK {

  def getSingleton = SurveyEntryAnswer

  def this(cId:Long,t:String){
    this()
    this.surveyEntry.set(cId)
    this.text.set(t)
  }

  object surveyEntry extends MappedLongForeignKey(this, SurveyEntry) {
    override def dbIndexed_? = true
  }

  object number extends MappedInt(this){
    override def defaultValue = 0
  }

  object text extends MappedPoliteString(this, 100) 

}

object  SurveyEntryAnswer extends SurveyEntryAnswer
   with LongKeyedMetaMapper[SurveyEntryAnswer]
   with LecturerOnlyCRUDify[Long, SurveyEntryAnswer] {
}

