package name.panitz.subato.model.mapper

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.model.MapperModelDependencies

import scala.xml.Text
import net.liftweb.common._
import net.liftweb.mapper._

import net.liftweb.common.{Full, Box}
import net.liftweb.http.{S, RedirectResponse}
import net.liftweb.sitemap.Loc
import net.liftweb.util.ControlHelpers
import net.liftweb.sitemap.Menu


class ResourceFileForCourse extends LongKeyedMapper[ResourceFileForCourse]
   with MapperModelDependencies
   with IdPK
   with ControlHelpers	
   with SWrapperL10n
{

  def getSingleton = ResourceFileForCourse

  object name extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }

  object description extends MappedPoliteString(this, 1000) 

  object coursePage extends MappedLongForeignKey(this, CoursePage) {
    override def dbIndexed_? = true
//    override def validSelectValues = {
//      coursePages.findAll().map((cp)=> (cp.id,cp.name))
      /*     currentUser.uid.map(user =>{
          for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user))) yield {
            val courseId = iti.course.get;
            val course = Course.find(courseId)
            (courseId, course.map(_.name)+"")            
          }
      }
      )*/
//    }
/*    override def asHtml() = {
      Text(Course.find(By(Course.id, this.get)) map {_.name.get} openOr "")
    }*/
  }

  /* public means that unregistered user world wide can access this resource */
  object isPublic extends MappedBoolean(this) {
    override def defaultValue = false
  }

  /* isResource means that the file is listed in the resource section */
  object isResource extends MappedBoolean(this) {
    override def defaultValue = false
  }

}

object ResourceFileForCourse extends ResourceFileForCourse
  with LongKeyedMetaMapper[ResourceFileForCourse]
  with LecturerOnlyCRUDify[Long, ResourceFileForCourse]
  with SWrapperL10n
{

//  override def fieldsForList = List(name, exercise)

  //editing or deleting allowed?
  private def deleteEditAllowed(b:Box[ResourceFileForCourse]) = b match {
    case Full(e) => {
      val cpb = coursePages.find(e.coursePage.get)
      cpb.map((cp)=>currentUser.isLecturerIn(cp.course.id,cp.term.id)).openOr(false)
    }
//    case Full(e) => User.isLecturerIn(coursePages.find(e.coursePage.get).openOrThrowException("").id)
    case _ => false
  }

  private def errorAndRedirect() = {
    S.error(locStr("accessDenied"))
    new RedirectResponse(S.referer openOr "/",null)
  }

  override def showAllMenuLoc = Empty

  override def createMenuLoc: Box[Menu] = Empty


  //override menu loc params to disable deleting and editing if the user isn't allowed to
  override def deleteMenuLocParams =
    new Loc.IfValue[ResourceFileForCourse](deleteEditAllowed, errorAndRedirect _) ::
		super.deleteMenuLocParams

  override def editMenuLocParams =
    new Loc.IfValue[ResourceFileForCourse](deleteEditAllowed, errorAndRedirect _) ::
      super.editMenuLocParams

  //L10n
  override def createMenuName = locStr("createResourceFileForCourse")
  override def showAllMenuName = locStr("listResourceFileForCourse")
  override def editMenuName = locStr("editResourceFileForCourse")
  override def deleteMenuName = locStr("deleteResourceFileForCourse")
  override def viewMenuName = locStr("viewResourceFileForCourse")
  override def displayName = locStr("ResourceFileForCourse")


}
