package name.panitz.subato.model

import net.liftweb.common.Box
import scala.xml.Node

trait ResourceFileForCourse extends  IdAndNamed {
  def name:String
  def description:String
  def course:CoursePage
  def isPublic:Boolean
  def isResource:Boolean
}


private[model] class MapperResourceFileForCourse(private val c: mapper.ResourceFileForCourse) extends ResourceFileForCourse {
  override def id = c.id.get
  override def name:String = c.name.get
  override def description:String = c.description.get
  override def course = new MapperCoursePage(c.coursePage.obj.openOrThrowException(""))
  override def isPublic:Boolean = c.isPublic.get
  override def isResource:Boolean= c.isResource.get
}

trait ResourceFileForCourses extends Finder[ResourceFileForCourse] {
  def createAndSave(name: String,description:String,courseId:Long)
  def findAll(courseId:Long):List[ResourceFileForCourse]
}

import name.panitz.subato.model.mapper.{ ResourceFileForCourse => MC }

object MapperResourceFileForCourses extends  ResourceFileForCourses {
  override def find(id: Long) = MC.find(id).map(new MapperResourceFileForCourse(_))
  override def findAll = MC.findAll.map(new MapperResourceFileForCourse(_))
  import net.liftweb.mapper.By
  override def findAll(courseId:Long) = MC.findAll(By(MC.coursePage,courseId)).map(new MapperResourceFileForCourse(_))

  override def createAndSave(name: String,description:String,exId:Long) = {
    val mc = new MC().name(name).description(description).coursePage(exId)
    mc.save()
    new MapperResourceFileForCourse(mc)
  }
}


