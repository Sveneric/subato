package name.panitz.subato.model.mapper.learnsystem

import net.liftweb.mapper.{By, IdPK, LongKeyedMapper, LongKeyedMetaMapper, MappedLongForeignKey, MappedPoliteString, MappedBoolean, MappedInt, MappedEnum}
import name.panitz.subato.model.mapper.{CardBox, MultipleCard, User}
import java.time.LocalDateTime

class LearningMaterial extends LongKeyedMapper[LearningMaterial] with IdPK {

  def getSingleton = LearningMaterial

  def this(flashcardCourse: Long, card: Long, isExamMaterial: Boolean) {
    this()
    this.flashcardCourse.set(flashcardCourse)
    this.flashcard.set(card)
    this.isExamMaterial.set(isExamMaterial)
  }

  object flashcardCourse extends MappedLongForeignKey(this, FlashcardCourse) {}

  object flashcard extends MappedLongForeignKey(this, MultipleCard) {}

  object isExamMaterial extends MappedBoolean(this) {
    override def defaultValue = false
  }

  override def delete_! = {
    SubscribedLearningMaterial findAll By(SubscribedLearningMaterial.learningMaterial, id.get) foreach { _.delete_! }
    super.delete_!
  }
}

object LearningMaterial extends LearningMaterial with LongKeyedMetaMapper[LearningMaterial] {}


/*
 * This Enumeration represents the five partitions of the flashcard (System of Leitner)
 **/
object MasteryLevel extends Enumeration {
  type MasteryLevel = Value

  val One   = Value(1)
  val Two   = Value(2)
  val Three = Value(3)
  val Four  = Value(4)
  val Five  = Value(5)
  val End   = Value(6)

  val NotExaminedYet = Value(7)
  val ExamFailed     = Value(9)
}

import MasteryLevel._
class SubscribedLearningMaterial extends LongKeyedMapper[SubscribedLearningMaterial] with IdPK {

  def getSingleton = SubscribedLearningMaterial

  def this(learner: String, learningMaterial: Long, flashcardCourse: Long, initialMasteryLevel: MasteryLevel) {
    this()
    this.subscriber set learner 
    this.learningMaterial set learningMaterial
    this.flashcardCourse set flashcardCourse
    this.masteryLevel set initialMasteryLevel
  }

  object subscriber extends MappedPoliteString(this, 128)

  object learningMaterial extends MappedLongForeignKey(this, LearningMaterial) {
    override def dbIndexed_? = true
  }

  object flashcardCourse extends MappedLongForeignKey(this, FlashcardCourse) {
    override def dbIndexed_? = true
  }

  object masteryLevel extends MappedEnum(this, MasteryLevel) {
  }

  object lastLearningSession extends MappedPoliteString(this, 16) {
    override def defaultValue = LocalDateTime.now.format(FlashcardCourseDateFormatter.get)
  }

}

object SubscribedLearningMaterial
    extends SubscribedLearningMaterial
    with LongKeyedMetaMapper[SubscribedLearningMaterial] {}
