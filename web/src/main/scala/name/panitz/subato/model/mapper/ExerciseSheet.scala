package name.panitz.subato.model.mapper

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.model.MapperModelDependencies

import scala.xml._
import net.liftweb.common._
import net.liftweb.mapper._

import net.liftweb.common.{Full, Box}
import net.liftweb.http.{S, RedirectResponse}
import net.liftweb.http.{ SHtml }
import net.liftweb.sitemap.Loc
import net.liftweb.util.ControlHelpers
import net.liftweb.sitemap.Menu
import net.liftweb.sitemap.Loc.Hidden
import java.text.DateFormat
import java.util.Date

import S._

class ExerciseSheet extends LongKeyedMapper[ExerciseSheet]
   with MapperModelDependencies
   with IdPK
   with ControlHelpers	
   with SWrapperL10n
{

  def getSingleton = ExerciseSheet

  object name extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }
  object description extends MappedWysiwygEditor(this, ()=>locStr("description"))

  object number extends MappedInt(this)

  object reachablePoints extends MappedInt(this)

  object isPublic extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object lastSubmission extends MappedDateTime(this) {
//    val df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, S.locale)
    override def parse(input: String): Box[Date] = (tryo { df.parse(input) }) match {
      case f@Full(_) => f
      case _ => Full(null)
    }
    override def displayName = "due date " 
    override def displayHtml = Text(displayName)
    override def asHtml = Text(if (this.get != null) df.format(this.get) else "")
    val df = new java.text.SimpleDateFormat("y-M-d H:m");
    val datef = new java.text.SimpleDateFormat("yyyy-MM-dd");
    val timef = new java.text.SimpleDateFormat("HH:mm");

    override def _toForm: Box[Elem] = {
      var dateString="";
      var timeString="";
      def trySet(){
        try{
          this.setFromAny(df.parse(dateString+" "+timeString))
        }catch{
          case e:Exception  => println(e)
        }
      }

      Full(<span>
        {SHtml.text({if (get==null) "2050-05-10" else {val r = datef.format(get);r}}
          , x=> {dateString=x;trySet();println(dateString+" "+timeString+this.get)}
          , "type" -> "date")
        }
        {SHtml.text({if (get==null) "20:15" else timef.format(get)}
          , x=> {timeString=x;trySet();println(dateString+" "+timeString+this.get)}
          , "type" -> "time")
        }
        </span>
      )
    }
  }


  def sheetExercises: List[ExerciseSheetEntry]
  = ExerciseSheetEntry.findAll(By(ExerciseSheetEntry.exerciseSheet, id.get))

  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def validSelectValues = {
      currentUser.uid.map(user =>
        for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user)))
        yield {
          val courseId = iti.course.get;
          val Full(name) = Course.find(courseId).map(_.name.get)
          (courseId, name)
        }
      )
    }

    override def asHtml() = {
      Text(Course.find(By(Course.id, this.get)) map {(c) => c.name.get} openOr "")
    }
    
  }
  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def validSelectValues = {
     Full(for (term <- Term.findAll())yield {
          (term.id.get, (term.year+" "+term.addendum))
     })
    }
    override def asHtml() = {
      Text(Term.find(By(Term.id, this.get)) map {(t) => t.addendum.get+" "+t.year.get} openOr "")
    }
  }

  def courseName = Course.find(By(Course.id, course.get)) map {_.name.get} openOr ""
}

object ExerciseSheet extends ExerciseSheet
  with LongKeyedMetaMapper[ExerciseSheet]
  with LecturerOnlyCRUDify[Long, ExerciseSheet]
  with SWrapperL10n
{

//  override def fieldsForList = List(name, description, course,term)

  //editing or deleting allowed?
  private def deleteEditAllowed(b:Box[ExerciseSheet]) = b match {
    case Full(e) => User.isLecturerIn(e.course.get)
    case _ => false
  }

  private def errorAndRedirect() = {
    S.error(locStr("accessDenied"))
    new RedirectResponse(S.referer openOr "/",null)
  }

  //override menu loc params to disable deleting and editing if the user isn't allowed to
/*  override def deleteMenuLocParams =
    new Loc.IfValue[ExerciseSheet](deleteEditAllowed, errorAndRedirect _) ::
		super.deleteMenuLocParams

  override def editMenuLocParams =
    new Loc.IfValue[ExerciseSheet](deleteEditAllowed, errorAndRedirect _) ::
  super.editMenuLocParams
 */
  //L10n
  override def createMenuName = locStr("createExerciseSheet")
  override def showAllMenuName = locStr("listExercisesSheet")
  override def editMenuName = locStr("editExercisesSheet")
  override def deleteMenuName = locStr("deleteExerciseSheet")
  override def viewMenuName = locStr("viewExerciseSheet")
  override def displayName = locStr("exerciseSheet")
  override def showAllMenuLoc = Empty
}


private[model] class ExerciseSheetEntry extends LongKeyedMapper[ExerciseSheetEntry]
  with IdPK
  with MapperModelDependencies
  with SWrapperL10n
{
  def getSingleton = ExerciseSheetEntry

  object number extends MappedInt(this)

  object exerciseSheet extends MappedLongForeignKey(this, ExerciseSheet) {
    override def dbIndexed_? = true

    override def validSelectValues
    = currentUser.uid.map(user =>
      (for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user)))
      yield {
        val courseId = iti.course.get;
        ExerciseSheet.findAll.filter((x)=>x.course.get==courseId).map(x => (x.id.get, x.name.get+" ("+x.courseName+")"))
      }).flatten)

    override def asHtml() = {
      Text(ExerciseSheet.find(By(ExerciseSheet.id, this.get))  map {(x)=>x.name.get } openOr "")
    }

    def courseID = ExerciseSheet.find(By(ExerciseSheet.id, this.get)) map {_.course.get} openOr -1
  }
	
  object exercise extends MappedLongForeignKey(this, Exercise) {
    override def dbIndexed_? = true
    override def validSelectValues
    = Full(Exercise
      .findAll.filter((x)=>User.isLecturerIn(x.course.get))
      .map(x => (x.id.get, x.name.get+" ("+x.description.get.substring(0,Math.min(30,x.description.get.length))+")" ))
    )

    override def asHtml() = {
      Text(Exercise.find(By(Exercise.id, this.get)) map {_.name.get} openOr "" )
    }
  }

   
}

object ExerciseSheetEntry extends ExerciseSheetEntry
  with LongKeyedMetaMapper[ExerciseSheetEntry]
  with LecturerOnlyCRUDify[Long, ExerciseSheetEntry] {

  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams
  override def createMenuName = locStr("createExerciseSheetEntry")
}
