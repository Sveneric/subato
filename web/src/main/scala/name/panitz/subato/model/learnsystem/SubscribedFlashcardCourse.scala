package name.panitz.subato.model.learnsystem

import java.time.LocalDateTime

import FinalGrade._
import name.panitz.subato.model.{Finder, Id, MapperModelDependencies}
import name.panitz.subato.model.mapper.learnsystem.{FlashcardCourseDateFormatter => FCDF, MasteryLevel, SubscribedFlashcardCourse => SFCC}
import name.panitz.subato.model.mapper.learnsystem.MasteryLevel._
import net.liftweb.common.{Box, Full}
import net.liftweb.mapper.By


trait SubscribedFlashcardCourse extends Id {
  def finalGrade: FinalGrade
  def toFlashcardCourse: Box[(FlashcardCourse, SubscribedFlashcardCourse)]
  def flashcardCourseId: Long
  def isCompleted_? : Boolean
  def isLinkedTo(course: FlashcardCourse): Boolean
  def nextMaterialsToLearn: Seq[SubscribedLearningMaterial]
  def material(cardId: Long): Box[SubscribedLearningMaterial]
  def ownerId: String
  def isInExamModus_? : Boolean
  def lastLearningSession: LocalDateTime
  def userMustBeCongratulated_? : Boolean
  def userHasBeenCongratulated(): Unit
  
  def allMaterials: Seq[SubscribedLearningMaterial] = Nil

  def save(): Unit = {}

  def delete(): Unit = {}

  def completion: Double = {
    if (isCompleted_?) {
      100.0
    } else {
      val materials = allMaterials
      val numOfMaterials = materials.size
      var result = 0.0
      for (slm <- materials) result += completionOf(slm, numOfMaterials)
      result
    }
  }

  def completionOf(slm: SubscribedLearningMaterial, numOfMaterials: Int): Double = {
    val COMPLETION_PER_CARD_AND_LEVEL: Double = 20.0 / numOfMaterials
    if (slm.isExamMaterial_?) {
      if (slm.masteryLevel == End) (5 * COMPLETION_PER_CARD_AND_LEVEL) else 0
    } else {
      (slm.masteryLevel.id - 1) * COMPLETION_PER_CARD_AND_LEVEL
    }
  }

}

trait SubscribedFlashcardCourses extends Finder[SubscribedFlashcardCourse] {
  def allBy(uid: String): Seq[SubscribedFlashcardCourse]
  def getFor(uid: String, flashcardCourseId: Long): Box[SubscribedFlashcardCourse]
  def existFor(flashcardCourseId: Long): Boolean
  def linkedTo(flashcardCourseId: Long): Seq[SubscribedFlashcardCourse]
  def createAndSave(uid: String, flashcardCourse: Long): SubscribedFlashcardCourse
}

class MapperSubscribedFlashcardCourse(val course: SFCC) extends SubscribedFlashcardCourse with MapperModelDependencies {

  override def id: Long = course.id.get

  override def ownerId: String = course.subscriber.get

  private def lastVisitedBox: MasteryLevel = course.lastVisitedBox.get

  override def isInExamModus_? : Boolean = course.inExam.get

  private def nextMasteryLevel: (MasteryLevel, Seq[SubscribedLearningMaterial]) = {
    markCompletionByNeed()
    if (isCompleted_?) {
      (MasteryLevel.End, allMaterials)
    } else {
      val toTrain = allTrainingMaterials
        .filter { case (lvl, _) => lvl != End }
        .sortBy { case (_, seq) => seq.size }
      if (toTrain.size > 1)
        toTrain.filter { case (lvl, _) => lvl != lastVisitedBox }.last
      else if (toTrain.nonEmpty)
        toTrain.last
      else
        (One, Nil)
    }
  }

  override def allMaterials: Seq[SubscribedLearningMaterial] = MapperSubscribedLearningMaterials of (course.subscriber.get, course.flashcardCourse.get)

  private def updateMasteryLevel(newLvl: MasteryLevel): Unit = {
    course.lastVisitedBox set newLvl
    save
  }

  override def lastLearningSession: LocalDateTime = {
    val result: LocalDateTime = LocalDateTime.parse(course.lastLearningSession.get, FCDF.get)
    course.lastLearningSession.set(LocalDateTime.now.format(FCDF.get))
    save
    result
  }
  
  override def finalGrade: FinalGrade = {
    val maybeProfile = MapperLearningProfiles.findOrCreate(course.subscriber.get)
    maybeProfile match {
      case Some(profile) => profile.getCompletionGradeOf(flashcardCourseId)
      case None          => NULL
    }
  }

  override def isLinkedTo(course: FlashcardCourse): Boolean = this.course.flashcardCourse.get == course.id

  override def isCompleted_? : Boolean = course.isCompleted.get

  private def markCompletionByNeed(): Unit = {
    if(trainingHasJustBeenCompleted_?) {
      markTrainingCompletion()
      tryToMarkCourseAsCompleted()
    } else if(examCanBeEvaluated_?) {
      evaluateExamResults()
    }
  }

  private def markTrainingCompletion(): Unit = {
    course.trainingCompleted.set(true)
    save()
  }

  override def userMustBeCongratulated_? : Boolean = {
    markCompletionByNeed()
    course.userMustBeCongratulated.get
  }

  private def markUserMustBeCongratulated(): Unit = {
    course.userMustBeCongratulated.set(true)
    save()
  }

  override def userHasBeenCongratulated(): Unit = {
    course.userMustBeCongratulated.set(false)
    save()
  }

  private def isTrainingCompleted_? : Boolean = course.trainingCompleted.get

  private def trainingHasJustBeenCompleted_? : Boolean = {
    (!isTrainingCompleted_?) && areAllMaterialsLearned_?
  }

  private def examCanBeEvaluated_? : Boolean = {
    isInExamModus_? &&
    examMaterials.forall { _.masteryLevel != NotExaminedYet }
  }

  private def tryToMarkCourseAsCompleted(): Unit = {
    if (courseHasExam_?) {
      markAsInExam
    } else {
      course.markAsCompleted
      examCompleted()
      markUserMustBeCongratulated()
      storeEarnedScore(100)
    }
  }

  private def evaluateExamResults(): Unit = {
    val em = examMaterials
    val correctlyAnsweredCards = em filter { _.masteryLevel == End }
    val score = 100.0 * (correctlyAnsweredCards.size.toDouble / em.size)
    if (score >= 50) {
      course.markAsCompleted
      examCompleted
      markUserMustBeCongratulated()
      storeEarnedScore(score)
    } else {
      correctlyAnsweredCards foreach { _.degradeMasteryLevel }
    }
  }

  private def storeEarnedScore(score: Double): Unit = {
    val maybeProfile = MapperLearningProfiles.findOrCreate(course.subscriber.get)
    maybeProfile match {
      case Some(profile) => profile.updateEarnedStars(flashcardCourseId, score)
      case None          =>
    }
  }

  override def toFlashcardCourse: Box[(FlashcardCourse, SubscribedFlashcardCourse)] = {
    MapperFlashcardCourses.find(course.flashcardCourse.get) map {(_, this)}
  }

  override def flashcardCourseId: Long = course.flashcardCourse.get

  override def delete: Unit = {
    MapperSubscribedLearningMaterials delete (course.subscriber.get, course.flashcardCourse.get)
    course.delete_!
  }
  
  private def allTrainingMaterials: Seq[(MasteryLevel, Seq[SubscribedLearningMaterial])] = {
    updateSubscribedMaterialsIfNeeded
    MapperSubscribedLearningMaterials
      .of (course.subscriber.get, course.flashcardCourse.get)
      .filter { !_.isExamMaterial_? }
      .groupBy { _.masteryLevel }
      .toSeq
  }

  override def nextMaterialsToLearn: Seq[SubscribedLearningMaterial] = {
    if (isInExamModus_?) {
      examMaterials
    } else if(allTrainingMaterials.isEmpty){
      markAsInExam
      examMaterials
    }else {
      val (nextLvl, lms) = nextMasteryLevel
      updateMasteryLevel(nextLvl)
      lms
    }
  }

  private def examMaterials: Seq[SubscribedLearningMaterial] = {
    updateSubscribedMaterialsIfNeeded
    MapperSubscribedLearningMaterials
      .of(course.subscriber.get, course.flashcardCourse.get)
      .filter { _.isExamMaterial_? }
  }

  def updateSubscribedMaterialsIfNeeded: Unit = {
    val maybeFlashcardCourse = MapperFlashcardCourses.find(course.flashcardCourse.get)
    val (updateMustBeConsidered_?, fcc) = maybeFlashcardCourse match {
      case Full(fcc_) => (lastLearningSession isBefore fcc_.lastUpdate, fcc_)
      case _          => (false, null)
    }

    if (updateMustBeConsidered_?) {
      val subscribedmaterials = MapperSubscribedLearningMaterials of (course.subscriber.get, course.flashcardCourse.get)
      val toAdd = fcc.learningMaterials filter { x => !subscribedmaterials.exists(_.learningMaterialId == x.id) }
      toAdd foreach { _.subscribe(course.subscriber.get) }
    }
  }

  def markAsInExam(): Unit = {
    if (courseHasExam_?) {
      course.inExam.set(true)
      save()
    }
  }

  private def courseHasExam_? : Boolean = examMaterials.nonEmpty

  def examCompleted(): Unit = {
    course.inExam.set(false)
    save()
  }

  override def material(cardId: Long): Box[SubscribedLearningMaterial] = {
    val trainingMaterials = allTrainingMaterials.foldLeft(Seq.empty[SubscribedLearningMaterial]) { case (res, (_, seq)) => seq ++: res }
    (trainingMaterials ++: examMaterials) find {
      _.learningMaterial match {
        case Full(lm) => lm.flashcardId == cardId
        case _        => false
      }
    }
  }

  private def areAllMaterialsLearned_? : Boolean = {
    val trainingMaterials = allTrainingMaterials
    lazy val (lvl, _)     = trainingMaterials.head
    (trainingMaterials.size == 1) && (lvl == End)
  }

  override def save: Unit = course.save

}


object MapperSubscribedFlashcardCourses extends SubscribedFlashcardCourses {

  override def allBy(user: String): Seq[SubscribedFlashcardCourse] = {
    SFCC.findAll(By(SFCC.subscriber, user)) map { new MapperSubscribedFlashcardCourse(_) }
  }

  override def getFor(uid: String, flashcardCourseId: Long): Box[SubscribedFlashcardCourse] = {
    SFCC.find(By(SFCC.subscriber, uid), By(SFCC.flashcardCourse, flashcardCourseId)) map {new MapperSubscribedFlashcardCourse(_)}
  }

  override def existFor(flashcardCourseId: Long): Boolean = {
    !SFCC.find(By(SFCC.flashcardCourse, flashcardCourseId)).isEmpty
  }

  override def find(id: Long): Box[SubscribedFlashcardCourse] = SFCC.find(id) map { new MapperSubscribedFlashcardCourse(_) }

  override def findAll: List[SubscribedFlashcardCourse] = SFCC.findAll map { new MapperSubscribedFlashcardCourse(_) }

  override def createAndSave(uid: String, flashcardCourse: Long): SubscribedFlashcardCourse = {
    val subscription = new MapperSubscribedFlashcardCourse(new SFCC(uid, flashcardCourse))
    subscription.save
    MapperSubscribedLearningMaterials createAndSaveFrom (uid, flashcardCourse)
    subscription
  }

  override def linkedTo(flashcardCourseId: Long): Seq[SubscribedFlashcardCourse] = {
    SFCC.findAll(By(SFCC.flashcardCourse, flashcardCourseId)) map { new MapperSubscribedFlashcardCourse(_) }
  }

}
