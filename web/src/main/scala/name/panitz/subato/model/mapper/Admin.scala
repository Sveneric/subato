package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.common.Full
import net.liftweb.mapper.By
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.mapper.MappedTextarea
import net.liftweb.mapper.MappedString
import net.liftweb.mapper.IdPK
import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.CRUDify

class Admin extends LongKeyedMapper[Admin] with IdPK {
  def getSingleton = Admin
	
  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }
}

object Admin extends Admin
    with LongKeyedMetaMapper[Admin]
    with AdminOnlyCRUDify[Long, Admin] {
  def isAdmin(uid:String) = find(By(this.uid, uid)) match {
    case Full(_) => true
    case _ => false
  }
}
