package name.panitz.subato.model

import net.liftweb.mapper.By
import net.liftweb.common.Box
import mapper.{Group => MGroup}
import java.time.LocalTime
import java.time.LocalDateTime

object Weekday extends Enumeration {
  type Weekday = Value
  val Mo, Tu, We, Th, Fr, Sa, Su = Value

  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)

  def german:String= this match{
    case Tu => "Di"
    case We => "Mi"
    case Th => "Do"
    case Su => "So"
    case _  => this.toString
  }
}

import Weekday._

trait Group extends IdAndNamed {
  def name:String
  def course:Box[Course]
  def courseId:Long
  def term:Box[Term]
  def termId:Long
  def lecturer:String
  def weekday: java.time.DayOfWeek
  def time:LocalTime
  def room:String
  def isLecture:Boolean
  def member:List[UserInGroup]
  def coursePage:CoursePage
}

trait Groups extends Finder[Group] {
  def findByCourseAndTerm(course:Course,term:Term):List[Group]
  def findByCourseAndTerm(courseId:Long,termId:Long):List[Group]
  def createAndSave(name:String, course:Course,term:Term):Group
}

class MapperGroup(private val mg:MGroup) extends Group with MapperModelDependencies {
  def id = mg.id.get
  def name = mg.name.get
  def course = mg.course.obj.map(new MapperCourse(_))
  def courseId = mg.course.get
  def term = mg.term.obj.map(new MapperTerm(_))
  def termId = mg.term.get
  def lecturer:String=mg.lecturer.get
  def weekday = java.time.DayOfWeek.of(mg.weekday.get.id+1)
  def time:LocalTime={
    val t = mg.time.get
    if (t == null) return null
    val input = t.toString.replace( " " , "T" );
    val ldt = LocalDateTime.parse( input );
    ldt.toLocalTime();
  }
  def room:String=mg.room.get
  def isLecture:Boolean=mg.isLecture.get

  def member = mapper.UserInGroup.findAll(By(mapper.UserInGroup.group,id)).map(new MapperUserInGroup(_))

  override def coursePage:CoursePage=coursePages.find(courseId,termId)(0)


  override def toString:String={
    var result = name
    if (weekday!=null) result = result +" "+weekday.getDisplayName(java.time.format.TextStyle.SHORT,net.liftweb.http.S.locale);
    if (time!=null) result = result +" "+time
    if (room!=null) result = result +" "+room
    if (lecturer!=null) result = result +" ("+lecturer+")"
    result
  }
  override def equals(that:Any) = {
    if (that.isInstanceOf[Group]) {
      that.asInstanceOf[Group].name == this.name
    } else {
      false
    }
  }
}

object MapperGroups extends Groups with MapperFinder[Group, MGroup] {
  override def createAndSave(name:String, course:Course,term:Term):Group = {
    val mg = new MGroup().name(name).course(course.id).term(term.id)
      mg.save()
    val res = new MapperGroup(mg)
    res
  }
  override def wrap(in:MGroup):Group = new MapperGroup(in)
  override def metaMapper = MGroup

  override def findByCourseAndTerm(course:Course,term:Term)
    = findByCourseAndTerm(course.id,term.id)

  override def findByCourseAndTerm(courseId:Long,termId:Long)
    = MGroup.findAll(By(MGroup.course, courseId),By(MGroup.term, termId)).map(wrap(_))
}
