package name.panitz.subato.model
import name.panitz.subato.model.mapper.{ Course => MC }

import net.liftweb.common.Box

trait Courses extends Finder[Course] {
  def createAndSave(name: String): Course
}

import scala.xml.Node

trait Course extends IdAndNamed {
  def name: String
  def description:Node
  def exercises: List[Exercise]
  def exerciseSheets(termId:Long): List[ExerciseSheet]
  def exerciseSheets: List[ExerciseSheet]
  def asXML: Node
//  def resourceFiles:List[ResourceFileForCourse]
  def files:List[String]

}

object MapperCourses extends Courses {
  override def find(id: Long) = MC.find(id).map(new MapperCourse(_))
  override def findAll = MC.findAll.map(new MapperCourse(_))
  override def createAndSave(name: String) = {
    val mc = new MC().name(name)
    mc.save()
    new MapperCourse(mc)
  }
}

import name.panitz.subato.XmlConfigDependency

private[model] class MapperCourse(private val c: mapper.Course) extends Course with XmlConfigDependency{
  override def id = c.id.get
  override def name = c.name.get
  override def description = c.description.asHtml
  override def exercises = c.exercises.map(new MapperExercise(_))
  override def exerciseSheets(termId:Long) = c.exerciseSheets(termId).map(new MapperExerciseSheet(_))
  override def exerciseSheets = c.exerciseSheets.map(new MapperExerciseSheet(_))
  override def asXML = <course>{for (exercise <- exercises) yield exercise.asXML}</course>

//  override def resourceFiles:List[ResourceFileForCourse] = c.resourceFiles.map(new MapperResourceFileForCourse(_))

  override def files:List[String] = {//
    def filesInFolder(folderName:String):List[String] ={
      val folder = new java.io.File(folderName)
      if (!folder.isDirectory){
        List(folderName)
      }else {
        folder.list.map(f => filesInFolder(folderName+"/"+f)).toList.flatten
      }
    }
    val courseDir = config.courseBaseDir+"/"+c.id.get+"/"
    filesInFolder(courseDir).map(s=>s.substring((courseDir).size))
  }

}
