package name.panitz.subato.model

import name.panitz.subato.model.learnsystem.LearningProfile
import name.panitz.subato.model.mapper.learnsystem.MasteryLevel._

trait AbstractCardOracle {
  def fromEasiestToMostDifficult(cards: Seq[Card]): Seq[Card]
  def next(cards: Seq[Card], profile: LearningProfile): Option[Card] = None
}

object CardOracle extends AbstractCardOracle {
  override def fromEasiestToMostDifficult(cards: Seq[Card]): Seq[Card] = sortByStatisticalData(cards)

  private def sortByStatisticalData(cards: Seq[Card]): Seq[Card] = {
    def statisticEvaluation(card: Card): Double = MapperStatistics.findCardStatistic(card.id).evaluate
    cards sortBy { statisticEvaluation }
  }

}
