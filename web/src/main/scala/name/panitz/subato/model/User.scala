package name.panitz.subato.model

import net.liftweb.common.Empty
import net.liftweb.common.Full
import net.liftweb.mapper.By

import net.liftweb.common.Box
import name.panitz.subato.model.mapper.User
import mapper.{UserInGroup => MUIG}

trait CurrentUser {
  def isLecturerIn(course:Course):Boolean
  def isLecturerIn(courseId:Long):Boolean
  def isLecturerIn(courseId:Long,termId:Long):Boolean
  def isLecturer:Boolean

  def isAuthorOfSlsCourse(courseId:Long):Boolean

  def isTutorIn(course:Course):Boolean
  def isTutorIn(courseid:Long):Boolean
  def isTutorIn(courseId:Long,termId:Long):Boolean
  def isTutor:Boolean

  def loggedIn:Boolean
  def uid:Box[String]
  def uid_=(uid:Box[String])
  def isAdmin:Boolean
  def addGroup(group:Group)
  def group(course:Course,term:Term):Box[Group]
}

private[model] trait AbstractLdapCurrentUser extends CurrentUser with ModelDependencies {
  private def uid_! = uid.openOrThrowException("uid is not set for LDAP User")   //open_!

  //programmatically set uid, only for testing
  private var progSetUid:Box[String] = Empty
	
  override def loggedIn = User.loggedIn_?
  override def isAdmin =  {
    loggedIn  && (admins.contains(uid_!) || uid_! =="admin")
  }
  override def isLecturer = loggedIn  && (lecturers.contains(uid_!)  || admins.contains(uid_!))
  override def isTutor = loggedIn  && (tutors.contains(uid_!) || lecturers.contains(uid_!))

  override def isLecturerIn(course:Course)
    = loggedIn && (lecturers.contains(uid_!, course) || admins.contains(uid_!)) 
  override def isLecturerIn(courseId:Long)
   = loggedIn && (lecturers.contains(uid_!, courseId) || admins.contains(uid_!))
  override def isLecturerIn(courseId:Long,termId:Long)
  = {
    loggedIn && (lecturers.contains(uid_!, courseId,termId) || admins.contains(uid_!))
  }

  override def isAuthorOfSlsCourse(courseId:Long)
  // = loggedIn && (flashcardCourses.find(courseId).openOrThrowException("sls course with id "+courseId+" is unknown").author==uid_! || admins.contains(uid_!))
   = loggedIn && (flashcardCourses.find(courseId).map(_.author)==uid || admins.contains(uid_!))

  override def isTutorIn(courseId:Long,termId:Long)
  = {
    loggedIn && (tutors.contains(uid_!, courseId,termId)
      || lecturers.contains(uid_!, courseId,termId)
      || admins.contains(uid_!))
  }
  override def isTutorIn(course:Course)
    = loggedIn && (tutors.contains(uid_!, course) || lecturers.contains(uid_!))

  override def isTutorIn(courseId:Long)
    = loggedIn && (tutors.contains(uid_!, courseId) || lecturers.contains(uid_!))


  override def uid = progSetUid match {
    case Full(_) => progSetUid
    case _ => User.currentUserId
  }

 override def uid_=(uid:Box[String]) { uid match {
    case Full(uid) => progSetUid = Full(uid)
    case _ =>
  }}

  override def addGroup(group:Group) {
    //delete groups that belong to the same course. User is pro term und course just in one group.
    //we change the group.
    for (c <- group.course; t<- group.term;g <- groups.findByCourseAndTerm(c,t)) {
      MUIG.bulkDelete_!!(By(MUIG.uid, uid_!), By(MUIG.group, g.id))
    }
    //add this one
    new MUIG().uid(uid_!).group(group.id).save()
  }
  override def group(course:Course,term:Term):Box[Group] = uid.flatMap { uid =>
    userInGroups.findByUid(uid,course,term)
  }
}

private[model] object LdapCurrentUser extends AbstractLdapCurrentUser with MapperModelDependencies {

}
