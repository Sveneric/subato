package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.common.Full
import net.liftweb.mapper.By
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.mapper.MappedTextarea
import net.liftweb.mapper.MappedLongForeignKey
import net.liftweb.mapper.IdPK
import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.CRUDify
import scala.xml._
import name.panitz.subato.model.MapperModelDependencies

class Ontology extends LongKeyedMapper[Ontology] with IdPK {
  def getSingleton = Ontology
	
  object name extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }

}

object  Ontology extends  Ontology
  with LongKeyedMetaMapper[Ontology]
  with LecturerOnlyCRUDify[Long, Ontology] {

}


class OntologyEntry extends LongKeyedMapper[OntologyEntry]
    with IdPK
    with MapperModelDependencies{

  def getSingleton = OntologyEntry

  object ontology extends MappedLongForeignKey(this, Ontology) {
    override def dbIndexed_? = true

    override def validSelectValues
      = Full(Ontology.findAll.map(x => (x.id.get, x.name.get)))
     
    override def asHtml() = {
      Text(Ontology.find(this.get)  map {(x)=>x.name.get } openOr "")
    }
  }
  object flashcard extends MappedLongForeignKey(this, MultipleCard) {
    override def dbIndexed_? = true
    override def validSelectValues
    = Full(multipleCards.findAll.map(x => (x.id, x.name)))

    override def asHtml() = {
      <a href={"/ShowSingleCard/"+this.get}>{multipleCards.find(this.get).map((x)=> x.name).openOr ("no flash card found")}</a>
    }
  }
}

object  OntologyEntry extends  OntologyEntry
  with LongKeyedMetaMapper[OntologyEntry]
  with LecturerOnlyCRUDify[Long, OntologyEntry] {

  override def findForList(start: Long, cnt: Int) ={
    println("findall!")
    new Exception().printStackTrace()
    findAll()
  }

}
