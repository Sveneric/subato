package name.panitz.subato.model

import net.liftweb.mapper._
import net.liftweb.common._
import mapper.{UserInGroup => UIG}

trait UserInGroup extends IdAndNamed {
  def name:String
  def group:Group
}

class MapperUserInGroup(private val mg:UIG) extends UserInGroup {
  def id = mg.id.get
  def name = mg.uid.get
  def group = new MapperGroup(mg.group.obj.openOrThrowException(""))
}


trait UserInGroups {
  def findByUid(uid:String, courseId:Long,termId:Long):Box[Group]
  def findByUid(uid:String, course:Course,term:Term):Box[Group]
}

object MapperUserInGroups extends UserInGroups {
  override def findByUid(uid:String, courseId:Long,termId:Long) = {
    UIG.findAll(By(UIG.uid, uid), PreCache(UIG.group)) flatMap {
      uig => uig.group.obj.map(new MapperGroup(_))
    } filter {
      g => g.courseId == courseId && g.termId == termId
    } match {
      case g::_ => Full(g)
      case _ => Empty
    }
  }
  override def findByUid(uid:String, course:Course,term:Term):Box[Group]
    =  findByUid(uid,course.id,term.id)
}
