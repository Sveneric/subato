package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.mapper.MappedString
import net.liftweb.http.UnauthorizedResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.common.Full
import net.liftweb.mapper.{By, IdPK, LongKeyedMapper, LongKeyedMetaMapper, MappedLongForeignKey}
import net.liftweb.sitemap.Loc
import scala.xml.Text
import name.panitz.subato.model.{SubmissionMode, Language, MapperModelDependencies}

import name.panitz.subato.XmlConfigDependency
import name.panitz.subato.SWrapperL10n


class IsTutorIn
    extends LongKeyedMapper[IsTutorIn]
    with IdPK
    with MapperModelDependencies
    with SWrapperL10n
    with XmlConfigDependency{
  def getSingleton = IsTutorIn

  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }
  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def validSelectValues = {
      currentUser.uid.map(user =>
        for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user)))	yield {
	  val courseId = iti.course.get;
	  val Full(name) = Course.find(courseId).map(_.name.get)
	  (courseId, name)
	}
      )
    }

    override def asHtml() = {
      Text(Course.find(By(Course.id, this.get)) map {_.name.get} openOr "")
    }
  }
  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(Term.findAll.map(x => (x.id.get, x.addendum.get+" "+x.year.get)))
    override def asHtml() = {
      Text(Term.find(By(Term.id, this.get)) map {(t) => t.addendum.get +" "+t.year.get  } openOr "")
    }
  }
}

import net.liftweb.sitemap.Loc.Hidden
import net.liftweb.common.Empty
object IsTutorIn
    extends IsTutorIn
    with LongKeyedMetaMapper[IsTutorIn]
    with LecturerOnlyCRUDify[Long, IsTutorIn] {

  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams
}
