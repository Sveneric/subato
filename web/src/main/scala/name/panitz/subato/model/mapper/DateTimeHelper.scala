package name.panitz.subato.model.mapper
import java.text.DateFormat
import java.util.Date
import scala.xml._
import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.util.Helpers._

import net.liftweb.common.{Full, Box}
import net.liftweb.http.{ SHtml }

import net.liftweb.util.Helpers
import net.liftweb.http.S


import net.liftweb.util.ControlHelpers
import net.liftweb.sitemap.Menu
import net.liftweb.sitemap.Loc.Hidden

class DateTimeHelper[T <: Mapper[T]](owner: T, label:()=>String) extends MappedDateTime(owner) {

    override def parse(input: String): Box[Date] = (tryo { df.parse(input) }) match {
      case f@Full(_:java.util.Date) => f
      case _ => Full(null:java.util.Date)
    }
    override def displayName = label()+" "
    override def displayHtml = Text(displayName)
    override def asHtml = Text(if (this.get != null) df.format(this.get) else "")
    val df = new java.text.SimpleDateFormat("y-M-d H:m");
    val datef = new java.text.SimpleDateFormat("yyyy-MM-dd");
    val timef = new java.text.SimpleDateFormat("HH:mm");

    override def _toForm: Box[Elem] = {
      var dateString="";
      var timeString="";
      def trySet(){
        try{
          //println("date: \""+dateString+" "+timeString+"\"");
          this.setFromAny(df.parse(dateString+" "+timeString))
        }catch{
          case e:Exception  => //println(e)
        }
      }

      Full(<span>
        {SHtml.text({if (get==null) "2050-05-10" else {val r = datef.format(get);r}}
          , x=> {dateString=x;trySet();}
          , "type" -> "date")
        }
        {SHtml.text({if (get==null) "20:15" else timef.format(get)}
          , x=> {timeString=x;trySet()}
          , "type" -> "time")
        }
        </span>
      )
    }
}
