package name.panitz.subato.model.mapper

import scala.xml.Text
import net.liftweb.common._
import net.liftweb.mapper._

private[model] class UserInGroup extends LongKeyedMapper[UserInGroup] with IdPK {
  def getSingleton = UserInGroup

  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }
	
  object group extends MappedLongForeignKey(this, Group) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(Group.findAll.map(g => (g.id.get, g.name.get)))
    override def asHtml() = {
      Text(Group.find(By(Course.id, this.get)) map {_.name.get } openOr "")
    }
  }
}

private[model] object UserInGroup extends UserInGroup with LongKeyedMetaMapper[UserInGroup]
    with AdminOnlyCRUDify[Long, UserInGroup] {
}
