package name.panitz.subato.model.mapper

import name.panitz.subato.XmlConfigDependency
import name.panitz.subato.Encrypt
//import de.hs_rm.cs.smlt.lib.SWrapperL10n
import scala.xml.Text
import net.liftweb.http.LiftRules
import scala.xml.Elem
import net.liftweb.http.S
import _root_.name.panitz.subato.{Config, XmlConfig}
import _root_.net.liftweb.mapper._
import _root_.net.liftweb.util._
import _root_.net.liftweb.common._
import net.liftweb.ldap.{LDAPProtoUser, MetaLDAPProtoUser, LDAPVendor}
import net.liftweb.sitemap.Loc.Hidden

//private[model]
class User extends LDAPProtoUser[User] {
  def getSingleton = User
}

//private[model]
object User extends User
    with MetaLDAPProtoUser[User]
    with Logger
//    with SWrapperL10n
    with XmlConfigDependency
{

  def loc(s:String)=s

/*  override def loginXhtml : Elem =
    <form method="post" action={S.uri}>
      <table>
        <tr>
          <td colspan="2">{loc("loginExplanation")}</td>
       </tr>
       <tr>
         <td>{loc("username")}</td><td><user:name /></td>  
       </tr>  
       <tr>
         <td>{loc("password")}</td><td><user:password /></td>
       </tr>  
       <tr>  
         <td> </td><td><user:submit /></td>
       </tr>  
     </table>  
  </form>*/
	
  override def loginErrorMessage: String = "'%s' is not a valid user or password does not match"
  override def ldapUserSearch: String = "(&(objectclass=person)(uid=%s))"

  override def rolesNameRegex: String = ".*cn=(.[^,]*),.*"
  override def rolesSearchFilter: String = "(&(objectclass=posixGroup)(gidNumber=%s))"

  private def configLogin(user:String, password:String) ={
    val epassword = Encrypt.encrypt(password)
    //println(epassword)
    //println(config.users)
    config.users.contains(user, epassword)
  }
	
  //hide the login page from the menu
  override def loginMenuLocParams = Hidden :: super.loginMenuLocParams
	
  override def ldapLogin(username: String, password: String):Boolean = {
    //def _getUserAttributes(dn: String) = ldapVendor.attributesFromDn(dn)
    def redirect() {
      val uri = S.cookieValue("origUri").openOr(homePage)
      S.redirectTo(uri)
    }
		
    lazy val users = ldapVendor.search(ldapUserSearch.format(username))
    if (configLogin(username, password)) {
      logUserIdIn(username)
      redirect()
    } else if (users.size >= 1 && ldapVendor.bindUser(users(0), password)) {
//      val completeDn = users(0) + "," + ldapVendor.parameters().get("ldap.base").getOrElse("")
      logUserIdIn(username)

//      bindAttributes(_getUserAttributes(completeDn))

//      setRoles(completeDn, ldapVendor)
      redirect()
    } else return false

    return true
  }
	
	
/*  override def dbTableName = "users"*/
  override def screenWrap =
    Full(<lift:surround with="default" at="content">
      <div><lift:Loc.loginexplanation/></div>
      <lift:bind/>
      {name.panitz.subato.model.MapperSurveys.currentActiveSurveysPage}
      </lift:surround>)
/*  override def fieldOrder = List(id, firstName, lastName, email, locale, timezone, password, superUser)
  override def skipEmailValidation = true
 */	
  def isAdmin
  = loggedIn_? &&
      (Admin.isAdmin(currentUserId.openOrThrowException(null))
                 ||currentUserId.openOrThrowException(null)=="admin")
	
  def isLecturer = loggedIn_? && (IsLecturerIn.findAll(By(IsLecturerIn.uid, currentUserId.openOrThrowException(null) )) match {
    case Nil => isAdmin
    case _ => true
  })

  def isTutor = loggedIn_? && (IsTutorIn.findAll(By(IsTutorIn.uid, currentUserId.openOrThrowException(null) )) match {
    case Nil => isLecturer
    case _ => true
  })


  def isLecturerIn(courseId:Long) = loggedIn_? &&
  (IsLecturerIn.find(By(IsLecturerIn.uid, currentUserId.openOr( "")), By(IsLecturerIn.course, courseId)) match {
    case Full(_) => true
    case _ => isAdmin
  })
}

