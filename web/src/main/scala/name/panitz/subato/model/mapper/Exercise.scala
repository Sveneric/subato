package name.panitz.subato.model.mapper

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.model.{Language, MapperModelDependencies, SubmissionMode}
import name.panitz.subato.XmlConfigDependency
import _root_.net.liftweb.mapper._
import java.io.File

import name.panitz.subato.model.SubmissionMode.SubmissionMode
//import java.text.DateFormat
import java.util.Date
import net.liftweb.common.{Full, Box}
import net.liftweb.http.{S, RedirectResponse}
import net.liftweb.sitemap.Loc
import net.liftweb.util.ControlHelpers
import scala.xml.Text

import net.liftweb.common.Empty
import net.liftweb.mapper.CRUDify

class Exercise extends LongKeyedMapper[Exercise]
  //  with MapperModelDependencies
  with IdPK
  with ControlHelpers
  with SWrapperL10n
  with XmlConfigDependency {

  def getSingleton = Exercise

  def this(cId: Long, lang: String, name: String, descr: String, txt: String, fqn: String, solTmpl: String, solution: String, mode: SubmissionMode = SubmissionMode.Graded, testFiles: String, teId: Long) {
    this()
    this.solution.set(solution)
    this.language.set(Language.withNameOpt(lang) getOrElse (Language.Java))
    this.name.set(name)
    this.description.set(descr)
    this.course.set(cId)
    this.testExecutor.set(teId)
    this.exerciseText.set(txt)
    this.solutionFQCN.set(fqn)
    this.solutionTemplate(solTmpl)
    this.owner.set(currentUser.uid.openOr(""))
    this.submissionMode.set(mode)
    this.testFiles.set(testFiles)
  }

  def this(cId: Long, lang: String, extension: String, name: String, descr: String, txt: String, fqn: String, solTmpl: String, solution: String, testFiles: String, teId: Long) {
    this(cId, lang, name, descr, txt, fqn, solTmpl, solution, SubmissionMode.Graded, testFiles, teId)
    this.theFileExtention.set(extension)
  }

  object filename extends UploadField(this, new File(Exercise.exBaseDir)) {
    override def displayName = locStr("exerciseFilenameDescription")
  }

  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true

    override def displayName = locStr("class")

    override def validSelectValues = {

      _root_.name.panitz.subato.model.LdapCurrentUser.uid.map(user =>
        for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user))) yield {
          val courseId = iti.course.get;
          val Full(name) = Course.find(courseId).map(_.name.get)

          (courseId, name)
        }
      )
    }

    /*
    override def asHtml() = {
      Text(Course.find(By(Course.id, this.is)) map (_.name.is) openOr "")
    }*/
  }

  object testExecutor extends MappedLongForeignKey(this, TestExecutor) {
    override def dbIndexed_? = true

    override def displayName = "testExecutor"

    override def validSelectValues = {

      Full(TestExecutor.findAll.map(x => {
        (x.id.get, x.name.get)
      }))


    }
  }


  object owner extends MappedPoliteString(this, 100) {
    override def displayName = locStr("owner")
  }

  object name extends MappedPoliteString(this, 100) {
    override def displayName = locStr("name")

    override def dbIndexed_? = true

    override def asHtml = Text(get)
  }

  object description extends MappedPoliteString(this, 300) {
    override def displayName = locStr("description")
  }

  object exerciseText extends MappedWysiwygEditor(this, () => locStr("exerciseText")) {
    override def displayName = " for java code use: <pre class=\"brush: java\">"
  }

  object solutionTemplate extends MappedTextarea(this, 100 * 1000) {
    override def displayName = locStr("solutionTemplate")
  }

  object solution extends MappedTextarea(this, 100 * 1000) {
    override def displayName = locStr("solution")
  }

  object solutionIsPublic extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object lambdaUserAllowed extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object language extends MappedEnum(this, Language) {
    override def defaultValue = Language.Java
  }

  object submissionMode extends MappedEnum(this, SubmissionMode) {
    override def defaultValue = SubmissionMode.Graded
  }

  //TODO: date widget
  /*  object deadline extends MappedDateTime(this) {
      val df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, S.locale)
      override def parse(input: String): Box[Date] = (tryo { df.parse(input) }) match {
        case f@Full(_) => f
        case _ => Full(null)
      }
      override def displayName = "(currently ignored) deadline formatted like " + df.format(new Date) + " or empty if there is no deadline"
      override def displayHtml = Text(displayName)
      override def asHtml = Text(if (this.is != null) df.format(this.is) else "")
    }*/

  object attempts extends MappedInt(this) {
    override def displayName = locStr("attempts")
  }

  /**
    * Fully qualified class name of the class the student is supposed to write.
    */
  object solutionFQCN extends MappedPoliteString(this, 1000) {
    override def displayName = locStr("fqcn")
  }

  object testFiles extends MappedTextarea(this, 100 * 1000) {
    override def displayName = "testFiles"
  }

  object theFileExtention extends MappedPoliteString(this, 25) {
    override def displayName = locStr("fileExtention")
  }

  override def delete_! = {
    results.foreach { res =>
      res.delete_!
    }
    /*    cards.foreach { res =>
          res.delete_!
        }*/

    super.delete_!
  }

  def results = Submission.findAll(By(Submission.exercise, this))

  //  def cards = MultipleCard.findAll(By(MultipleCard.exercise, this))
  def currentUser = _root_.name.panitz.subato.model.LdapCurrentUser

  def resourceFiles = ResourceFile.findAll(By(ResourceFile.exercise, id.get))
}

object Exercise extends Exercise
  with LongKeyedMetaMapper[Exercise]
  with CRUDify[Long, Exercise]
  with LecturerOnlyCRUDify[Long, Exercise]
  with SWrapperL10n {

  //
  override def viewId: String = id + "/" + 0

  //show only these fields when listing all exercises
  override def fieldsForList = List(name, description, course)

  //override edit and create templates to add multipart="true" for file upload
  override def _editTemplate = {
    <lift:crud.edit form="post" multipart="true">
      {super._editTemplate.child}
    </lift:crud.edit>
  }

  override def _createTemplate = {
    <lift:crud.create form="post" multipart="true">
      {super._createTemplate.child}
    </lift:crud.create>
  }

  //editing or deleting allowed?
  private def deleteEditAllowed(b: Box[Exercise]) = b match {
    case Full(e) =>
      var x = {
        currentUser.uid match {
          case Full(n) =>
            n == e.owner.get
          case _ =>
            false
        }
      }
      x || User.isLecturerIn(e.course.get)
    case _ => false
  }

  private def errorAndRedirect() = {
    S.error(locStr("accessDenied"))
    new RedirectResponse(S.referer openOr "/", null)
  }

  override def showAllMenuLoc = Empty

  //override menu loc params to disable deleting and editing if the user isn't allowed to
  override def deleteMenuLocParams =
    new Loc.IfValue[Exercise](deleteEditAllowed, errorAndRedirect _) ::
      super.deleteMenuLocParams

  override def editMenuLocParams =
    new Loc.IfValue[Exercise](deleteEditAllowed, errorAndRedirect _) ::
      super.editMenuLocParams

  //L10n
  override def createMenuName = locStr("createExercise")

  override def showAllMenuName = locStr("listExercises")

  override def editMenuName = locStr("editExercises")

  override def deleteMenuName = locStr("deleteExercise")

  override def viewMenuName = locStr("viewExercise")

  override def displayName = locStr("exercise")


  //callbacks for uploading new jars
  val baseDir = config.baseDir
  val exBaseDir = config.exBaseDir

  private def extractTests(ex: Exercise) {
    val exDir = exBaseDir + ex.id.get + "/"
    val exDirFile = new java.io.File(exDir)
    if (!exDirFile.exists) exDirFile.mkdir
    if (ex.filename.get != ex.filename.was) { //if the file is new
      val exDir = exBaseDir + ex.id.get + "/"
      val testsJar = exBaseDir + ex.filename.get
      //TODO this needs to be re implemented ????
      //new Java().extractTestsJar(testsJar, exDir)
      // removing classfile for solution class from extracted test jarfile
      // otherwise this would be tested for empty submissions
      if (ex.solutionFQCN.get != null && !ex.solutionFQCN.get.trim.isEmpty) {
        val solClass = ex.solutionFQCN.get.trim
        val solFileName = exDir + "/test-classes/" + solClass.replaceAll("\\.", "/") + ".class"
        new File(solFileName).delete
      }
    }
  }

  private def deleteOldJar(ex: Exercise) {
    new File(exBaseDir + ex.filename.was).delete()
  }

  override def afterCreate = extractTests _ :: super.afterCreate

  override def afterUpdate = extractTests _ :: deleteOldJar _ :: super.afterUpdate


}
