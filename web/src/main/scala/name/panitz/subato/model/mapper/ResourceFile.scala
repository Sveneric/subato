package name.panitz.subato.model.mapper

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.model.MapperModelDependencies

import scala.xml.Text
import net.liftweb.common._
import net.liftweb.mapper.{MappedBoolean, _}
import net.liftweb.common.{Box, Full}
import net.liftweb.http.{RedirectResponse, S}
import net.liftweb.sitemap.Loc
import net.liftweb.util.ControlHelpers
import net.liftweb.sitemap.Menu


class ResourceFile extends LongKeyedMapper[ResourceFile]
  with MapperModelDependencies
  with IdPK
  with ControlHelpers
  with SWrapperL10n {

  def getSingleton = ResourceFile

  object name extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }

  object description extends MappedPoliteString(this, 100)

  object isSrcFile extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object exercise extends MappedLongForeignKey(this, Exercise) {
    override def dbIndexed_? = true

    override def validSelectValues = {
      currentUser.uid.map(user => {
        val ll: List[List[(Long, String)]] =
          for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user))) yield {
            val courseId = iti.course.get;
            for (ex <- Exercise.findAll(By(Exercise.course, courseId))) yield {
              val exId = ex.id.get;
              val Full(name) = Exercise.find(exId).map(_.name.get)
              (exId, name)
            }
          }
        ll.flatten
      }
      )
    }

    override def asHtml() = {
      Text(Exercise.find(By(Exercise.id, this.get)) map {
        _.name.get
      } openOr "")
    }
  }

}

object ResourceFile extends ResourceFile
  with LongKeyedMetaMapper[ResourceFile]
  with LecturerOnlyCRUDify[Long, ResourceFile]
  with SWrapperL10n {

  //  override def fieldsForList = List(name, exercise)

  //editing or deleting allowed?
  private def deleteEditAllowed(b: Box[ResourceFile]) = b match {
    case Full(e) => User.isLecturerIn(exercises.find(e.exercise.get).openOrThrowException("").course.id)
    case _ => false
  }

  private def errorAndRedirect() = {
    S.error(locStr("accessDenied"))
    new RedirectResponse(S.referer openOr "/", null)
  }

  override def showAllMenuLoc = Empty

  override def createMenuLoc: Box[Menu] = Empty


  //override menu loc params to disable deleting and editing if the user isn't allowed to
  override def deleteMenuLocParams =
    new Loc.IfValue[ResourceFile](deleteEditAllowed, errorAndRedirect _) ::
      super.deleteMenuLocParams

  override def editMenuLocParams =
    new Loc.IfValue[ResourceFile](deleteEditAllowed, errorAndRedirect _) ::
      super.editMenuLocParams

  //L10n
  override def createMenuName = locStr("createResourceFile")

  override def showAllMenuName = locStr("listResourceFile")

  override def editMenuName = locStr("editResourceFile")

  override def deleteMenuName = locStr("deleteResourceFile")

  override def viewMenuName = locStr("viewResourceFile")

  override def displayName = locStr("ResourceFile")


}
