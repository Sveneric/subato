package name.panitz.subato.model.mapper

import java.util.Date
import net.liftweb.mapper._
import name.panitz.subato.SWrapperL10n
import net.liftweb.common.Full
import net.liftweb.common.Empty
import net.liftweb.sitemap.Loc.If

import scala.xml.Text

class Submission extends LongKeyedMapper[Submission] with IdPK with SWrapperL10n {
  def getSingleton = Submission
	
  def this(time:Date, exerciseId:Long, uid:String, solution:String, graded:Boolean,courseId:Long,termId:Long) {
    this()
    this.datetime.set(time)
    this.exercise.set(exerciseId)
    this.uid.set(uid)
    this.solution.set(solution)
    this.graded.set(graded)
    this.course.set(courseId)
    this.term.set(termId)
  }
	
  object graded extends MappedBoolean(this){
    override def _toForm = Empty
  }
  object datetime extends MappedDateTime(this){
    override def _toForm = Full(<span>{this.get}</span>)
  }
  object exercise extends MappedLongForeignKey(this, Exercise) {
    override def dbIndexed_? = true
    override def _toForm = Empty
  }
  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
    override def _toForm = Full(<span>{this.get}</span>    )
  }

  object directory extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
    override def _toForm = Full(<span>{this.get}</span>    )
  }

  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def _toForm = Empty
  }
  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def _toForm = Empty
  }
  object solution extends MappedTextarea(this, 100*1000){
    override def _toForm = Full(<pre>{this.get}</pre>)
  }

  object tests extends MappedInt(this){
    override def _toForm = Empty
  }
  object failures extends MappedInt(this){
    override def _toForm = Empty
  }
  object errors extends MappedInt(this){
    override def _toForm = Empty
  }
  object stylecheckErrors extends MappedInt(this){
    override def _toForm = Empty
  }
  object allocations extends MappedInt(this){
    override def _toForm = Empty
  }
  object frees extends MappedInt(this){
    override def _toForm = Empty
  }
  object score extends MappedLong(this){
    override def _toForm = Empty
  }

  object granted extends MappedBoolean(this){
    override def _toForm = Empty
  }

  object compilationFailed extends MappedBoolean(this){
    override def _toForm = Empty
  }
  object compilationOutput extends MappedPoliteString(this, 100*1000) {
    override def _toForm = Empty
  }
  object styleCheckOutput  extends MappedWysiwygEditor(this, ()=>locStr("unittests")) {
    override def _toForm = Empty
  }

  object messages extends MappedPoliteString(this, 100*1000) {
    override def _toForm = Empty
  }
  object unittests extends MappedWysiwygEditor(this, ()=>locStr("unittests")) {
    override def _toForm = Empty
  }
  object comment extends MappedWysiwygEditor(this, ()=>locStr("comment")) 

}
import net.liftweb.http.RedirectResponse

object Submission extends Submission
	with LongKeyedMetaMapper[Submission]
    with TutorOnlyCRUDify[Long,Submission]{

  override def editMenuLocParams =  super.editMenuLocParams

  override def showAllMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login")) :: super.showAllMenuLocParams
  override def createMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.createMenuLocParams
  override def deleteMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.deleteMenuLocParams

}
