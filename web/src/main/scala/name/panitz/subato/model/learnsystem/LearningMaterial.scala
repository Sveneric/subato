package name.panitz.subato.model.learnsystem

import net.liftweb.common.{Box, Full}
import net.liftweb.mapper.By
import name.panitz.subato.model.{Card, Finder, Id, MapperModelDependencies}
import name.panitz.subato.model.mapper.learnsystem.{LearningMaterial => LM, MasteryLevel, SubscribedLearningMaterial => SLM, FlashcardCourseDateFormatter}
import java.time.LocalDateTime

import MasteryLevel._
trait LearningMaterial extends Id {
  def flashcardCourse: Box[FlashcardCourse]
  def flashcard: Box[Card]
  def flashcardId: Long
  def save: Unit
  def delete: Unit
  def subscribe(uid: String): SubscribedLearningMaterial
  def isExamMaterial_? : Boolean
}

trait LearningMaterials extends Finder[LearningMaterial] {
  def of(flashcardCourseId: Long): Seq[LearningMaterial]
  def numberFor(flashcardCourseId: Long): Int
  def createAndSaveFrom(flashcardCourse: Long, card: Long, isExamMaterial: Boolean = false): LearningMaterial
}

trait SubscribedLearningMaterial extends Id {
  def learningMaterial: Box[LearningMaterial]
  def learningMaterialId: Long
  def masteryLevel: MasteryLevel
  def degradeMasteryLevel: Unit
  def upgradeMasteryLevel: Unit

  def delete: Unit = {}
  def save: Unit = {}
  def lastLearningSession: LocalDateTime = LocalDateTime.now
  def learningCompleted_? : Boolean = masteryLevel == End
  def isExamMaterial_? : Boolean = {
    learningMaterial match {
      case Full(lm) => lm.isExamMaterial_?
      case _        => false
    }
  }
}

trait SubscribedLearningMaterials {
  def of(user: String, flashcardCourseId: Long): List[SubscribedLearningMaterial]
  def createAndSaveFrom(uid: String, fcc: Long): Seq[SubscribedLearningMaterial]
  def delete(uid: String, fcc: Long): Unit
  def linkedTo(learningMaterialId: Long): Seq[SubscribedLearningMaterial]
}

class MapperLearningMaterial(private val material: LM) extends LearningMaterial with MapperModelDependencies {
  
  override def flashcardCourse: Box[FlashcardCourse] = {
    flashcardCourses.find(material.flashcardCourse.get)
  }

  override def isExamMaterial_? : Boolean = material.isExamMaterial.get

  override def flashcard: Box[Card] = {
    multipleCards find (material.flashcard.get)
  }

  override def delete: Unit = material.delete_!

  override def subscribe(uid: String): SubscribedLearningMaterial = {
    val initialLevel = if (isExamMaterial_?) MasteryLevel.NotExaminedYet else One
    val result = new MapperSubscribedLearningMaterial(new SLM(uid, id, material.flashcardCourse.get, initialLevel))
    result.save
    result
  }


  override def flashcardId: Long = material.flashcard.get

  override def id: Long = material.id.get

  override def save: Unit = material.save


}


object MapperLearningMaterials extends LearningMaterials {

  override def find(id: Long): Box[LearningMaterial] = LM.find(id).map(new MapperLearningMaterial(_))

  override def findAll: List[LearningMaterial] = LM.findAll.map(new MapperLearningMaterial(_))

  override def numberFor(flashcardCourseId: Long): Int = this.of(flashcardCourseId).size

  override def of(flashcardCourseId: Long): Seq[LearningMaterial] = {
    LM.findAll(By(LM.flashcardCourse, flashcardCourseId)).map(new MapperLearningMaterial(_))
  }

  override def createAndSaveFrom(flashcardCourse: Long, card: Long, isExamMaterial: Boolean = false): LearningMaterial = {
    val result = new MapperLearningMaterial(new LM(flashcardCourse, card, isExamMaterial))
    result.save
    result
  }


}

class MapperSubscribedLearningMaterial(private val material: SLM) extends SubscribedLearningMaterial with MapperModelDependencies {

  override def id: Long = material.id.get

  override def learningMaterial: Box[LearningMaterial] = {
    learningMaterials find material.learningMaterial.get
  }

  override def masteryLevel: MasteryLevel = material.masteryLevel.get

  override def delete: Unit = material.delete_!

  override def save: Unit = {
    updateLastLearningSession
    material.save
  }

  override def degradeMasteryLevel: Unit = {
    def degradeMasteryLevelOf(lm: LearningMaterial): Unit = {
      def isProgrammingMaterial_? = lm.flashcard map { _.isProgrammingCard_? } getOrElse false

      if (lm.isExamMaterial_?) {
        material.masteryLevel.set(ExamFailed)
        save
      } else if ((!learningCompleted_?) && (!isProgrammingMaterial_?) ) {
        material.masteryLevel.set(One)
        save
      }
    }

    learningMaterial match {
      case Full(lm_) => degradeMasteryLevelOf(lm_)
      case _         => 
    }    
  }

  override def upgradeMasteryLevel: Unit = {
    def upgradeMasteryLevelOf(lm: LearningMaterial): Unit = {
      if (lm.isExamMaterial_?) {
        material.masteryLevel.set(End)
      } else {
        val nextLvL = material.masteryLevel.get match {
          case One   => Two
          case Two   => Three
          case Three => Four
          case Four  => Five
          case _     => End
        }
        material.masteryLevel.set(nextLvL)
      }
      save
    }

    learningMaterial match {
      case Full(lm_) => upgradeMasteryLevelOf(lm_)
      case _         => 
    }  
  }

  override def learningMaterialId: Long = material.learningMaterial.get

  override def lastLearningSession: LocalDateTime = {
    LocalDateTime.parse(material.lastLearningSession.get , FlashcardCourseDateFormatter.get)
  }

  private def updateLastLearningSession: Unit = {
    val formattedDatetime = LocalDateTime.now.format(FlashcardCourseDateFormatter.get)
    material.lastLearningSession.set(formattedDatetime)
  }

}

object MapperSubscribedLearningMaterials extends SubscribedLearningMaterials {

  override def of(user: String, flashcardCourseId: Long): List[SubscribedLearningMaterial] = {
    SLM.findAll(By(SLM.subscriber, user), By(SLM.flashcardCourse, flashcardCourseId)).map(new MapperSubscribedLearningMaterial(_))
  }

  override def createAndSaveFrom(uid: String, fcc: Long): Seq[SubscribedLearningMaterial] = {
    for {
      lm <- MapperLearningMaterials of fcc
      initialLevel = if (lm.isExamMaterial_?) MasteryLevel.NotExaminedYet else One
      slm = new MapperSubscribedLearningMaterial(new SLM(uid, lm.id, fcc, initialLevel))
      _ = slm.save
    } yield slm
  }

  def linkedTo(learningMaterialId: Long): Seq[SubscribedLearningMaterial] = {
    SLM.findAll(By(SLM.learningMaterial, learningMaterialId)) map { new MapperSubscribedLearningMaterial(_)}
  }

  override def delete(uid: String, fcc: Long): Unit = {
    for (slm <- MapperSubscribedLearningMaterials of (uid, fcc)) slm.delete
  }

}
