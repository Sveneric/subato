package name.panitz.subato.model.mapper

import java.util.Date
import net.liftweb.mapper._
import name.panitz.subato.SWrapperL10n
import net.liftweb.common.Full
import net.liftweb.common.Empty
import net.liftweb.sitemap.Loc.If


class Points extends LongKeyedMapper[Points] with IdPK with SWrapperL10n {
  def getSingleton = Points
	
  def this(uid:String,points:Int,lecturer:String,time:Date, exerciseSheet:Long,courseId:Long,termId:Long) {
    this()
    this.datetime.set(time)
    this.exerciseSheet.set(exerciseSheet)
    this.uid.set(uid)
    this.lecturer.set(lecturer)
    this.course.set(courseId)
    this.term.set(termId)
    this.points.set(points)
  }

  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
    override def _toForm = Full(<span>{this.get}</span>    )
  }
  object lecturer extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
    override def _toForm = Full(<span>{this.get}</span>    )
  }
  object datetime extends MappedDateTime(this){
   override def _toForm = Full(<span>{this.get}</span>)
  }

  object exerciseSheet extends MappedLongForeignKey(this, ExerciseSheet) {
    override def dbIndexed_? = true
    override def _toForm = Empty
  }
  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def _toForm = Empty
  }
  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def _toForm = Empty
  }
  object points extends MappedInt(this)

}
import net.liftweb.http.RedirectResponse
object Points extends Points
	with LongKeyedMetaMapper[Points]
    with TutorOnlyCRUDify[Long,Points]{

  override def editMenuLocParams =  super.editMenuLocParams

  override def showAllMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login")) :: super.showAllMenuLocParams
  override def createMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.createMenuLocParams
  override def deleteMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.deleteMenuLocParams

}
