package name.panitz.subato.model.mapper

import scala.xml._
import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.util.Helpers._

import java.text.DateFormat
import java.util.Date

import net.liftweb.common.{Full, Box}
import net.liftweb.http.{ SHtml }

import java.time.LocalDateTime


private[model] class Attendance extends LongKeyedMapper[Attendance] with IdPK {
  def getSingleton = Attendance

  object uid extends MappedPoliteString(this, 1000)
	
  object coursePage extends MappedLongForeignKey(this, CoursePage) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(CoursePage.findAll.map(x => (x.id.get, x.course.get+" " +x.term.get)))
    override def asHtml() = {
      Text(Group.find(By(Group.id, this.get)) map {_.name.get} openOr "")
    }
  }
  object datum extends MappedDateTime(this) {
/*    override def parse(input: String): Box[java.util.Date] = (tryo { datef.parse(input) }) match {
      case f@Full(_) => f
      case _ => Full(null)
    }
    override def displayName = "time"
    override def displayHtml = Text(displayName)
    override def asHtml = Text(if (this.get != null) datef.format(this.get) else "")
    val datef = new java.text.SimpleDateFormat("yyyy-MM-dd");

    override def _toForm: Box[Elem] = {
      var timeString="";
      def trySet(){
        try{
          this.setFromAny(datef.parse(timeString))
        }catch{
          case e:Exception  => println(e)
        }
      }
      Full(<span>
        {SHtml.text({if (get==null) "10:15" else datef.format(this.get)}
          , x=> {timeString=x;trySet()}
          , "type" -> "date")
        }
        </span>
      )
    }*/
  }
}

import net.liftweb.sitemap.Loc.Hidden

object Attendance extends Attendance with LongKeyedMetaMapper[Attendance]
    with LecturerOnlyCRUDify[Long, Attendance] {
  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams

}
