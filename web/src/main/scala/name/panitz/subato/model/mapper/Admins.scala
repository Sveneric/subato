package name.panitz.subato.model

import net.liftweb.mapper.By
import net.liftweb.common.Full
import name.panitz.subato.model.mapper.{Admin => MA}

trait Admins {
  def contains(uid:String):Boolean
  def insert(uid:String)
  def deleteAll(uid:String)
}

object MapperAdmins extends Admins {
  override def contains(uid:String) = MA.findAll(By(MA.uid, uid)) match {
    case Nil => false
    case _ => true
  }
  override def insert(uid:String) {
    val admin = MA.create
    admin.uid.set(uid)
    admin.save()
  }
  override def deleteAll(uid:String) {
    MA.findAll(By(MA.uid, uid)) foreach { _.delete_! }
  }
}
