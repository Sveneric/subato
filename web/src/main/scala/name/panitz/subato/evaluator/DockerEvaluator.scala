package name.panitz.subato.evaluator

import java.io.InputStream
import java.util.Date
import java.util.concurrent.TimeUnit

import io.grpc.netty.{NegotiationType, NettyChannelBuilder}
import io.grpc.panitz.name.EvaluatorService.service.{EvaluatorServiceGrpc, TestResourceFile, TestSet}
import name.panitz.subato.XmlConfigDependency
import name.panitz.subato.model.Language.Language
import name.panitz.subato.model.{Exercise, Language, MapperModelDependencies, Submission, UnitTestCaseResult}
import net.liftweb.common.Logger
import net.liftweb.http.FileParamHolder

class DockerEvaluator extends Evaluator with XmlConfigDependency
  with MapperModelDependencies
  with Logger {

  override def testSolution(code: String, userDir: String, exercise: Exercise, courseId: Long, termId: Long, file: FileParamHolder, storeResult: Boolean, isApprovalCheck:Boolean): Submission = {
    init(code, "", exercise, courseId, termId)

    val time = System.currentTimeMillis

    val submission
    = submissions
      .create(new Date(time)
        , exercise
        , courseId
        , termId
        , currentUser.uid.openOr("lambdaUser")
        , code)

    val channel = NettyChannelBuilder.forAddress(sys.env("TEST_EXECUTOR_URL"), sys.env("TEST_EXECUTOR_PORT").toInt).negotiationType(NegotiationType.PLAINTEXT).build()
    val stub = EvaluatorServiceGrpc.blockingStub(channel)

    var list: List[TestResourceFile] = List()

    val elem = scala.xml.XML.loadString(exercise.testFilesXML)

    (elem \\ "testclasses" \ "class").foreach(c => {
      var pack = c \ "@package"+""
      if (pack=="") pack="."
      list = new TestResourceFile(pack +"/"+ c \ "@name" + "." + c \ "@lang", c.text) :: list
    })

    val set = new TestSet(
      list,
      code,
      fqcnToPath(exercise.solutionFQCN, exercise),
      exercise.testExecutor.image,
      exercise.hasStyleCheck,
      isApprovalCheck
    )
    val result = stub.runTest(set)
    
    channel.shutdownNow()
    channel.awaitTermination(20, TimeUnit.SECONDS)

    submission.compilationFailed = result.compilationFailed
    submission.tests = result.tests
    submission.errors = result.errors
    submission.failures = result.failures
    submission.allocations = result.allocations
    submission.frees = result.frees
    submission.compilationOutput = result.compilationOutput
    submission.stylecheckErrors = result.stylecheckErrors
    
    //Calculate score
    submission.score = 5 * (submission.tests - submission.errors - submission.failures)
    -(submission.allocations - submission.frees)
    -submission.stylecheckErrors

    var tcrs: List[UnitTestCaseResult] = Nil


    for (testCase <- result.testCases) {
      var tcMessages: scala.xml.Node = <div>
        <pre>
          <failure message={testCase.failure.get.message} type={testCase.failure.get.`type`}>
            {testCase.failure.get.text}
          </failure>
        </pre>
      </div>

      tcrs ::= UnitTestCaseResult(testCase.name, tcMessages)
    }

    submission.unittests = <div>
      {tcrs.reverse.map((t) => <div>
        <b>
          {t.testCase}
        </b>{t.messages}
      </div>)}
    </div>

    submission.styleCheckOutput = <ol>
      {result.styleCheckResults.map(result => <li>
        {result.file}
        :
        {result.line}
        :
        {result.column}
        :
        {result.message}
      </li>)}
    </ol>

    if (graded && storeResult){
      submission.save()
    }
    submission
  }

  def fqcnToPath(fqcn: String, ex: Exercise): String = 
    fqcn.trim.replaceAll("\\.", "/") + "."+ex.fileExtention
  

}
