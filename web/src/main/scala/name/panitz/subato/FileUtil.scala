package name.panitz.subato

import java.io.File

object FileUtil{
  //creates the dir the file is in and each superdir if it doesn't exist yet
  //if you want the file itself to be created as a dir you must end its name with a /
  def mkdir(file:String) {
    var dir = file
    if (dir.endsWith("/")) {
      dir = dir.slice(0, dir.length - 1)
    }
    val parts = dir.trim.split('/')
    for (i <- 0 to parts.size - 1) {
      val f = new File(parts.slice(0, i).foldLeft("/")(_ + "/" + _))
      if (!f.exists) {
	f.mkdir()
      }
    }
    if (file.endsWith("/")) {
      val f = new File(file)
      if (!f.exists) {
	f.mkdir()
      }
    }
  }

  def deleteDir(file: File):Unit= {
    val contents = file.listFiles()
    if (contents != null) for (f <- contents) deleteDir(f)
    file.delete
  }
}
