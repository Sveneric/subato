package name.panitz.subato.comet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty, Failure}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._


import net.liftweb.http.CometActor
import scala.xml.Text
import net.liftweb.util.Schedule
import net.liftweb.http.js.JsCmds.SetHtml

import name.panitz.subato.snippet.Params
import name.panitz.subato.snippet.ParamsImpl


class DuelComet
    extends AbstractDuelComet
    with SWrapperL10n
    with MapperModelDependencies {

  override def localSetup {
    DuelMaster ! SubscribeDuel(this)
    super.localSetup()
  }
  override def localShutdown {
    DuelMaster ! UnsubDuel(this)
    super.localShutdown()
  }
}

trait  AbstractDuelComet extends CometActor {
  this:ModelDependencies
      with L10n =>
  protected var duel:Box[Duel] = Empty
  var duelId:Long = 0
  override def sendInitialReq_?() : Boolean = true
  override def captureInitialReq(initialReq: Box[net.liftweb.http.Req]): Unit = {
    initialReq match {
      case Full(req) =>
        req.param("id") match{
          case Full(idstr) =>  duelId =idstr.toLong
          case _ =>
        }
      case _ => println(initialReq)
    }
    super.captureInitialReq(initialReq)
  }

  override def dontCacheRendering: Boolean=true

  import _root_.name.panitz.subato.model.Progress._
  def render = {
    duel = duels.find(duelId)
    val uid = currentUser.uid.openOrThrowException("")
    duel match{
      case Full(d) => d.renderForUser(uid)
      case _ =>  "#time" #> <b>No Duel Found</b>
    }
  }


  override def lowPriority : PartialFunction[Any, Unit] = {
    case Ticke(id) => {
      reRender()
    }
  }

}

case class Ticke(val duel:Duel)


case class SubscribeDuel(duelC : DuelComet)
case class UnsubDuel(duelC : DuelComet)

object DuelMaster extends net.liftweb.actor.LiftActor {
  private var duels : List[DuelComet] = Nil
  def messageHandler = {
    case SubscribeDuel(clk) => duels::= clk
    case UnsubDuel(clk) =>  duels = duels filter(_ != clk)
    case Ticke(duel) => {
      duels.foreach((du) => if (du.duelId==duel.id) du ! Ticke(duel) )
    }
  }
}
