package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}

import scala.xml.{NodeSeq, Text, Node, XML}

class LessonSnippet(val c:Box[Lesson])
    extends AbstractLessonSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val lesson = c

}

trait AbstractLessonSnippet{
  this:ModelDependencies
      with L10n =>

  protected val lesson:Box[Lesson]

  def edit={
    var theLesson:Lesson=null

    (lesson) match {
      case (Full(cp)) => {
        var content = cp.content

        def asHtml(s:String): Node= try{
          XML.loadString("<div>"+s+ "</div>")
        }catch{
          case e:Exception => XML.loadString("<pre><![CDATA[" +s + "]]></pre>")
        }

        def saved(){
          val mcp = mapper.Lesson.find(cp.id)
          mcp.map(c => c.content(content+"").save)
          net.liftweb.http.S.redirectTo("/CoursePage/"+cp.coursePageId)
        }
        if (cp.coursePage.map(_.isLecturerIn).openOr(false)){
          "#date *" #> Text(""+cp.date) &
          "#coursePageName *" #> cp.coursePage.map(_.name).openOr("") &
          "#description *" #> <div>
          <div>
            <script type="text/javascript" src="/static/3dpartyjscript/tiny_mce/js/tinymce/tinymce.min.js"></script>
          <script type="text/javascript">
          <xml:unparsed>
          // <![CDATA[
          tinymce.init({
	    width: 500,
	    mode: "exact",
	    elements: "mce1",
	    // theme: "advanced",
            theme: "modern",
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc"
            ],
            toolbar1: "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
            entity_encoding : "numeric",
	    entities: "38,amp,34,quot,60,lt,62,gt"});
          // ]]>
          </xml:unparsed>
          </script>
          <label for="mce1">content</label>
          </div>
          {SHtml.textarea(cp.content+"", (t)=>{content=asHtml(t)}, "id" -> "mce1")}
          </div> &
          "#submit *" #> SHtml.button(loc("save"),()=>saved,("type","submit"))
        }else{
          "#row  " #> Text("not allowed")
        }
      }
      case _ =>  "*" #> <span>{loc("noLesson")}</span>
    }
  }
}
