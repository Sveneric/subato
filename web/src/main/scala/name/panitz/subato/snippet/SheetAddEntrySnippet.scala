package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty}

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.http.{StatefulSnippet, SHtml}
import net.liftweb.common.Full
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class SheetAddEntrySnippet(e:Box[ExerciseSheet])
    extends AbstractSheetAddEntrySnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected val params = ParamsImpl
  override protected val sheet = e

  def this() {
    this(Empty)
  }
}

trait AbstractSheetAddEntrySnippet  extends StatefulSnippet{
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val sheet:Box[ExerciseSheet]

  override def dispatch = {
    case "addEntry" => addEntry 
  }

  def addEntry = sheet match {

    case Full(she) => {
      if (currentUser.isLecturerIn(she.course.id,she.term.id)) {
        var exercise:Box[Exercise] = Empty
        var number = ""
        def submitted = {
          exerciseSheetEntries.createAndSave(she.id,exercise.openOrThrowException( "no exercise specified" ).id,Integer.parseInt(number))
          redirectTo("/ListExerciseSheetEntries/"+she.id)
        }
        "#addExerciseToSheet *" #> <span>{loc("addExerciseToSheet")}</span> &
        "#sheet *" #> <span>{she.name+" ("+she.course.name+" "+she.term+")"}</span> &
        "#create *" #> SHtml.button(loc("create"),submitted _,("type","submit"))&
        "#number *" #> SHtml.text("0",(t)=>number=t)&
        "#exercise *" #> SHtml.selectObj(
          exercises.findAll(she.course.id).map(x => (x, x.name)), Empty, (e:Exercise) => exercise = Full(e)
        )
        

      }
      else  "*" #> <span>{loc("noCourseSpecified")}</span>
    }
    case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
  }
}
