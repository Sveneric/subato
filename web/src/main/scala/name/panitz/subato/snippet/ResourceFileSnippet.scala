package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class ResourceFileSnippet(val c:Box[ Exercise ])
    extends AbstractResourceFileSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractResourceFileSnippet extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
  with L10n  =>



  protected val params:Params
  protected val exercise:Box[ Exercise]
  protected def config:Config

  override def dispatch = {
    case "Form" => addForm
    case "List" => list
  }

  def filesInFolder(folderName:String):List[String] ={
    val folder = new java.io.File(folderName)
    if (!folder.isDirectory){
      List(folderName)
    }else {
      folder.list.map(f => filesInFolder(folderName+"/"+f)).toList.flatten
    }
  }

  def addForm = {
    (exercise) match {
      case Full(ex) => {
        var selectedFile : String = ""
        val files = ex.getTestFiles()
        var description: String = ""

        def submitted = {
          if (selectedFile.startsWith("src_")) {
            resourceFiles.createAndSave(selectedFile.stripPrefix("src_"),description,ex.id, true)
          } else {
            resourceFiles.createAndSave(selectedFile.stripPrefix("local_"),description,ex.id, false)
          }

          redirectTo("/ListResourceFilesForExercise/"+ex.id)
        }


        var options = (files \ "class").seq.map(x=>("src_" + (x \@ "name") + "." + (x \@ "lang"),(x \@ "name") + "." + (x \@ "lang")))

        val exDir = config.exBaseDir+"/"+ex.id
        val folderFiles = filesInFolder(exDir).map(s=>s.substring((exDir).size)).map(x => ("local_" + x,x))
        options = options ++ folderFiles

        "#exName *"    #> Text(ex.name)&
        "#fileNames"   #> SHtml.select(options, Empty,(s)=>selectedFile = s, "class" -> "myselect") &
        "#submit *"    #> SHtml.submit(locStr("submitNewResourceFile"), submitted _) &
        "#description" #> SHtml.textarea( ""
                                    , s => { description = s }
                                    , "id" -> "description"
                                    , "style" -> "height: 50px; width: 100%;"
                                    )
      }
      case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>
    }
  }

  def list = {
    (exercise) match {
      case Full(ex) => {
        "#add *" #> <a href={"/AddResourceFileForExercise/"+ex.id}>{loc("addResourceFile")}</a> &
        "#upload *" #> <a href={"/UploadFile/"+ex.id}>{loc("uploadResourceFile")}</a> &
        "#exerciseName *" #> Text(loc("exercise")+" "+ex.name+" ("+ex.course.name+")") &
        "#row *" #> ex.resourceFiles.map(resF => (
              <td>{resF.name}</td>
              <td>{resF.description}</td>
              <td><a href={"/resourcefile/edit/"+resF.id}>{loc("edit")}</a></td>
              <td><a href={"/resourcefile/delete/"+resF.id}>{loc("delete")}</a></td>
        )
        )
      }
      case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>
    }
  }

}
