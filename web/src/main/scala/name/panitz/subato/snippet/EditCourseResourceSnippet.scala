package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class EditCourseResourceSnippet(val c:Box[ CoursePage ])
    extends AbstractEditCourseResourceSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  def this(e:(String,Box[CoursePage])) = {
    this(e._2)
    this.fileName = e._1
  }
  override protected var fileName:String = null
  override protected val params = ParamsImpl
  override val coursePage = c
}

trait AbstractEditCourseResourceSnippet extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
      with L10n  =>

  protected val params:Params
  protected val coursePage:Box[ CoursePage]
  protected var fileName:String
  protected def config:Config

  override def dispatch = {
    case "editForm" => editForm
    case "deleteForm" => deleteForm
  }


  def deleteForm = {
    (coursePage) match {
      case Full(cp) => {
	val fileN = fileName.replace("____",".")
	val fqn = config.courseBaseDir+"/"+cp.course.id+"/"+fileN.replaceAll(":","/")
	val shortName = fqn.substring(fqn.lastIndexOf('/')+1)

        def solutionSubmitted = {
	  //here delete file!!!
	  new java.io.File(fqn).delete()

          redirectTo("/ListResourceFilesForCourse/"+cp.id)
        }
        def abbrechenSubmitted = {

          redirectTo("/ListResourceFilesForCourse/"+cp.id)
        }


        "#exName *" #> cp.name  &
        "#resourceFile *" #> (loc("reallyDeleteFile") +" "+shortName)  &
        "#abbrechen *" #> {
          SHtml.button(loc("abbrechen"),abbrechenSubmitted _,("type","submit"))

        } &
        "#submit *" #> {
            if (currentUser.loggedIn&&(cp.isTutorIn)){
              SHtml.button(loc("delete"),solutionSubmitted _,("type","submit"),("onclick","startSpinner()"))
            } else {<span>{locStr("cannotSubmit")}</span>}
          } 
      }
      case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>
                
    }
  }


  def editForm = {
    (coursePage) match {
      case Full(cp) => {
	val fileN = fileName.replace("____",".")
	val fqn = config.courseBaseDir+"/"+cp.course.id+"/"+fileN.replaceAll(":","/")
	val shortName = fqn.substring(fqn.lastIndexOf('/')+1)

	val bufferedSource = scala.io.Source.fromFile(fqn)
	val buffer = new StringBuffer()
	for (line <- bufferedSource.getLines) {
	  buffer.append(line)
          buffer.append("\n")
	}
	bufferedSource.close
	
        var solution = buffer.toString

        def solutionSubmitted = {
	  //here save again:
	  val writer = new java.io.FileWriter(fqn)
	  writer.write(solution)
	  writer.close

          redirectTo("/ListResourceFilesForCourse/"+cp.id)
                       
        }


        "#javascript" #>     <script language="javascript" type="text/javascript"><![CDATA[
      editAreaLoader.init(
        {id : "code"	                // textarea id
        ,syntax: "java"	                // syntax to be uses for highglBiting
        ,start_highlight: true	        // to display with highlight mode on start-up
        ,replace_tab_by_spaces: 2
        }
      );
   ]]> </script> &
        "#exName *" #> cp.name  &
        "#submit *" #> {
            if (currentUser.loggedIn&&(cp.isTutorIn)){
              SHtml.button(loc("submitSolution"),solutionSubmitted _,("type","submit"),("onclick","startSpinner()"))
            } else {<span>{locStr("cannotSubmit")}</span>}
          } &
	"#solution" #>
        SHtml.textarea(
                solution    
               , s => { solution = s }
               , "id" -> "code"
               , "style" -> "height: 450px; width: 100%;"
               )
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
        
        
    }
  }
}
 
