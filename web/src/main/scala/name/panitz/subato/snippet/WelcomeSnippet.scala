package name.panitz.subato.snippet

import name.panitz.subato.model._
import net.liftweb.common.Logger

import net.liftweb.util.Helpers._
import java.util.Date

import net.liftweb.http.S
import net.liftweb.common.Full


class WelcomeSnippet extends AbstractWelcomeSnippet
  with Logger
  with MapperModelDependencies

trait AbstractWelcomeSnippet extends _root_.name.panitz.subato.SWrapperL10n{

      

  protected val currentUser:CurrentUser
  lazy val date = new Date()

  def name = {

    currentUser.uid match {
      case Full(uid) => {
        "#time *" #> date.toString &
        "#name *" #> uid &
//Duelle scheinen ein extremes performance Problem zu haben....
/*                "#openDuels *" #> {
          val notFinished = _root_.name.panitz.subato.model.MapperDuels.findForUser(uid).filter((d) => !d.finished && d.userCanPlay(uid))
          if (notFinished.size > 0){
            <div><h4><a href="/DuelList">{loc("playYourDuels")}</a></h4></div>
          }else{<span />}
        }& */
        "#activeSurveys *" #> _root_.name.panitz.subato.model.MapperSurveys.currentActiveSurveysPage
      }
      case _ => S.redirectTo("/user_mgt/login")
    }
  }
}



