package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}

class SheetResultsSnippet(val c:Box[ ExerciseSheet ])
    extends AbstractSheetResultsSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[ExerciseSheet])) = {
    this(e._2)
    this.termId = e._1
  }

  override protected var termId:String = null
  override protected val params = ParamsImpl
  override val exerciseSheet = c

}

trait AbstractSheetResultsSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val exerciseSheet:Box[ ExerciseSheet]
  protected var termId:String

  def studentsByExercise = exerciseSheet match {
    case Full(sheet) => {
      if (currentUser.isTutorIn(sheet.course.id,termId.toLong)) {
        import net.liftweb.mapper._
        val exercises = sheet.sheetExercises.map(_.exercise)
        val users1:List[String]
          = exercises.map((ex) => submissions.find(ex,sheet.course.id,termId.toLong).map((sub)=>sub.uid)).flatten
        val users = users1.distinct

        val usersAndGroups = users.map( u =>
          exercises
            .map((ex) => (u, userInGroups.findByUid(u, sheet.course.id,termId.toLong).map(_.name).openOr("")))
        ) .map(_.head)
        .sortWith((a,b)=>
          a._2 < b._2 || (a._2 == b._2 && a._1 < b._1) //sort by group, then uid
        )

        var rows:List[scala.xml.NodeBuffer] = usersAndGroups.map( uag => {
          val (u, group) = uag
          val testResults
            = exercises
               .filter(_.isGraded)
               .map((ex) => submissions.findBest(u, sheet.course.id, ex.id,termId.toLong))
               .filter((x) => !x.isEmpty)
//            println(testResults)
          def studentLink(exId:Long, uid:String,termId:String):NodeSeq = {
//            <a href={"/SheetResults/" +exId+"/"+termId  + "/" + uid}>{uid}</a>
            <a href={"/CoursePageStudentResults/"+(coursePages.find(sheet.course.id,termId.toLong)(0).id) +"/" + uid}>{uid}</a>
          }

          def grantLink(sheet:Long, uid:String,termId:String):NodeSeq ={
            <div>
            <script>
            function ajaxcall{uid}(element) {{
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {{
                if (this.readyState == 4 <![CDATA[&&]]> this.status == 200) {{
                  var el = document.createElement( 'html' );
                  el.innerHTML=this.responseText;
                  element.innerText=el.textContent;
                  element.removeAttribute("onclick")
                }}
              }};
              xhttp.open("GET", "{"/subato/GrantExerciseSheetForStudent/" +sheet+"/"+termId  + "/" + uid}", true);
              xhttp.send();
            }}</script>
            <span onclick={"ajaxcall"+uid+"(this)"}>grant {uid}</span></div>
          }
          (<td>{studentLink(sheet.id,u,termId)}</td>
            <td>{group}</td>
            <td>{testResults.size.toString}</td>
            <td>{testResults.map((testResult) => testResult.map(_.tests).openOr(0)).sum.toString }</td>
            <td>{testResults.map(testResult => testResult.map(_.failures).openOr(0)).sum.toString }</td>
            <td>{testResults.map(testResult => testResult.map(_.errors).openOr(0)).sum.toString }</td>
            <td>{testResults.map(testResult => testResult.map((x)=>(x.allocations-x.frees)).openOr(0)).sum.toString}</td>
            <td>{testResults.map(testResult => testResult.map(_.stylecheckErrors).openOr(0)).sum.toString}</td>
            <td>{testResults.map(testResult => testResult.map(_.score).openOr(0L)).sum.toString}</td>
            <td>{
              if (currentUser.isTutorIn(sheet.course.id,termId.toLong)){
                <span>{sheet.grantButton(u)}</span>
              }else{
                <span>{sheet.isGranted(u).openOr("not granted")}</span>
              }
            }</td>)

        })
        "#row *" #> rows
      } else {
        loc("accessDenied")
      }
    }
    case _ => loc("noSuchExercise")
  }

}
