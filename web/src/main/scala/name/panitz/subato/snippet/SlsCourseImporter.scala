package name.panitz.subato.snippet

import java.time.LocalDateTime

import scala.xml.{Node, NodeSeq, XML}

import Severity._
import name.panitz.subato._
import name.panitz.subato.model._
import name.panitz.subato.model.learnsystem.FlashcardCourse
import net.liftweb.common.{Box, Empty, Full}
import net.liftweb.http.{FileParamHolder, SHtml}
import net.liftweb.util.Helpers._

object CardBuilder extends MapperModelDependencies {

  def newCardFrom(content: Node): Card = {
    val card = buildCardFrom(content)
    card.save()
    // better import statistics as well
    //  <statistic goodAnswers="100,0%" almostCorrectAnswers="0,0%" wrongAnswers="0,0%" attempts="1" averageTime="5 Sec."/> 
    //new Cards get statistics imprted
    // updated cards 

    var stats = content\"statistic"
    def statValue(s:String)=if (!stats.isEmpty) (""+(stats\\("@"+s))).toInt else 0

    var sd = statistics.createAndSave(card.id, statValue("goodAnswers"), statValue("almostCorrectAnswers"), statValue("wrongAnswers"))

    sd.averageSolutionTime.set(if (!stats.isEmpty) (""+(stats\\("@averageTime"))).toDouble else 0.0)

    sd.save

    for ((ans,i) <- (content \ "answers" \ "answer").zipWithIndex){
      val a = answers.create(card.id, i ,ans.flatMap(_.child)+"", (ans \\ "@correct").text=="true")
      a.save()
    }
    card
  }

  def updateCard(oldCard: Card, newContent: Node): Card = {
    val c = buildCardFrom(newContent)
    oldCard.deleteAnswers()
    oldCard.update(c.id, c.cardType, c.genericExerciseText, c.explanation+"", c.name, c.bloomsLevel.toString, c.topics)
    val newAnswers = (newContent \ "answers" \ "answer").zipWithIndex
    for ((ans, i) <- newAnswers) {
      val a = answers.create(oldCard.id, i, ans.flatMap(_.child)+"",(ans \\ "@correct").text=="true")
      a.save()
    }
    oldCard
  }

  type Updated = Boolean
  def createOrUpdate(content: Node): (Card, Updated) = {
    def maybeCard(uid: String): Box[Card] = {
      try {
        val found@Full(card) = multipleCards find uid.toLong
        val sec = (content \ "@sec").text
        val salt = card.salt
        if (SecureExportImport.verify(sec, uid, salt)) {
          found
        }  else Empty
      } catch {
        case _: Exception => Empty
      }
    }

    maybeCard((content \ "@uid").text) match {
      case Full(card) => (updateCard(card, content), true)
      case _          => (newCardFrom(content),      false)
    }
  }

  def extractTopics(xml: Node): Seq[String] = {
    def extractTopic(subXml: Node): String = subXml.text.trim
    for {
      x <- xml \ "topic"
      topic = extractTopic(x)
    } yield topic
  }

  def buildCardFrom(content: Node): Card = {
    val cardType =  (content \ "@type").text
    val bloomsLevel =  (content \ "@bloomsLevel").text
    val topicNodes = content \ "topics"
    var author = (content \ "author").text.trim
    if (author.isEmpty) author = currentUser.uid.getOrElse("")
    val topics = if (topicNodes.isEmpty) Seq.empty else extractTopics(topicNodes.head)
    val result = multipleCards.create(-1
      , cardType
      , (content \ "question").flatMap(_.child)
      , (content \ "explanation").flatMap(_.child)+""
      , (content \ "name").flatMap(_.child)+""
      , bloomsLevel
      , topics
    )
    result.setAuthor(author)
    if ((result.cardType == "Programming") && (content \ "exercise").nonEmpty){
      val ex = ImportExercise.createExerciseFromXML((content \ "exercise").head, -1)
      result.exerciseId = ex.id
    }
    result
  }
}

object SlsCourseImporter extends XmlConfigDependency with MapperModelDependencies {

  private def createSLSCourseFrom(xml: Node): Unit = {
    val (fcc, boxUpdated) = createSLSCourseOrUpdateFrom(xml)
    if (boxUpdated) deleteDeprecatedCards(extractValidCardIds(xml), fcc)
    val cardNodes = extractCardsNodesFrom(xml)
    for ((isExamCard, cardNode) <- cardNodes) {
      val (card, cardUpdated) = if (boxUpdated) CardBuilder.createOrUpdate(cardNode) else (CardBuilder.newCardFrom(cardNode), false)
      if (!cardUpdated) learningMaterials.createAndSaveFrom(fcc.id, card.id, isExamCard)
    }
    val alertMsg = if (boxUpdated) s"Course >>${fcc.title}<< successfully updated" else s"course >>${fcc.title}<< successfully created"
    val Full(uid) = currentUser.uid
    if (!boxUpdated) subscribedFlashcardCourses.createAndSave(uid, fcc.id)
    PrettyAlerter.setMsg(s"Import success!", alertMsg, Success)
  }

  type IsExamCard = Boolean
  private def extractCardsNodesFrom(xml: Node): Seq[(IsExamCard, Node)] = {
    val normalCards = (xml \ "cards" \ "card") map { (false, _) }
    val examCards   = (xml \ "exam" \ "card")  map { (true,  _) }
    normalCards ++: examCards
  }

  def addAbility = {
    var fileHolder : FileParamHolder = null
    def exerciseSubmitted = {
      fileHolder match {
      // An empty upload gets reported with a null mime type,
      // so we need to handle this special case
        case FileParamHolder(_, "text/xml", _, data) => {
          isCardBoxConformToSchema(data) match {
            case (true, _) => {
              val content = XML.load(fileHolder.fileStream)
              createSLSCourseFrom(content)
            }
            case (false, msg) => {
              PrettyAlerter.setMsg("Import failed!", s"Invalid content: $msg", Error)
            }
          }
        }
        case _ => {
          PrettyAlerter.setMsg("Import failed!", "Invalid receipt attachment", Error)
        }
       }
    }

    "#slsCourseImport *" #> <div><lift:Loc.importcoursefromxml />{SHtml.fileUpload(fileHolder = _ )}</div> &
    "#file *" #> SHtml.submit("OK", () => exerciseSubmitted)
  }

  type Updated = Boolean
  private def createSLSCourseOrUpdateFrom(content: Node): (FlashcardCourse, Updated) = {
    val Full(uid) = currentUser.uid
    def maybeFlashcardCourse(): Box[FlashcardCourse] = {
      val id = (content \ "@uid").text
      try {
        val found@Full(fcc)  = flashcardCourses find id.toLong
        val sec    = (content \ "@sec").text
        if ((fcc.ownerId == uid) && SecureExportImport.verify(sec, id, fcc.salt)) {
          found
        } else Empty
      } catch {
        case _: Exception => Empty
      }
    }

    val courseTitle = (content \ "boxName").text.trim
    val topicNodes = content \ "topics"
    val topics = if (topicNodes.isEmpty) Seq.empty else extractTopics(topicNodes.head)
    val courseDescription = (content \ "description").flatMap(_.child) + ""
    lazy val maybeCourse = maybeFlashcardCourse
    var author = (content \ "author").text.trim
    if (author.isEmpty) author = currentUser.uid.getOrElse("")

    if(((content \ "@update").text == "true") && (maybeCourse.isDefined)) {
      val oldCourse@Full(fcc) = maybeCourse
      fcc.update(courseTitle, courseDescription, topics)
      (fcc, true)
    } else {
      val now = LocalDateTime.now
      val fcc = flashcardCourses.createAndSave(courseTitle, true, uid, courseDescription, topics, now, now)
      fcc.setAuthor(author)
      fcc.save
      (fcc, false)
    }
  }

  private def extractValidCardIds(xml: Node): Seq[Long] = {
    var result = Seq.empty[Long]
    for(cardNode <- xml \ "cards" \ "card") {
      try {
        result = (cardNode \ "@uid").text.toLong +: result
      } catch {
        case _: Exception =>
      }
    }
    result
  }

  private def extractTopics(content: Node): Seq[String] = {
    def extractTopic(subXml: Node): String = subXml.text
    val result = for {
      x <- content \ "topic"
      topic = extractTopic(x)
    } yield topic
    result filter { _.nonEmpty }
  }

  private def deleteDeprecatedCards(actualCardIds: Seq[Long], fcc: FlashcardCourse): Unit = {
    val oldCards = fcc.cards filter {x => !actualCardIds.contains(x.id)}
    oldCards foreach {_.delete}
  }

  private def isCardBoxConformToSchema(byteArray: Array[Byte]): (Boolean, String) = {
    /*val source = new stream.StreamSource(new ByteArrayInputStream(byteArray))
    try {
      val schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File("./webapps/subato/cardBox.xsd"))
      val validator = schema.newValidator()
      validator validate source
      (true, "")
    } catch {
      case exc: Exception => (false, s"${exc.getMessage}")
     }*/
    (true, "")
  }

}
