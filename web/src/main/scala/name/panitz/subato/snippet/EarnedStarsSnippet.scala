package name.panitz.subato.snippet
import net.liftweb.util.Helpers._
import net.liftweb.util.CssSel

import name.panitz.subato.model._

class EarnedStarsSnippet extends MapperModelDependencies {
  def currentUid(): String = currentUser.uid.getOrElse("")

  def showEarnedStars: CssSel = {
    val maybeProfile = learningProfiles.findOrCreate(currentUid)
    val earnedStars = maybeProfile match {
      case Some(profile) => profile.earnedStars
      case _             => 0
    }
    "#earned-stars *" #> earnedStars
  }
}
