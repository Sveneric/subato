package name.panitz.subato.snippet

import java.time.{LocalDateTime => LDT}
import java.time.temporal.{ChronoUnit => CU}

import scala.xml.{Elem, Text}

import name.panitz.subato._
import name.panitz.subato.model._
import name.panitz.subato.model.Progress._
import name.panitz.subato.model.learnsystem.{FlashcardCourse, SubscribedFlashcardCourse => SFC, StringUtils => SU, EphemeralSubscribedFlashcardCourse => ESFC}
import net.liftweb.common.{Box, Empty, Full, Logger}
import net.liftweb.http.{S, SHtml, SessionVar, StatefulSnippet}
import net.liftweb.mapper.By
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._

class SlsCourseSnippet(val fcc: Box[FlashcardCourse])
    extends AbstractSlsCourseSnippet
    with SWrapperL10n
    with MapperModelDependencies {
  def this() {
    this(Empty)
  }

  def flashcardCourseId: Long = {
    try{
      fcc match {
        case Full(c) => c.id
        case _       => -1
      }
    }catch{
      case _ : ClassCastException => -1
    }
  }

  override protected val params     = ParamsImpl
  override protected val subscription = {
    if (flashcardCourseId == -1) {
      Empty
    } else if (fcc.map(x => x.isPublic_? || (x.ownerId == currentUid())).getOrElse(false)) {
      subscribedFlashcardCourses getFor (currentUid, flashcardCourseId) match {
        case s@Full(_) => s
        case _         => Full(ESFC(flashcardCourseId))
      }
    } else {
      Empty
    }
  }
}

trait AbstractSlsCourseSnippet  extends StatefulSnippet
    with  AnswerHandler
    with name.panitz.subato.XmlConfigDependency
    with Logger
    with AbstractCardSnippet {
  this: ModelDependencies with L10n =>

  protected val params: Params
  protected val subscription: Box[SFC]

  private def maybe_sfcc: Box[SFC] = {
    val resetOfEphemeralDataIsNeeded_? = {
      def flashcardCourseIdOf(c: Box[SFC]): Long = c map { _.flashcardCourseId } getOrElse -1 
      (!currentUser.loggedIn) && flashcardCourseIdOf(ephemeralSubscription) != flashcardCourseIdOf(subscription)
    }

    var result: Box[SFC] = subscription
    if (resetOfEphemeralDataIsNeeded_?) {
      ephemeralSubscription set subscription
    } else if (!currentUser.loggedIn) {
      result = ephemeralSubscription.get
    }
    result
  }

  abstract class StateVar[T](defV: T) extends SessionVar[Map[Long,T]](Map()) {

    def <= (ep: T) = {
      maybe_sfcc match {
        case Full(sfcc) => set(get + (sfcc.flashcardCourseId -> ep))
        case _ =>
      }
    }

    def >= = {
      maybe_sfcc match {
        case Full(sfcc) =>  get.get(sfcc.flashcardCourseId).getOrElse(defV)
        case _ => defV
      }
    }

  }

  override def isSolutionCorrect_? : Boolean = isSolutionCorrect.get
  override def isSolutionCorrect(value: Boolean): Unit = isSolutionCorrect set value
  override def isSolutionPartialCorrect_? : Boolean = isSolutionPartialCorrect.get
  override def isSolutionPartialCorrect(value: Boolean): Unit = isSolutionPartialCorrect set value
  override def getLastCard: Option[Card] = (lastCard.>=)
  override def getSubmission: Option[Submission] = submission.>=
  override def getTextAnswers: Seq[String] = textAnswers.get
  override def getTextAnswer: String = textAnswer.get
  override def getPlayedCardResultIndex: Map[Long, List[Elem]] = {
    maybe_sfcc match {
      case Full(sfcc) => Map(sfcc.flashcardCourseId -> List())
      case _          => Map()
    }
  }
  override def playedCardResultIndex(newMap: Map[Long, List[Elem]]): Unit = {}
  override val QUESTION_RELATIVE_URL = "/SlsCourse/"
  override val RESULT_RELATIVE_URL   = "/SlsCardResult/"
  override def setNextCardLink(url: String, label: String): CssSel = {
    "#nextCard [style]" #> ""    &
    "#nextCard [href]"  #> url   &
    "#nextCard *"       #> label
  }

  override def dispatch = {
    case "showMultiple"   => showMultiple
    case "Results"        => showResults
    case "showBoxes"      => showBoxes
    case "CreateForm"     => createBoxForm
    case "CreateCardForm" => createCardForm
    case "statistics"     => showStatisticalData
    case "progress"       => showUserCourseProgress
    case "earnedStars"    => showEarnedStars
    case "showCourseInfo" => showCourseInfo
  }

  

  def showEarnedStars: CssSel = {
    val maybeProfile = learningProfiles.findOrCreate(currentUid)
    val earnedStars = maybeProfile match {
      case Some(profile) => profile.earnedStars
      case _             => 0
    }
    "#earned-stars *" #> earnedStars
  }

  def showUserCourseProgress: CssSel = {
    val progress = maybe_sfcc.map(_.completion).getOrElse(0.0).formatted("%.1f").replace(",", ".")
    val setBarLength = s"$$('#progress-bar').attr('style', 'width: $progress%');"
    val setBarText = s"$$('#progress-bar').text('$progress%');"
    val setBarValue = s"$$('#progress-bar').attr('aria-valuenow', '$progress');"
    val setToolTip = s"$$('#progress-area').attr('title', 'Your progress in this course: $progress%');"
    val showProgressArea = s"$$('#progress-area').show();"
    "#progressBarScript" #> <script>{s"$setBarLength $setBarText $setBarValue $setToolTip $showProgressArea"}</script>
  }

  def showCourseInfo: CssSel = {
    val jsCode = maybe_sfcc map { _.toFlashcardCourse } match {
      case Full(Full((fcc, _))) =>
        val setCourseTitle       = s"$$('#dropdownCourseTitle').text(`${SU.unknowIfEmpty(fcc.title)}`);"
        val setCourseDescription = s"$$('#dropdownCourseDesc').html(`${fcc.description}`);"
        val setCourseTopics      = s"$$('#dropdownCourseTopics').text(`${SU.unknowIfEmpty(fcc.topics.mkString(", "))}`);"
        val makeDropdownVisible  = s"$$('#infoDropdown').show();"
        s"$setCourseTitle $setCourseDescription $setCourseTopics $makeDropdownVisible"
      case _ => s"$$('#infoDropdown').hide();"
    }
    "#infoDropdownScript" #> <script>{jsCode}</script>
  }

  override def processResult(id: Long, res: Progress): Unit = {
    processResult(id, res, true)
  }

  override def processResult(id: Long, res: Progress, redirectionTarget: String): Unit = {
    processResult_(id, res, true, false)
    redirectTo(redirectionTarget)
  }

  def noticeResultCorrectness(cardId: Long, actualizeStatistics_? : Boolean): Unit = {
    isSolutionCorrect set true
    val lcid = (lastCard.>=) match {
      case Some(lc) => lc.id
      case _        => -1
    }
    if ((lastQuestion.get == lcid) && actualizeStatistics_?) {
      maybe_sfcc map { _.material(lcid) } match {
        case Full(Full(slm)) => slm.upgradeMasteryLevel
        case _               =>
      }
    }
  }

  def currentUid(): String = currentUser.uid.getOrElse("")

  def noticeResultInCorrectness(cardId: Long, actualizeStatistics_? : Boolean): Unit = {
    val lcid = (lastCard.>=) match {
      case Some(lc) => lc.id
      case _        => -1
    }
    if ((lastQuestion.get == lcid) && actualizeStatistics_?) {
      maybe_sfcc map { _.material(lcid) } match {
        case Full(Full(slm)) => slm.degradeMasteryLevel
        case _               =>
      }
    }
  }

  def resetResultCorrectness(): Unit = {
    isSolutionCorrect        set false
    isSolutionPartialCorrect set false
  }

  override def processResult(id: Long, res: Progress, actualizeStatistics_? : Boolean) = {
    processResult_(id, res, actualizeStatistics_?)
  }

  private def processResult_(id: Long, res: Progress, actualizeStatistics_? : Boolean, autoRedirect_? : Boolean = true): Unit  = {
    resetResultCorrectness
    res match {
      case Correct        => noticeResultCorrectness(id, actualizeStatistics_?)
      case PartialCorrect => isSolutionPartialCorrect set true
      case _              => noticeResultInCorrectness(id, actualizeStatistics_?)
    }

    maybe_sfcc match {
      case Full(fcc) => {
        if (actualizeStatistics_?) actualizeCardStatistics(id)
        if (autoRedirect_?) redirectTo(s"${RESULT_RELATIVE_URL}${fcc.id}")
      }
      case _ =>
    }
  }

  def actualizeCardStatistics(id: Long): Unit = {
    val neededTime = computeNeededTime
    resolutionTime set neededTime
    if (isSolutionCorrect) statistics incGoodAnswerNum (id, neededTime)
    else if (isSolutionPartialCorrect) statistics incAlmostCorrectAnswerNum (id, neededTime)
    else statistics incWrongAnswerNum (id, neededTime)
  }

  def computeNeededTime: Long = (cardDisplayTime.>=) until (LDT.now(), CU.SECONDS)

  def solutionSubmitted(sol: Solution) = {
    sol match {
      case TextSolution(text) => textAnswer set text
      case WordListSolution(ws) => textAnswers set ws
      case ProgrammingSolution(sub) => submission <= Some(sub)
    }
  }

  object cards                     extends StateVar[Option[Seq[Card]]](None)

  object isSolutionCorrect         extends SessionVar[Boolean](false) {}

  object isSolutionPartialCorrect  extends SessionVar[Boolean](false) {}

  object lastTimeAsResult          extends StateVar[Boolean](true)

  object points                    extends StateVar[Long](0)

  object playedQuestions           extends StateVar[Long](0)

  object lastCard                  extends StateVar[Option[Card]](None) {}

  object submission                extends StateVar[Option[Submission]](None) {}

  object lastQuestion              extends SessionVar[Long](-1) {}

  object cardDisplayTime           extends StateVar[LDT](LDT.now())

  object resolutionTime            extends SessionVar[Long](0)

  object textAnswer                extends SessionVar[String]("") {}

  object textAnswers               extends SessionVar[List[String]](List()) {}

  object ephemeralSubscription     extends SessionVar[Box[SFC]](Empty)

  def showBoxes: CssSel = {
    "#rows *" #> name.panitz.subato.model.mapper.CardBox
      .findAll(By(mapper.CardBox.isPrivate,false))
      .map(new MapperCardBox(_))
      .filter(_.isPublic)
      .map(b => <a href={s"${QUESTION_RELATIVE_URL}${b.id}"}>{b.name + " (" + b.course.name + ")" }</a>)
  }

  def getBox: Elem = {
    if (isSolutionCorrect.get)  <span class="greenbox"/>
    else if (isSolutionPartialCorrect) <span class="yellowbox"/>
    else <span class="redbox"/>
  }

  def eval: Any = {
    playedQuestions <= (playedQuestions.>= +1)
    if (isSolutionCorrect.get)  points <= (points.>= +2)
    else if (isSolutionPartialCorrect)   points <= (points.>= +1)
  }

  import Severity._
  override def abort(to: String): Nothing = {
    def reinitialize(): Unit = {
      cards <= None
      isSolutionCorrect set false
      isSolutionPartialCorrect set false
      lastTimeAsResult <= true
      points <= 0
      playedQuestions <= 0
      lastCard <= None
      submission <= None
      lastQuestion.set(-1)
      cardDisplayTime <= LDT.now
      textAnswer set ""
      textAnswers set List()
    }

    reinitialize()
    PrettyAlerter.setMsg("Ooops...", "The required resource could not be found.", Error)
    S.redirectTo(to)
  }

  def resetUserAnswers: Unit = textAnswers set List.empty

  def lastCardId: Long = {
    (lastCard.>=) match {
      case Some(lc) => lc.id
      case _        => -1
    }
  }

  def show(card: Card, flashcardCourseId: Long): CssSel = {
    cardDisplayTime <= LDT.now
    lastCard        <= Some(card)
    var tooltipContent = card.ontology mkString ", "
    if (tooltipContent.isEmpty) tooltipContent = "Unknown"
    card.snippet(this, flashcardCourseId) &
    "#hidden"              #> SHtml.hidden(() => lastQuestion(card.id)) &
    "#card-topics [title]" #> s"This card covers the topics: $tooltipContent"
  }

  def showMultiple: CssSel = {
    resetUserAnswers
    maybe_sfcc match {
      case Full(sfcc) => showNextAvailableCard(sfcc)
      case _          => abort("/Sls")
    }
  }

  def showNextAvailableCard(sfcc: SFC): CssSel = {
    resetCards(sfcc)
    lazy val Some(cs) = cards.>=
    lazy val fromResult_? = lastTimeAsResult.>=

    if ((lastCardId != -1) && (! fromResult_?)) {
      reshowLastCard(sfcc.flashcardCourseId)
    } else if (cs.isEmpty) {
      emptyCourseError(sfcc.flashcardCourseId)
    } else {
      showNextCard(sfcc.id)
    }
  }

  def resetCards(sfcc: SFC): Unit = {
    lazy val newCards = sfcc.nextMaterialsToLearn.foldRight (Seq.empty[Card]) {
      (next, result) => {
        val maybe_fc =  next.learningMaterial.flatMap(_.flashcard)
        lazy val fc = maybe_fc openOrThrowException ""
        if (maybe_fc.isDefined) fc +: result else result
      }
    }

    lazy val cardsSortedByDifficulty = CardOracle.fromEasiestToMostDifficult(newCards)
      (cards.>=, lastTimeAsResult.>=) match {
      case (None, _)         => cards <= Some(cardsSortedByDifficulty)
      case (Some(Nil), _)    => cards <= Some(cardsSortedByDifficulty)
      case _                 =>
    }
  }

  def showNextCard(flashcardCourseId: Long): CssSel = {
    val Some(card +: tail) = cards.>=
    card.initialize()
    val fromResult_? = lastTimeAsResult.>=
    lastTimeAsResult <= false

    if (card.exist_?) {
      val result = show(card, flashcardCourseId)
      cards <= Some(tail)
      result
    } else {
      abort("/Sls")
    }
  }

  def getCards: Seq[Card] = {
    (cards.>=) match {
      case Some(seq) => seq
      case _         => Seq()
    }
  }

  def emptyCourseError(flashcardCourseId: Long): CssSel = {
    cards <= None
    "#question *"      #> <div>The course does not contain any flashcards</div> &
    "#selectionItem *" #> List(<a href="/Sls">SLS-Menu</a>)
  }

  def reshowLastCard(flashcardCourseId: Long): CssSel = {
    lastTimeAsResult <= false
      (lastCard.>=) match {
      case Some(card) => show(card, flashcardCourseId)
      case _          =>  "#hidden" #> SHtml.hidden(() => lastQuestion(-1))
    }
  }

  def showResults: CssSel = {
    lazy val unavailableCardBoxMsg = "#t" #> loc("noCardBoxSpecified")
    maybe_sfcc match {
      case Full(sfcc) => tryToShowResults(sfcc)
      case _          => unavailableCardBoxMsg
    }
  }

  def tryToShowResults(sfcc: SFC): CssSel = {
    try {
      val Some(card) = lastCard.>=
      showResult(card, sfcc.flashcardCourseId) & congratulateUserIfCourseCompleted
    } catch {
      case _: Exception => redirectTo(s"$QUESTION_RELATIVE_URL${sfcc.flashcardCourseId}")
    }
  }

  def showResult(card: Card, flashcardCourseId: Long): CssSel = {
    val lastDisplayWasResult_? = lastTimeAsResult.>=
    if (lastCardId != lastQuestion.is){
      val url = s"$QUESTION_RELATIVE_URL$flashcardCourseId"
      redirectTo(url)
    } else if (! lastDisplayWasResult_?) {
      showResultOf(card, flashcardCourseId)
    } else {
      reshowResultOf(card, flashcardCourseId)
    }
  }

  def congratulateUserIfCourseCompleted: CssSel = {
    def userCanBeCongratulated_? : (Boolean, SFC) = {
      maybe_sfcc match {
        case Full(sfcc) => (sfcc.userMustBeCongratulated_?, sfcc)
        case _          => (false, null)
      }
    }
    
    userCanBeCongratulated_? match {
      case (true, sfcc) =>
        sfcc.userHasBeenCongratulated()
        "#congratulation" #> <script>{Alerts.buildCourseCongratulation(sfcc.finalGrade).toJsCode};</script>
      case _ =>
        "#congratulation" #> <script></script>
    }

  }

  def showResultOf(card: Card, flashcardCourseId: Long): CssSel = {
    lastTimeAsResult <= true
    if (!card.isInstanceOf[MapperFreeTextCard]) eval
    card.resultSnippet(this, flashcardCourseId) & highlightResultCardBorder(card)
  }

  def reshowResultOf(card: Card, flashcardCourseId: Long): CssSel = {
    lastTimeAsResult <= true
    card.resultSnippet(this, flashcardCourseId) & highlightResultCardBorder(card)
  }

  def highlightResultCardBorder(card: Card): CssSel = {
    var bootstrapCssClassName  = ""
    var headerBorderStyle      = "border-color: "
    var headerTitle            = ""
    if (card.isInstanceOf[MapperFreeTextCard]) {
      bootstrapCssClassName = "border-info"
      headerBorderStyle    += "rgba(23, 162, 184, 0.4);"
      headerTitle           = "Self-assessment"
    } else if (isSolutionCorrect_?) {
      bootstrapCssClassName = "border-success"
      headerBorderStyle    += "rgba(40, 167, 69,  0.4);"
      headerTitle           = "Your answer was correct"
    } else if (isSolutionPartialCorrect_?) {
      bootstrapCssClassName = "border-warning"
      headerBorderStyle    += "rgba(255, 193, 7,  0.4);"
      headerTitle           = "Your answer was partial correct"
    } else {
      bootstrapCssClassName = "border-danger"
      headerBorderStyle += "rgba(220, 53, 69,  0.4);"
      headerTitle           = "Your answer was incorrect"
    }
    "#sls-card [class+]"        #> bootstrapCssClassName &
    "#sls-card-header [style+]" #> headerBorderStyle     &
    "#feedbackTitle *"          #> headerTitle           &
    "#sls-card-footer [style+]" #> headerBorderStyle
  }

  def showStatisticalData: CssSel = {
    maybe_sfcc match {
      case Full(sfcc) => {
        try {
          val Some(card) = lastCard.>=
          statistics.layoutOf(card.id, resolutionTime.get)
        } catch {
          case _: Exception => redirectTo(s"${QUESTION_RELATIVE_URL}${sfcc.flashcardCourseId}")
        }
      }
      case _ => "#t" #> loc("noCardBoxSpecified")
    }
  }

  def createBoxForm: CssSel={
    var name=""
    var description=""
    var courseId=1L

    def submitted = {
      val box = cardBoxes.create(courseId, currentUser.uid.openOrThrowException(""), name, description)
      box.save
    }


    "#courseSelector" #> SHtml.select(
      courses.findAll.map(c => new SHtml.SelectableOption(c.id + "", c.name + ""))
        , Empty
        , (s) => {courseId=s.toLong}) &
    "#name"        #> SHtml.textarea("", s => { name = s}, ("rows","1"),("class","singleRow")) &
    "#description" #> SHtml.textarea( "", s => { description = s }, "style" -> "height: 450px; width: 100%;")&
    "#submit *"    #> SHtml.submit(locStr("submit"), submitted _,("type","submit"),("onclick","startSpinner()"))
  }


  import name.panitz.subato.model.mapper.CardType
  def createCardForm = {
    maybe_sfcc match {
      case Full(fcc) => {
        var answers: List[(String,Boolean)] = List()
        var question = ""
        var name = ""
        var explanation = ""
        var cardType = CardType.MultipleChoice

        def submitted = {}

        val answer = <tr>
        <td>{SHtml.textarea( "", s => { explanation = s }, "style" -> "height: 1em; width: 100%;")}</td>
        <td>{SHtml.checkbox( false,s=> {})}</td>
        </tr>

        "#cardType" #> SHtml.select(
          CardType.values.map(v => new SHtml.SelectableOption(v+"",v+"")).toList
            , Full(CardType.MultipleChoice+"")
            , (s) => {cardType = CardType.withNameOpt(s).get}) &
        "#question"    #> SHtml.textarea( "", s => { question = s }, "style" -> "height: 450px; width: 100%;")&
        "#submit *"    #> SHtml.submit(locStr("submit"), submitted _,("type","submit"),("onclick","startSpinner()")) &
        "#explanation" #> SHtml.textarea( "", s => { explanation = s }, "style" -> "height: 450px; width: 100%;")&
        "#answers *"   #> <table>{List(answer,answer,answer,answer,answer,answer,answer,answer,answer,answer)}</table>
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
    }

  }

}
