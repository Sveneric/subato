package name.panitz.subato.snippet

import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import name.panitz.subato.model._

import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}

class SolutionSnippet(private val e:Box[Exercise]) extends AbstractSolutionSnippet
    with MapperModelDependencies
    with SWrapperL10n {

  def this() = this(Empty)

  override protected val exercise = e
  override protected val params = ParamsImpl
}


trait AbstractSolutionSnippet {
  this:ModelDependencies
      with L10n   =>
  protected val params:Params
  protected val exercise:Box[Exercise]

  def solution = exercise match {
    case Full(ex) => {
      if (currentUser.isTutorIn(ex.course)||ex.solutionIsPublic) {
         "#exerciseName" #> ex.name &
        "#solution" #>  <pre class="line-numbers"><code class={"language-" +ex.brush}>{ex.solution}</code></pre>
      }else {
        "#exerciseName" #> ex.name & "#solution" #> <div >{locStr("solutionNotPublic")}</div>
      }
    }
    case _ => loc("noCourseSpecified")
  }
}


