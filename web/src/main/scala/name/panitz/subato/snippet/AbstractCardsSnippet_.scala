package name.panitz.subato.snippet

import net.liftweb.http.StatefulSnippet
import name.panitz.subato.model.{Card, Submission, AnswerHandler, Progress}
import scala.xml.Elem
import net.liftweb.util.CssSel

import Progress._

trait AbstractCardSnippet {
  this: Any =>
  def redirectTo(link: String): CssSel
  def isSolutionCorrect_? : Boolean
  def isSolutionCorrect(value: Boolean): Unit
  def isSolutionPartialCorrect_? : Boolean
  def isSolutionPartialCorrect(value: Boolean): Unit
  def getLastCard: Option[Card]
  def getSubmission: Option[Submission]
  def computeNeededTime: Long
  def getTextAnswers: Seq[String]
  def getTextAnswer: String
  def eval: Any
  def getPlayedCardResultIndex: Map[Long, List[Elem]]
  def playedCardResultIndex(newMap: Map[Long, List[Elem]]): Unit
  def getBox: Elem
  val RESULT_RELATIVE_URL: String   = "/CardResult/"
  val QUESTION_RELATIVE_URL: String = "/CardBox/"
  def setNextCardLink(url: String, urlLabel: String): CssSel
  final def canProcessCardResult_?(): Boolean = this.isInstanceOf[AnswerHandler]
  def processResult(id: Long, res: Progress, redirectionTarget: String): Unit = {}
}
