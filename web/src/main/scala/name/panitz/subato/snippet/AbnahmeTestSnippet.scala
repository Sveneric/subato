package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class AbnahmeTestSnippet(val c:Box[ Exercise ])
    extends AbstractAbnahmeTestSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  def this(e:(String,Box[Exercise])) = {
    this(e._2)
    this.submissionId = e._1
  }

  override protected var submissionId:String = null
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractAbnahmeTestSnippet extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
  with L10n  =>

  protected val params:Params
  protected val exercise:Box[ Exercise]
  protected var submissionId:String

  protected def config:Config
  private object results extends SessionVar[Box[(Submission)]](Empty)

  override def dispatch = {
    case "TestSolution" => testSolution
    case "Results" => showResults
  }

  def testSolution = {
    (exercise) match {
      case Full(ex) => {

        (submissions.find(submissionId.toLong)) match {
          case Full(sub) => {
            results.set(Full(ex.evaluator.testSolution(sub.solution, "", ex, sub.courseID, ex.latestTerm.id, null, false, true)))
            redirectTo("/AbnahmeTestResults")
          }
          case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>

        }

      }
      case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>
    }
  }

  private def showResults = {
    results.get match {
      case Full((submission:Submission)) => {
        "#content *" #> submission.asHtml(exercise.openOrThrowException(""))
      }
      case x =>  "*" #> <span>{loc("noResultsToDisplay")+x.toString}</span>
    }
  }


}
