package name.panitz.subato.snippet

import net.liftweb.http.S
import net.liftweb.common.Box
import net.liftweb.util.Helpers._

object ParamsImpl extends Params {
  def get(name:String) = S.param(name)
  def getLong(name:String) = S.param(name) map (_ match { case AsLong(l) => l })
}

trait Params {
  def get(name:String):Box[String]
  def getLong(name:String):Box[Long]
}
