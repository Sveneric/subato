package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{FileParamHolder, SHtml, SessionVar, StatefulSnippet}

import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger

class ExerciseSnippet(val c: Box[Exercise])
  extends AbstractExerciseSnippet
    with SWrapperL10n
    with XmlConfigDependency
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  def this(e: (String, Box[Exercise])) = {
    this(e._2)
    this.sheetId = e._1
  }

  override protected var sheetId: String = null
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractExerciseSnippet extends StatefulSnippet
  with name.panitz.subato.XmlConfigDependency
  with Logger {
  this: ModelDependencies
    with L10n =>

  protected val params: Params
  protected val exercise: Box[Exercise]
  protected var sheetId: String

  protected def config: Config

  override def dispatch = {
    case "Form" => showForm
    case "Results" => showResults
    case "TestSampleSolution" => testSampleSolution
  }

  private object lastSolution extends SessionVar[Map[Long, String]](Map()) {
    def <=(ep: (Long, String)) = set(get + (ep._1 -> ep._2))

    def >=(id: Long) = get.get(id)
  }

  def testSampleSolution = {
    (exercise) match {
      case Full(ex) => {
        val userDir =
          if (currentUser.loggedIn) {
            config.usersBaseDir +
              "/" + currentUser.uid.openOrThrowException("no current user found") +
              "/" + ex.id + "/"
          } else "/dev/null"
        println(net.liftweb.http.LiftRules.stdRequestTimeout )
        //println("nun wird die Musterlösung getestet")
        //net.liftweb.http.js.JE.Call("stopSpinner")
        //def f():net.liftweb.http.js.JsCmd = net.liftweb.http.js.JsCmds.jsExpToJsCmd(net.liftweb.http.js.JE.Call("alarmiere"))
        //SHtml.ajaxInvoke(f )//

        results.set(Full(ex.evaluator.testSolution(ex.solution, userDir, ex, sheetId.toLong, ex.latestTerm.id, null, false, false)))

        
        /*println("das war die Musterlösung getestet")
        println("und jetzt ein redirect")

        println("spinner ausmachen")

        net.liftweb.http.js.JE.Call("stopSpinner")
        println("spinner is weg...")*/
        redirectTo("/Results")
      }
      case _ => "*" #> <span>
        {loc("noExerciseSpecified")}
      </span>
    }
  }

  def showForm = {
    (exercise) match {
      case Full(ex) => {

        var solution = ""
        var termId = 0L
        var group: Box[Group] = Empty
        var uploadFile : FileParamHolder = null
        var uploadStream: java.io.InputStream = null
        val sheetbox = exerciseSheets.find(sheetId.toLong)
        var notToLate = false
        var courseId = ex.course.id
        (sheetbox) match {
          case Full(sheet) => {
            val now = new java.util.Date();
            //println(now+" "+sheet.lastSubmission+" "+now.compareTo(sheet.lastSubmission))
            if (sheet.lastSubmission != null) notToLate = now.compareTo(sheet.lastSubmission) < 1
            else notToLate = true

            courseId = sheet.course.id
          }
          case _ => {}
        }
        val userDir =
          if (currentUser.loggedIn) {
            config.usersBaseDir +
              "/" + currentUser.uid.openOrThrowException("no current user found") +
              "/" + ex.id + "/"
          } else // "/dev/null"
            config.usersBaseDir +
              "/lambdaUser/" + ex.id + "/"


        def solutionSubmitted = {
          if (currentUser.loggedIn) {
            for (g <- group) {
              currentUser.addGroup(g)
            }
          }
          lastSolution <= (ex.id, solution)
          //println("teste mit: "+notToLate)
          results.set(Full(ex.evaluator.testSolution(solution, userDir, ex, courseId.toLong, termId, uploadFile, notToLate && currentUser.loggedIn, false)))
          redirectTo("/Results")

        }

        val currentTerm = ex.latestTerm
        val currentGroup = currentUser.group(ex.course, currentTerm)

        "#javascript" #> <script language="javascript" type="text/javascript">
          <![CDATA[
            var editor = undefined;
            function loadAceEdit() {
              if(document.getElementById("editor") == undefined) return;
              ace.require("ace/ext/language_tools");
              editor = ace.edit("editor");
              editor.setOptions({
                enableBasicAutocompletion: true,
                scrollPastEnd: 1,
                printMargin: 90
              });
              editor.session.setOptions({
                tabSize: 2,
                useSoftTabs: true,
              });
              editor.setTheme("ace/theme/chrome");
              var modelist = ace.require("ace/ext/modelist");
              var filePath = "excercise." + $("#fileExt").html(); // create dummy file with ext from exercise to detect ace mode
              var mode = modelist.getModeForPath(filePath).mode;
              editor.session.setMode(mode);
              document.getElementById("exForm").onsubmit = function(evt) { // on submit, move ace editor contents to textarea in actual form
                document.getElementById("code_form").value = editor.getValue();
              }
              $(".toggleFullscreen").click(function() {
                editor.container.webkitRequestFullscreen()
              });
              $(".help").click(function() {
                window.open("https://github.com/ajaxorg/ace/wiki/Default-Keyboard-Shortcuts");
              });
              $(".search").click(function() {
                editor.execCommand("find");
              });
              $(".unfold").click(function() {
                editor.getSession().unfold();
              });
              $(".fold").click(function() {
                editor.getSession().foldAll(0, 28);
              });
              $(".settings").click(function() {
                editor.execCommand("showSettingsMenu");
              });
            }
            loadAceEdit();
            ]]>
        </script> &
          "#exName *" #> ex.name &
          "#exText" #> ex.exerciseText &
          "#groupField" #> SHtml.selectObj(
            groups.findByCourseAndTerm(courseId, currentTerm.id).map(x => (x, x.name)), currentGroup, (g: Group) => group = Full(g)) &
          "#submit *" #> {
            if (ex.lambdaUserAllowed || currentUser.loggedIn) {
              SHtml.button(loc("submitSolution"), solutionSubmitted _, ("type", "submit"), ("onclick", "startSpinner()"))
            } else {
              <span>
                {locStr("cannotSubmit")}
              </span>
            }
          } &
          //SHtml.submit(locStr("submitSolution"), solutionSubmitted _)
          "#hiddenTerm  " #> SHtml.hidden(() => termId = currentTerm.id) &
          //  "#hiddenCourse" #> SHtml.hidden(()=>courseId = ex.course.id)&
          {
            if (ex.isFileUploadExercise) {
              "#solution" #>
                SHtml.fileUpload((fph) => {
                  uploadStream = fph.fileStream
                  uploadFile = fph
                }, ("accept", "." + ex.fileExtention.split(";").mkString(",.")))
            } else {
              "#solution" #>
              SHtml.textarea(
                (lastSolution >= ex.id).getOrElse(ex.solutionTemplate)
                , s => {
                  solution = s
                }
                , "id" -> "code_form"
                , "style" -> "height: 450px; width: 100%;"
              ) &
              "#code" #> {
                <p class="controls">
                  <img title="Search/Replace" class="search" src="/assets/images/search-24px.svg"/>
                  <img title="Fold all" class="fold" src="/assets/images/unfold_less-24px.svg"/>
                  <img title="Unfold all" class="unfold" src="/assets/images/unfold_more-24px.svg"/>
                  <img title="Fullscreen" class="toggleFullscreen" src="/assets/images/fullscreen-24px.svg"/>
                  <img title="Settings" class="settings" src="/assets/images/settings-24px.svg"/>
                  <img title="Help" class="help" src="/assets/images/help_outline-24px.svg"/>
                </p>
                <div id="editor">{(lastSolution >= ex.id).getOrElse(ex.solutionTemplate)}</div><span id="fileExt">{ex.fileExtention}</span>
              }
            }
          } &
          "#resourceFiles *" #>
            (
              <div style="margin: 5px;border: 1px solid #ccc;float: right; width:33%;">
                <div style="margin: 10px;">
                  <h3>
                    {locStr("resourceFiles")}
                  </h3>
                  <ul>
                    {if (!ex.isFileUploadExercise) {
                    <li>
                      <a href={"/serveTemplate/" + ex.id + "/history"}>
                        {val i = ex.solutionFQCN.lastIndexOf(".")
                      (if (i > 0) ex.solutionFQCN.substring(ex.solutionFQCN.lastIndexOf(".")) else ex.solutionFQCN) +
                        "." + ex.fileExtention}
                      </a> <br/>{locStr("solutionTemplate")}
                    </li>
                  }}{ex.resourceFiles.map(rf => {
                    val name = if (rf.name.indexOf("/") >= 0) rf.name.substring(rf.name.lastIndexOf("/") + 1) else rf.name
                    <li>
                      <a href={"/serveResource/" + ex.id + "/" + rf.id}>
                        {name}
                      </a> <br/>{rf.description}
                    </li>
                  })}
                    {if (currentUser.isTutorIn(ex.course)) {
                    for (file <- ex.files) yield
                      <li>
                        (hidden)
                        <a href={"/serveHiddenResource/" + ex.id + "/" + file}>
                          {file}
                        </a><br/>
                        (
                        <a href={"/EditTextResource/" + ex.id + "/" + file}>edit</a>
                        ) (
                        <a href={"/DeleteTextResource/" + ex.id + "/" + file}>delete</a>
                        )</li>

                  }}
                  </ul>{if (currentUser.isLecturerIn(ex.course)) {
                  <a href={"/ListResourceFilesForExercise/" + ex.id}>
                    {loc("editResourceFiles")}
                  </a>
                }}
                </div>
              </div>
              )
      }
      case _ => "*" #> <span>
        {loc("noExerciseSpecified")}
      </span>
    }
  }

  private object results extends SessionVar[Box[(Submission)]](Empty)

  private def showResults = {
    results.get match {
      case Full((submission: Submission)) => {
        showSubmission(submission) &
          "#startgame" #> {
            if (!submission.compilationFailed
              && submission.tests > 0
              && submission.failures + submission.errors == 0 && submission.stylecheckErrors == 0) {
              (Math.abs(randomInt(10) % 8)) match {
                case 0 =>
                  //Schiebepuzzle
                  <script type="text/javascript">oeffneFenster(500,500,'/subato/static/FettesPuzzle.html')</script>
                case 1 =>
                  //Feuerwerk
                  <script type="text/javascript" src="/static/fireworks.js"></script>
                case 2 =>
                  //Konfetti
                  <script type="text/javascript" src="/static/confetti.js"></script>
                case 3 =>
                  //Puzzle
                  <table align="center">
                    <tr>
                      <td colspan="4" id="puzzleHeader">Ein kleines Puzzle zur Entspannung</td>
                    </tr>
                    <tr>
                      <td id="wenigContainer"></td>
                      <td id="mittelContainer"></td>
                      <td id="vielContainer"></td>
                      <td id="sehrVielContainer"></td>
                    </tr>
                    <tr>
                      <td colspan="4" width="800" height="600" id="nameFieldContainer"></td>
                    </tr>
                    <script type="text/javascript" language="javascript" src="/static/puzzle/puzzle/puzzle.nocache.js"></script>
                  </table>
                case 4 =>
                  //Jumping Pinguin von Andreas Heinz WS15/16
                  <script type="text/javascript">oeffneFenster(500,800,'/subato/static/jumpingPinguin/WebGames.html')</script>
                case 5 =>
                  //TicTacToe
                  <script type="text/javascript">oeffneFenster(700,700,'/subato/static/strategicGames/TicTacToe.html')</script>
                case 6 =>
                  //Vier Gewinnt
                  <script type="text/javascript">oeffneFenster(800,900,'/subato/static/strategicGames/VierGewinnt.html')</script>
                case 7 =>
                  //Othello
                  <script type="text/javascript">oeffneFenster(800,900,'/subato/static/strategicGames/Othello.html')</script>

              }
            } else {
                <div/>
            }
          }
      }
      case x => "*" #> <span>
        {loc("noResultsToDisplay") + x.toString}
      </span>
    }
  }


  private def showSubmission(submission: Submission) = {
    "#content *" #> submission.asHtml(exercise.openOrThrowException(""))
    /*

      "#uid" #> submission.uid &
      "#time" #> submission.datetime.toString &
      "#solution" #> (<script type="syntaxhighlighter" class={"brush: "+exercise.map(_.brush).openOr("")}>{
        scala.xml.Unparsed("<![CDATA[%s]]>".format(submission.solution))}</script>
        <script type="text/javascript">
    SyntaxHighlighter.all()
  </script>)&
      "#compilerMessages *" #> submission.compilationOutput &
      "#messages" #> submission.messages &
      "#tests" #> submission.tests.toString &
      "#failures" #> submission.failures.toString &
      "#errors" #> submission.errors.toString &
       (if (exercise.map(_.hasStorageCheck).openOr(false)){
         "#memoryManagement" #>  <div>
           <h3>Memory Usage</h3>
           <b>Allocations:</b> <span id="allocations">{submission.allocations.toString}</span><br />
           <b>Frees:</b> <span id="frees">{submission.frees.toString}</span><br />
           <b>Lost:</b> <span id="lost">{(submission.allocations-submission.frees).toString}</span>
         </div>
       }else "#memoryManagement" #>  Text("")) &
       (if (exercise.map(_.hasStyleCheck).openOr(false)){
         "#styleChecks" #>  <div>
           <h3>Style Checks</h3>
           <b>Style Problems:</b> <span id="stylecheckerrors">{submission.stylecheckErrors}</span><br />
           <div>{submission.styleCheckOutput}</div>
         </div>
       }else "#styleChecks" #>  Text("")) &
      "#testcase *" #> submission.unittests &
      "#comment *" #> (if (submission.comment==null) <span /> else submission.comment) &
      "#editComment" #>
         { if (currentUser.isLecturer) <a href={"/exerciseresult/edit/"+submission.id}>kommentieren</a> else <span/> } &
      "#indicator *" #>
         {
           if (!submission.compilationFailed){
             if (submission.tests==0){ 
               <div style="background: #FF2222;height:25px;">Fehlgeschlagen: keine Tests laufen"</div>
             }else if(submission.errors+submission.failures == 0){
                <div style="background: #00FF00;height:25px;">Alle Tests laufen erfolgreich"</div>
             }else if(submission.tests-submission.errors-submission.failures > submission.tests/2){
               <div style="background: #FFFF00;height:25px;">Fast: mehr als die Hälfte der Tests laufen"</div>
             }else {
               <div style="background: #FF2222;height:25px;">So gut wie keine Tests funktionieren</div>
             }
           } else {
             <div style="background: #AA0000;height:25px;">Fehlgeschlagen: kompiliert nicht"</div>
           }
         }*/
  }


}
