package name.panitz.subato.snippet

import net.liftweb.http.js.{JsCmds, JsCmd}
import scala.xml.{Node, NodeSeq}

trait ClienSideRefresher {

  def refresh: JsCmd = JsCmds.Noop

  def activeComplexTooltips: JsCmd = JsCmds Run """$('[data-html="true"]').tooltip();"""

  private[snippet] def refresh(htmlElemId: String, newContent: NodeSeq): JsCmd = JsCmds SetHtml (htmlElemId, newContent)

  private[snippet] def refresh(replacements: Map[String, NodeSeq]): JsCmd = {
    replacements.foldLeft(JsCmds.Noop) { case (result, (id, newContent)) => result & (refresh(id, newContent)) }
  }

  private[snippet] def hide(htmlElemIds: String*): JsCmd = {
    var jsCode = JsCmds.Noop
    for (id <- htmlElemIds) {
      val code = "$('#" + id + "').hide()"
      val cmd  = JsCmds Run code
      jsCode = jsCode & cmd 
    }
    jsCode
  }

  private[snippet] def show(htmlElemIds: String*): JsCmd = {
    var jsCode = JsCmds.Noop
    for (id <- htmlElemIds) {
      val code = "$('#" + id + "').show()"
      val cmd  = JsCmds Run code
      jsCode = jsCode & cmd 
    }
    jsCode
  }

  private[snippet] def setAttr(htmlElemId: String, attrName: String, newValue: String): JsCmd = {
    val jsCode = "$('#" + htmlElemId + "').attr('" + attrName + "', '" + newValue + "')"
    JsCmds Run jsCode
  }

  private[snippet] def setValue(htmlElemId: String, newValue: String): JsCmd = {
    val jsCode = "$('#" + htmlElemId + "').val('" + newValue + "')"
    JsCmds Run jsCode
  }

}
