package name.panitz.subato.snippet

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.CompilationUnit
import net.liftweb.json.DefaultFormats
import net.liftweb.json.JsonAST.JValue
import org.jsoup.Jsoup
import org.jsoup.helper.W3CDom
import org.w3c.dom.NamedNodeMap

import scala.xml.{Node, NodeSeq, XML}

object CardAttributeExtractor {

  type CardTitle = String
  type Errors = NodeSeq
  type ErrorMessage = (CardTitle, Errors)

  implicit val formats: DefaultFormats.type = DefaultFormats

  private val idAttrName                  = "id"
  private val titleAttrName               = "title"
  private val questionAttrName            = "question"
  private val explanationAttrName         = "explanation"
  private val bloomsLevelAttrName         = "bloomslevels"
  private val typeAttrName                = "type"
  private val initialSnippetAttrName      = "initialSnippet"
  private val programmingLanguageAttrName = "programmingLanguage"
  private val testsAttrName               = "tests"
  private val topicsAttrName              = "topics"
  private val answersAttrName             = "answers"
  private val correctAnswersAttrName      = "correctAnswers"
  private val incorrectAnswersAttrName    = "incorrectAnswers"
  private val newCardAttrName             = "newCard"

  private[snippet] def extractIdFrom(json: JValue): Long = {
    try {
      (json \ idAttrName).extractOrElse[String]("").trim.toLong
    } catch {
      case _: Exception => -1L
    }
  }

  private[snippet] def extractCorrectAnswersFrom(json: JValue): Seq[String] = {
    (json \ answersAttrName \ correctAnswersAttrName).extractOrElse[Array[String]](Array.empty)
      .toSeq
      .filter { _.nonEmpty }
  }

  private[snippet] def extractInCorrectAnswersFrom(json: JValue): Seq[String] = {
    (json \ answersAttrName \ incorrectAnswersAttrName).extractOrElse[Array[String]](Array.empty)
      .toSeq
      .filter { _.nonEmpty }
  }

  private[snippet] def extractExplanationFrom(json: JValue): String = {
    (json \ explanationAttrName).extractOrElse[String]("").trim
  }

  private[snippet] def extractTitleFrom(json: JValue): String = {
    (json \ titleAttrName).extractOrElse[String]("").trim
  }

  private[snippet] def extractBloomsLevelFrom(json: JValue): String = {
    val defaultBloomsLevel = "Remember"
    (json \ bloomsLevelAttrName).extractOrElse[String](defaultBloomsLevel)
  }

  private[snippet] def extractTopicsFrom(json: JValue): Seq[String] = {
    (json \ topicsAttrName).extractOrElse[Array[String]](Array.empty)
      .toSeq
      .filter { _.nonEmpty }
  }

  private[snippet] def extractTypeFrom(json: JValue): String = {
    (json \ typeAttrName).extractOrElse[String]("").toLowerCase.trim
  }

  private[snippet] def whatIsIncorrect_?(json: JValue): ErrorMessage = {
    if (containsProgrammingCard_?(json)) {
      (extractTitleFrom(json), whatIsIncorrectForProgrammingCardCreation_?(json))
    } else {
      (extractTitleFrom(json), whatIsIncorrectForSimpleCardCreation_?(json))
    }
  }

  private def whatIsIncorrectForProgrammingCardCreation_?(json: JValue): Seq[Node] = {
    var errorMsgs = Seq.empty[Node]
    if (extractProgrammingLanguageFrom(json).isEmpty) errorMsgs +: <li>Unknown Programming Language</li>
    extractTestCodeFrom(json) match {
      case Left(msg) => errorMsgs = <li>Parsing of test code failed</li> +: errorMsgs
      case _         =>
    }
    extractSampleSolutionCode(json) match {
      case Left(msg) => errorMsgs = <li>Parsing of sample solution failed</li> +: errorMsgs
      case _         =>
    }
    whatIsIncorrectForSimpleCardCreation_?(json) ++: errorMsgs
  }

  private def whatIsIncorrectForSimpleCardCreation_?(json: JValue): Seq[Node] = {
    var errorMsgs = Seq.empty[Node]
    if (extractTypeFrom(json).isEmpty) errorMsgs = <li>Unknown card type</li> +: errorMsgs
    if (extractCorrectAnswersFrom(json).isEmpty) errorMsgs = <li>Correct answers was empty</li> +: errorMsgs
    if (extractExplanationFrom(json).isEmpty) errorMsgs = <li>Explanation was empty</li> +: errorMsgs
    if (extractTitleFrom(json).isEmpty) errorMsgs = <li>Card title was empty</li> +: errorMsgs
    extractQuestionFrom(json) match {
      case Left(msg) => errorMsgs = <li>{msg}</li> +: errorMsgs
      case _         =>
    }
    errorMsgs
  }

  private[snippet] def containsProgrammingCard_?(json: JValue): Boolean = extractTypeFrom(json) == "programming"

  private[snippet] def simpleCardCanBeCreatedFrom(json: JValue): Boolean = {
    extractTitleFrom(json).nonEmpty &&
      extractQuestionFrom(json).isRight &&
      extractExplanationFrom(json).nonEmpty &&
      extractCorrectAnswersFrom(json).nonEmpty
  }

  private[snippet] def containsNewCard_? (json: JValue): Boolean = {
    (json \ newCardAttrName).extractOrElse[Boolean](false)
  }

  private[snippet] def extractCardTopics(json: JValue): Seq[String] = {
    (json \topicsAttrName).extractOrElse[Array[String]](Array.empty)
  }

  private[snippet] def extractQuestionFrom(json: JValue): Either[String, NodeSeq] = {
    try {
      val question = (json \ questionAttrName).extractOrElse[String]("").trim
      val w3cDomNode = new W3CDom().fromJsoup(Jsoup.parse(question)).getFirstChild
      val scalaNode = fromW3cDomNodeToScalaNode(w3cDomNode) \ "body" flatMap { _.child }
      Right(scalaNode)
    } catch {
      case e: Exception => Left(e.getMessage)
    }
  }

  private[snippet] def extractTestCodeFrom(json: JValue): Either[NodeSeq, CompilationUnit] = {
    val testsAsString = (json \ testsAttrName).extractOrElse[String]("").trim
    parseJavaCode(testsAsString, "Test code")
  }

  private[snippet] def extractSampleSolutionCode(json: JValue): Either[NodeSeq, CompilationUnit] = {
    val sampleSolutionCodeAsString = extractCorrectAnswersFrom(json).headOption.getOrElse("")
    parseJavaCode(sampleSolutionCodeAsString, "Test code")
  }

  private[snippet] def extractProgrammingLanguageFrom(json: JValue): String = {
    (json \ programmingLanguageAttrName).extractOrElse[String]("").trim
  }

  private[snippet] def extractInitialSnippetFrom(json: JValue): String = {
    (json \ initialSnippetAttrName).extractOrElse[String]("").trim
  }

  private[snippet] def programmingCardCanBeCreatedFrom(json: JValue): Boolean = {
    extractProgrammingLanguageFrom(json).nonEmpty &&
      extractTestCodeFrom(json).isRight &&
      extractSampleSolutionCode(json).isRight
  }

  private def fromW3cDomNodeToScalaNode(node: org.w3c.dom.Node): Node = XML.loadString(fromJavaNodeToXMLString(node))

  private def fromJavaNodeToXMLString(node: org.w3c.dom.Node): String = {
    try {
      if (node.getNodeType == org.w3c.dom.Node.ELEMENT_NODE) {
        val parameters = node.getAttributes
        val childNodes = node.getChildNodes
        val nodesAsStrings: Seq[String] = for {
          i <- 0 to childNodes.getLength
          nodeAsString = if (i < childNodes.getLength) fromJavaNodeToXMLString(childNodes.item(i)) else ""
        } yield nodeAsString
        s"<${node.getNodeName} ${buildAttributesString(node.getNodeName, parameters)}>${nodesAsStrings.mkString}</${node.getNodeName}>"
      } else if (node.getNodeType == org.w3c.dom.Node.TEXT_NODE) {
        s"<span>${node.getTextContent}</span>"
      } else {
        "<span></span>"
      }
    } catch {
      case _: Exception => "<span></span>"
    }
  }

  private def buildAttributesString(nodeName: String, attrMap: NamedNodeMap): String = {
    val style = if (nodeName == "img") {
      """style="max-width: 390px; display: block; margin-left: auto; margin-right: auto;""""
    } else {
      ""
    }
    val attrs =
      for {
        i <- 0 to attrMap.getLength
        attr = if (i < attrMap.getLength) {
        s"""${attrMap.item(i).getNodeName}="${attrMap.item(i).getNodeValue}""""
        } else ""
      } yield attr

    (style +: attrs).foldRight ("") { (str, result) => s"""$result $str""" }
  }

  private def parseJavaCode(code: String, remark: String): Either[NodeSeq, CompilationUnit] = {
    try {
      val parsedCode = JavaParser parse code
      if (parsedCode.getTypes.isNonEmpty) Right(parsedCode) else Left(<li>{s"$remark is not defined"}</li>)
    } catch {
      case e: Throwable => Left(<li>{e.getMessage}</li>)
    }
  }

  private[snippet] def formatErrorMessage(errorMessage: ErrorMessage): Node = {
    <div>
      Creation or updating card »${errorMessage._1}« failed:
      <ul style="text-align: left; margin-top: 15px; margin-left: 10px;">{errorMessage._2}</ul>
    </div>
  }


}
