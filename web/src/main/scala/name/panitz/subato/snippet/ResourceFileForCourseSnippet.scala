package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class ResourceFileForCourseSnippet(val c:Box[ CoursePage ])
    extends AbstractResourceFileForCourseSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait AbstractResourceFileForCourseSnippet extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
  with L10n  =>



  protected val params:Params
  protected val course:Box[ CoursePage]
  protected def config:Config

  override def dispatch = {
    case "Form" => addForm
    case "List" => list
  }



  def addForm = {
    (course) match {
      case Full(co) => {
        var selectedFile : String = ""
        val files = co.course.files
        var description: String = ""

        def submitted = {
          resourceFileForCourses.createAndSave(selectedFile,description,co.id)
          redirectTo("/ListResourceFilesForCourse/"+co.id)
        }

        "#courseName *"#> Text(co.name)&
        "#fileNames"   #> SHtml.select(files.map(x=>(x,x)), Empty,(s)=>selectedFile = s, "class" -> "myselect") &
        "#submit *"    #> SHtml.submit(locStr("submitNewResourceFileForCourse"), submitted _) &
        "#description" #> SHtml.textarea( ""
                                    , s => { description = s }
                                    , "id" -> "description"
                                    , "style" -> "height: 50px; width: 100%;"
                                    )
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
    }
  }

  def list = {
    (course) match {
      case Full(co) => {
        "#add *" #> <a href={"/AddResourceFileForCourse/"+co.id}>{loc("addResourceFileForCourse")}</a> &
        "#upload *" #> <a href={"/UploadFileForCourse/"+co.id}>{loc("uploadResourceFileForCourse")}</a> &
        "#courseName *" #> Text(loc("course")+" "+co.name) &
        "#row *" #> co.theResources.map(resF => (
              <td>{resF.name}</td>
              <td>{resF.description}</td>
              <td><a href={"/resourcefileforcourse/edit/"+resF.id}>{loc("edit")}</a></td>
              <td><a href={"/resourcefileforcourse/delete/"+resF.id}>{loc("delete")}</a></td>
          )) &
        "#serverFiles " #>
          {
            if (currentUser.isTutorIn(co.course)){
              for (file <- co.course.files)
              yield
                <li>
                   <a href={"/serveHiddenCourseResource/"+co.course.id+"/"+file.replace(".","____").replaceAll("/",":")}>{file}</a>
                  (<a href={"/EditTextCourseResource/"+co.id+"/"+file.replace(".","____").replaceAll("/",":")}>edit</a>)
                  (<a href={"/DeleteTextCourseResource/"+co.id+"/"+file.replace(".","____").replaceAll("/",":")}>delete</a>)
                 </li>
            }else Text("")
          }
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
    }
  }

}
