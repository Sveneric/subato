package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato.model.learnsystem._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}

import scala.xml.{NodeSeq, Text}


class SLSCourseInCourseSnippet(val c:Box[Course])
    extends AbstractSLSCourseInCourseSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[Course])) = {
    this(e._2)
    this.termId = e._1
  }

  override protected var termId:String = null
  override protected val params = ParamsImpl
  override protected val course = c

}

trait AbstractSLSCourseInCourseSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val course:Box[Course]
  protected var termId:String


  def addSLSCourse={
    var name = ""
    var term:Term=null
    var theCourse:Course=null
    var slsCourse:Box[FlashcardCourse] = Empty

    def solutionSubmitted(){
      //      val create = groups.createAndSave(name,theCourse,term)
      val create = MapperSLSCourseInCourseTerms.createAndSave(slsCourse.openOrThrowException("").id,theCourse.id,term.id)
      import net.liftweb.http.S
      S.redirectTo("/ListSLSCourseForCourseAndTerm/"+theCourse.id+"/"+termId)
    }
    
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        term=t
        theCourse=c
        if (currentUser.isLecturerIn(c)){
          "#course *" #> (c.name+"") &
          "#term *" #> ("("+t.name+")") &
          "#createGroup *" #> loc("addSLSCourse") &
          "#name *" #> SHtml.selectObj(
            flashcardCourses.findAll
              .map(x => (x, x.name)), Empty, (g:FlashcardCourse) => slsCourse = Full(g)  )&  //  text("", (text)=> name=text)&
          "#hiddenTerm * " #> SHtml.hidden(()=>()) &
          "#hiddenCourse *" #> SHtml.hidden(()=>())&
          "#create *" #> 
             SHtml.button(loc("create"),solutionSubmitted _,("type","submit"))  //,("onclick","startSpinner()"))
        }else{
          "#courseName *" #> (c.name +" "+loc("groups"))
        }

      }
      case _ =>  "*" #> <span>{loc("noGroupSpecified")}</span>
    }
  }



  def listCourses={
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        "#course *" #> (c.name+"") &
        "#term *" #> ("("+t.name+")") &
        "#create" #> <a href={"/AddSLSCourseForCourseAndTerm/"+c.id+"/"+t.id}>{loc("createEntry")}</a> &
        "#row *" #>
          slsCourseInCourseTerms.findByCourseAndTerm(c.id,t.id)
//            .map(_.flashcardCourse.openOrThrowException(""))
            .filter(_.flashcardCourse.openOrThrowException("").isPublic_?)
            .map(s => <tr>
              <td>{s.flashcardCourse.openOrThrowException("").title}</td>
              <td><a href={"/slscourseincourseterm/edit/"+s.id}>{loc("edit")}</a></td>
              <td><a href={"/slscourseincourseterm/delete/"+s.id}>{loc("delete")}</a></td>
              </tr>
            )
      }
      case _ => loc("noCourseSpecified")
    }
  }

 

}
