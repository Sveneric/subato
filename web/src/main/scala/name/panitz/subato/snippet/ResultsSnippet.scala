package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty}

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.Full
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class ResultsSnippet(e:Box[Exercise])
    extends AbstractResultsSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected var termId:String = null
  override protected var courseId:String = null
  override protected val params = ParamsImpl
  override protected val exercise = e

  def this() {
    this(Empty)
  }
  def this(e:(String,String,Box[Exercise])) = {
    this(e._3)
    this.termId = e._2
    this.courseId = e._1
  }
}

trait AbstractResultsSnippet {
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val exercise:Box[Exercise]
  protected var termId:String
  protected var courseId:String

  def studentsByExercise = exercise match {
    case Full(ex) => {
      if (currentUser.isTutorIn(courseId.toLong,termId.toLong)) {
        import net.liftweb.mapper._
        val users1:List[String] = submissions.find(ex,courseId.toLong,termId.toLong).map((sub)=>sub.uid)
        //alle die die Aufgabe dieses Semester für diesen Kurs abgegeben haben
        val users = users1.distinct

        //und für die brauchen wir doch alle Abgaben?
        val usersAndGroups = users.map( u =>
          (u, userInGroups.findByUid(u, courseId.toLong,termId.toLong).map(_.name).openOr(""))
        ).sortWith((a,b)=>
          a._2 < b._2 || (a._2 == b._2 && a._1 < b._1) //sort by group, then uid
        )

        if (exercise.map(_.isGraded).openOr(false)){
          var rows:List[scala.xml.NodeBuffer] = usersAndGroups.map( uag => {
            val (u, group) = uag
            val testResult = submissions.findBest(u, ex.course.id, ex.id,termId.toLong)
              
            (<td>{studentLink(ex.id,u,termId)}</td>
            <td>{group}</td>
            <td>{testResult.map(_.tests.toString).openOr("-") }</td>
            <td>{testResult.map(_.failures.toString).openOr("-") }</td>
            <td>{testResult.map(_.errors.toString).openOr("-") }</td>
            <td>{testResult.map((x)=>(x.allocations-x.frees).toString).openOr("-")}</td>
            <td>{testResult.map(_.stylecheckErrors.toString).openOr("-")}</td>
            <td>{testResult.map(_.score.toString).openOr("-")}</td>
            <td ></td>)
          })
          "#row *" #> rows
        }else{
          "#row *" #> <b></b> //  rows.head
        }
      } else {
        "#row *" #> loc("accessDenied")
      }
    }
    case _ => loc("noSuchExercise")
  }

  import net.liftweb.ldap._
  def ldapVendor: SimpleLDAPVendor = SimpleLDAPVendor

  def studentTestsByExercise = exercise match {
    case Full(ex) => {
      if (currentUser.isTutorIn(courseId.toLong,termId.toLong)) {
        import net.liftweb.mapper._
        val users1:List[String] = submissions.find(ex,courseId.toLong,termId.toLong).map((sub)=>sub.uid)
        //alle die die Aufgabe dieses Semester für diesen Kurs abgegeben haben
        val users = users1.distinct

        //und für die brauchen wir doch alle Abgaben?
        val usersAndGroups = users.map( u =>
          (u, userInGroups.findByUid(u, courseId.toLong,termId.toLong).map(_.name).openOr(""))
        ).sortWith((a,b)=>
          a._2 < b._2 || (a._2 == b._2 && a._1 < b._1) //sort by group, then uid
        )

        if (exercise.map(_.isGraded).openOr(false)){
          var rows:List[scala.xml.NodeBuffer] = usersAndGroups.map( uag => {
            val (u, group) = uag
            val testResult = submissions.findBestTested(u, ex.course.id, ex.id,termId.toLong)
              
            (<td>{studentLink(ex.id,u,termId)}</td>
              <td>{u}</td>
            <td>{group}</td>
            <td>{testResult.map(_.tests.toString).openOr("-") }</td>
            <td>{testResult.map(_.failures.toString).openOr("-") }</td>
            <td>{testResult.map(_.errors.toString).openOr("-") }</td>
            <td>{testResult.map((x)=>(x.allocations-x.frees).toString).openOr("-")}</td>
            <td>{testResult.map(_.stylecheckErrors.toString).openOr("-")}</td>
            <td>{testResult.map((s)=>(s.tests - s.failures - s.errors).toString).openOr("-")}</td>
            <td ></td>)
          })
          "#row *" #> rows
        }else{
          "#row *" #> <b></b> //  rows.head
        }
      } else {
        "#row *" #> loc("accessDenied")
      }
    }
    case _ => loc("noSuchExercise")
  }

  private def studentLink(exId:Long, uid:String,termId:String):NodeSeq = {
    <a href={"/ExerciseResults/" +exId+"/"+termId  + "/" + uid}>{uid}</a>
  }
}
