package name.panitz.subato.snippet

import scala.xml.{Node, NodeSeq, XML, Text}

import name.panitz.subato.{L10n, SWrapperL10n}
import name.panitz.subato.model.{MapperModelDependencies, ModelDependencies, StatisticData}
import name.panitz.subato.model.learnsystem.{FlashcardCourse, StringUtils}
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._
import net.liftweb.common.{Box, Empty, Full}

trait AbstractStatisticalDataSnippet {
  this: ModelDependencies with L10n =>
  def show: CssSel
}

class StatisticalDataSnippet extends AbstractStatisticalDataSnippet with SWrapperL10n with MapperModelDependencies {

  override def show: CssSel = {
    val nodes = currentUser.uid match {
      case Full(id) => formatForDisplay(flashcardCourses.findAllCreatedBy(id))
      case _        => Text("Unfortunately you do not have access to this area!")
    }
    "#statistical-data-list_ *" #> nodes
  }

  private def formatForDisplay(course: Seq[FlashcardCourse]): Seq[Node] = {
    def fillinPattern(itemIndex: Int, courseName: String, internItems: NodeSeq): Node = {
      val COURSE_NAME_MAX_LENGTH = 100
      val transform: CssSel =
        "#heading [id]"                    #> s"heading$itemIndex"   &
        "#course-dropdown [data-target]"   #> s"#collapse$itemIndex" &
        "#course-dropdown [aria-controls]" #> s"collapse$itemIndex"  &
        "#course-dropdown *"               #> StringUtils.limitStr(courseName, COURSE_NAME_MAX_LENGTH) &
        "#course-dropdown [id]"            #> s"course$itemIndex"    &
        "#collapse [aria-labelledby]"      #> s"heading$itemIndex"   &
        "#collapse [id]"                   #> s"collapse$itemIndex"  &
        "#statistic-rows *"                #> internItems
      transform(StatisticalDataSnippet.statisticalDataExternRowPattern).head
    }
    course.zipWithIndex map {case (course, index) => fillinPattern(index, course.name, formatInterior(course, index))}
  }

  private def formatInterior(flashcardCourse: FlashcardCourse, fccIndex: Int): NodeSeq = {
    def transform(card: name.panitz.subato.model.Card, attempts: Long, statisticBar: Node): Node = {
      val CARD_NAME_MAX_LENGTH = 50
      def transformation: CssSel =
        "#statistic-bar" #> statisticBar &
        "#cardName" #> <a href={card.previewUrl}>{StringUtils.limitStr(card.name, CARD_NAME_MAX_LENGTH)}</a> &
        "#attempts-badge *" #> s"Attempts: $attempts"
      transformation(StatisticalDataSnippet.statisticalDataInternRowPattern).head
    }

    def createStatisticProgressBar(stat: StatisticData): Node = {

      def toUIWidthAttr(value: Double): String = {
        val minLen = if (value == 0) 0 else 34;
        s"width:$value%; min-width:${minLen}px;"
      }

      def transformation: CssSel = stat.isEmpty_? match {
        case false => 
          "#goodAns [style]"         #> toUIWidthAttr(stat.goodAnswerPercent) &
          "#almostGoodAns [style]"   #> toUIWidthAttr(stat.almostGoodAnswerPercent) &
          "#badAns [style]"          #> toUIWidthAttr(stat.wrongAnswerPercent) &
          "#goodAns *"               #> f"${stat.goodAnswerPercent}%.1f%%" &
          "#almostGoodAns *"         #> f"${stat.almostGoodAnswerPercent}%.1f%%" &
          "#badAns *"                #> f"${stat.wrongAnswerPercent}%.1f%%" &
          "#progressBarRoot [title]" #> f"Time Average: ${stat.averageTime}%.1f Sec."
        case _ => "#progressBarRoot *" #> "No data available"
      }
      transformation(StatisticalDataSnippet.statisticProgressBar).head
    }

    flashcardCourse.cards.zipWithIndex
      .map { case (x, index) => {
        val stat = statistics.findCardStatistic(x.id)
        (x, stat.attempts, createStatisticProgressBar(stat)) }
      }
      .map { case (card, attempts, svgNode) => transform(card, attempts, svgNode) }
  }

}

object StatisticalDataSnippet {
  val statisticalDataInternRowPattern: Node = XML.loadFile("./webapps/subato/StatisticalDataInternRow.html")
  val statisticalDataExternRowPattern: Node = XML.loadFile("./webapps/subato/StatisticalDataExternRow.html")
  val statisticProgressBar: Node            = XML.loadFile("./webapps/subato/StatistikProgressBar.html")
}  
