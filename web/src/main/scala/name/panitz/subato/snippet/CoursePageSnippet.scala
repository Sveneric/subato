package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty,Logger}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{S, SHtml}

import scala.xml.{NodeSeq, Text, Node, XML}


class CoursePageSnippet(val c:Box[CoursePage])
    extends AbstractCoursePageSnippet
    with SWrapperL10n
    with MapperModelDependencies{

  def this() {
    this(Empty)
  }
  override protected val coursePage = c
}

trait  AbstractCoursePageSnippet extends   Logger with ClienSideRefresher {
  this:ModelDependencies  with L10n =>

  protected val coursePage:Box[CoursePage]

  def showPage={
    var theCoursePage:CoursePage=null

    (coursePage) match {
      case (Full(cp)) => {
        theCoursePage=cp
        if (theCoursePage.isPublic || currentUser.isLecturerIn(cp.course.id,cp.term.id)){
          val termId = cp.term.id
          val uid = currentUser.uid.openOr("")
          "#lecturer *" #> (locStr("responsiblelecturer")+": "+cp.lecturer) &
          "#lecturers *" #> { if (!cp.theLecturers.isEmpty) List(<h4>{locStr("lecturers")}</h4>, <ul>{cp.theLecturers.map(x => <li>{x}</li>)}</ul>)  else <span/>} &
          "#tutors *" #> {if (!cp.theTutors.isEmpty) List(<h4>{locStr("tutors")}</h4>, <ul>{
            cp.theTutors
              .sortWith((x,y)=>x.name<y.name)
              .map((s) => <li>{s.name}</li>)
          }</ul>) else <span/> } &        
          "#course *" #> (cp.course.name+"") &
          "#term *"   #> (cp.term.name+"") &
          "#mainText *" #> cp.description &
          "#exerciseSheet *" #> {
            val xs = cp.courseExerciseSheets
            <div>
              {if (!xs.isEmpty)  <h4>{loc("exercisesheets")}</h4>}
              {if (!xs.isEmpty)
                <ul>{
                  xs
                    .filter(_.isPublic || currentUser.isTutorIn(cp.course.id,termId))
                    .sortWith((x,y)=>x.number>y.number)
                    .map((s) =>
                      <li>{loc("exerciseSheet")+" "+s.number}.
                        <a href={"/ExerciseSheet/"+s.id}>{s.name}</a>
                        {if (currentUser.loggedIn)
                          List(<br></br> , <span>{
                            s.isGranted(currentUser.uid.openOrThrowException(""))
                              .dmap(<span>{locStr("notGranted")}</span>)((x)=> <span style="background-color:yellow;">{locStr("grantedBy")+" "+x}</span>)


                          }</span>
                            , <span>{
                              if (s.reachablePoints>0){
                                s.getPoints(currentUser.uid.openOrThrowException(""))+"/"+s.reachablePoints+" Punkte"
                              }
                             }</span>
                          )
                        }
                        {
                          if (s.reachablePoints>0){
                             <span><br />Bepunktetes Übungsblatt. Max. Punkzahl {s.reachablePoints}</span>
                        }

                        }
                        {
                          if (s.lastSubmission!=null) List(<br></br>, <b>({loc("lastSubmission")+": "+s.lastSubmission.toString})</b>)
                        }
                        {//s.description
                        }
                        <div>{
                          if (currentUser.isTutorIn(cp.course.id,termId)){
                            <span><a href={"/StudentsBySheetTerm/"+s.id+"/"+termId}>{loc("courseResults")}</a></span>
                          }
                        }{if (currentUser.loggedIn)
                          <span>&nbsp;&nbsp;
                            <a href={"/SheetResults/"+s.id+"/"+termId+"/"+currentUser.uid.openOrThrowException("")}>{loc("results")}</a></span>
                        }</div>
                        </li> )
                }</ul>
              }
            </div>
          } &
          "#trainingGroups *" #> { if (!cp.trainingGroups.isEmpty) List(<h4>{locStr("traininggroups")}</h4>, <ol>{
            cp.trainingGroups
              .sortWith((x,y)=>x.name.compareTo(y.name)<0)
              .map((s) => <li>
                {if (s.lecturer==currentUser.uid.openOr("--------")){
                  <a href={"/ListGroupMembers/"+s.id}>{s.toString}</a>
                }else Text(s.toString)}
                </li>)
          }</ol>) else <span/>} &
          "#flashcardCourses *" #> {
            val fc = cp.theFlashcardCourses
            <div>
              {if (!fc.isEmpty)  <h4>{loc("flashcardcourses")}</h4>}
              {if (!fc.isEmpty)
                <ul>{
                  fc
                    .sortWith((x,y)=>x.title<y.title)
                    .map((s) =>
                      {if(currentUser.loggedIn){
                          val uid = currentUser.uid.openOr("")
                          val subscription =  subscribedFlashcardCourses.getFor(uid, s.id)
                          if(subscription.isEmpty){
                            <li>
                            <span>{s.title}</span><br></br>
                            <span><form method="post" action={"/SlsCourse/"+s.id}> 
                            {
                              def sub() = {
                                subscribedFlashcardCourses createAndSave (uid, s.id);
                                S.redirectTo("/SlsCourse/"+s.id);
                              }
                              SHtml.button(loc("subscribe"), ()=>sub, ("type","submit"),("class","simple"))
                            }
                            </form></span>
	                    </li>
                          }else{
                            val percent =subscription.openOrThrowException("").completion
                            <li>
                              <a href={"/SlsCourse/"+s.id}>{s.title}</a>
                              <div class="progress">
                              <div class="progress-bar"
                                 role="progressbar"
                                 style={"width: "+percent+"%"}
                                 aria-valuenow={""+percent}
                                 aria-valuemin="0"
                                 aria-valuemax="100">{percent+"%"}
                              </div>
                              </div>
                            </li>
                          }
                        }else <li> <a href={"/SlsCourse/"+s.id}>{s.title}</a> </li>
                       }
                       )
                }</ul>}
            </div>
          } &
          "#lectureTimes *" #> { if (!cp.lectureTimes.isEmpty) List(<h4>{locStr("lectureTimes")}</h4>, <ul>{
            cp.lectureTimes
              .sortWith((x,y)=>x.name<y.name)
                        .map((s) => <li>{s.toString}</li>)
          }</ul>) else <span/>} &
          "#admin " #> 
            (if (currentUser.isLecturerIn(cp.course.id,cp.term.id)){
              <div class="dropdown">
                <button class="dropbtn"><h4>{loc("pageAdmin")}</h4></button>
                <div class="dropdown-content">
                  <a href={"/EditCoursePage/"+cp.id}>{loc("editPage")}</a>
                  <a href={"/ListSheetsForCourseAndTerm/"+cp.course.id+"/"+cp.term.id}>{loc("editSheets")}</a>
                  <a href={"/ListSLSCourseForCourseAndTerm/"+cp.course.id+"/"+cp.term.id}>{loc("editSLSCourses")}</a>
                  <a href={"/ListResourceFilesForCourse/"+cp.id}>{loc("editResourceFiles")}</a>
                  <a href={"/ListGroupsForCourseAndTerm/"+cp.course.id+"/"+cp.term.id}>{loc("editGroups")}</a>
                  <a href={"/ListTutorsForCourseAndTerm/"+cp.course.id+"/"+cp.term.id}>{loc("editTutors")}</a>
                  <a href={"/ListCourseMember/"+cp.id}>{loc("listMember")}</a>
                 </div>
              </div>
              } else Text("") ) &
          "#resourceFiles *" #> {
            val res = cp.theResources.filter((r) => r.isResource && (r.isPublic || currentUser.loggedIn))
            <div>
              {if (!res.isEmpty) <h4>{locStr("resourceFiles")}</h4>}
              {if (!res.isEmpty)
                <ul>{
                  res
                    .map(rf => {
                      val name = if (rf.name.indexOf("/") >=0) rf.name.substring(rf.name.lastIndexOf("/")+1) else rf.name
                      <li><a href={"/serveResourceForCourse/"+cp.course.id+"/"+rf.id}>{name}</a><br />{rf.description}</li>
                    })
                }</ul>
              }
            </div> 
          } &
          "#lessons *" #> {
            val les = cp.theLessons
            val formatter =  java.time.format.DateTimeFormatter.ofLocalizedDateTime(java.time.format.FormatStyle.LONG,java.time.format.FormatStyle.SHORT).withLocale(S.locale)
//              java.text.DateFormat.getDateTimeInstance( java.text.DateFormat.FULL, java.text.DateFormat.SHORT, )
            <div >
              {if (cp.isLecturerIn) <a href={"/CreateLessonForCoursePage/"+cp.id}>create new lesson</a>} 
              {if (!les.isEmpty) <h3>{locStr("lessons")}</h3>}
              {if (!les.isEmpty) 
                les.sortWith((x,y)=>x.date.isAfter(y.date)) // most recent lesson first
                .map(le =>
                  <div style="clear: left;">
                    <b>{formatter.format(le.date)}</b>
                    {if (cp.isLecturerIn){
                      <div>
                        <span><a href={"/EditLesson/"+le.id}>edit lesson</a></span>
                        <span><a href={"/lesson/delete/"+le.id}>delete lesson</a></span>
                      </div>
                     }
                    }  <br></br>
                    <div>{le.content}</div>
                    {if (cp.isMember )
                       if (le.hasRated(currentUser)){
                         le.evaluationAsHTML
                       }else
                         <div class="ratingBox" id={le.id+"rating"}>
                           <fieldset class="rating">
                            <input name={le.id+"rating"} type="radio" id={le.id+"rating5"} value="5" />
                            <label for={le.id+"rating5"}></label>
                            <input name={le.id+"rating"} type="radio" id={le.id+"rating4"} value="4" />
                            <label for={le.id+"rating4"}></label>
                            <input name={le.id+"rating"} type="radio" id={le.id+"rating3"} value="3" />
                            <label for={le.id+"rating3"}></label>
                            <input name={le.id+"rating"} type="radio" id={le.id+"rating2"} value="2" />
                            <label for={le.id+"rating2"}></label>
                            <input name={le.id+"rating"} type="radio" id={le.id+"rating1"} value="1" />
                            <label for={le.id+"rating1"}></label>
                            <button onclick={"getRatingValue(\'"+le.id+"\')"}>{loc("submitrating")}</button>
                          </fieldset>
                        </div>
                    }
                  </div>)
              }
            </div>} &
          "#memberCheck *" #>
            { 
              if (cp.isMember){
                <span>{loc("youParticipate")} <a href={"/CoursePageStudentResults/"+cp.id+"/"+uid}>{loc("here")}</a>.</span>
              }else if (currentUser.loggedIn){
                <span>{loc("youDontParticipate")} <a href={"/JoinCoursePage/"+cp.id}>{loc("here")}</a>.</span>
              }else Text("")
            }


        }else {
          "*" #> <span>{loc("notallowed")}</span>
        }
      }


      case _ =>  "*" #> <span>{loc("noCoursePage")}</span>
    }
  }


  def listMember={
    var theCoursePage:CoursePage=null

    (coursePage) match {
      case (Full(cp)) => {
        if (cp.isLecturerIn){
          val member = cp.member
          "#row  " #> member.sortWith((x:UserInGroup,y:UserInGroup)=>x.group.name<y.group.name).map(m => (<tr><td><a href={"/CoursePageStudentResults/"+cp.id+"/"+m.name}>{m.name}</a></td><td>{m.group.name}</td></tr>))
        }else{
           "#row  " #> Text("not allowed")
        }
      }
      case _ =>  "*" #> <span>{loc("noCoursePage")}</span>
    }
  }


  def joinCoursePage={
    (coursePage) match {
      case (Full(cp)) => {
        var group:Box[Group] = Empty

        if (!cp.isMember){
          def joined = {
            println("jetzt die Gruppe eintragen")
            for (g <- group) {
              currentUser.addGroup(g)
            }
            S.redirectTo("/CoursePage/"+cp.id)
          }

          "#groupField" #> SHtml.selectObj(
            cp.trainingGroups
              .map(x => (x, x.name)), Empty, (g:Group) => group = Full(g)  )&
          "#submit *" #> {if (currentUser.loggedIn){
              SHtml.button(loc("joinLecture"),()=>joined,("type","submit"))
            }else {<span>{locStr("cannotJoin")}</span>}} 

        }else {
           "*" #> <span>{loc("allreadyParticipating")}</span>
        }
      }
      case _ =>  "*" #> <span>{loc("noCoursePage")}</span>
    }
  }


  def edit={
    var theCoursePage:CoursePage=null


    (coursePage) match {
      case (Full(cp)) => {
        var isPublic=cp.isPublic
        var lecturer=cp.lecturer
        var description=cp.description
        def asHtml(s:String): Node= try{
          XML.loadString("<div>"+s+ "</div>")
        }catch{
          case e:Exception => XML.loadString("<pre><![CDATA[" +s + "]]></pre>")
        }

        def saved(){
          val mcp = mapper.CoursePage.find(cp.id)
          mcp.map(c =>
            c.isPublic(isPublic)
              .lecturer(lecturer)
              .description(description+"")
              .save)
          S.redirectTo("/CoursePage/"+cp.id)
        }
        if (cp.isLecturerIn){
          "#courseName *" #> cp.course.name &
          "#termName *" #> cp.term.name &
          "#isPublic *" #> SHtml.checkbox(cp.isPublic, (b) => isPublic=b) &
          "#lecturer *" #> SHtml.text(cp.lecturer, (s) => lecturer=s ) &
          "#description *" #> <div>
          <div>
            <script type="text/javascript" src="/static/3dpartyjscript/tiny_mce/js/tinymce/tinymce.min.js"></script>
          <script type="text/javascript">
          <xml:unparsed>
          // <![CDATA[
          tinymce.init({
	    width: 500,
	    mode: "exact",
	    elements: "mce1",
	    // theme: "advanced",
            theme: "modern",
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table contextmenu directionality",
              "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc"
            ],
            toolbar1: "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
            entity_encoding : "numeric",
	    entities: "38,amp,34,quot,60,lt,62,gt"});
          // ]]>
          </xml:unparsed>
          </script>
          <label for="mce1">description</label>
          </div>
          {SHtml.textarea(cp.description+"", (t)=>{description=asHtml(t)}, "id" -> "mce1")}
          </div> &
          "#submit *" #> SHtml.button(loc("save"),()=>saved,("type","submit"))
        }else{
          "#row  " #> Text("not allowed")
        }
      }
      case _ =>  "*" #> <span>{loc("noCoursePage")}</span>
    }
  }
  
  def createLessonForm={
    var theNewLesson:mapper.Lesson= null;
    (coursePage) match {
      case (Full(cp)) => {
        def create(){
          println(""+theNewLesson.date.get+"");
          theNewLesson.save
          S.redirectTo("/CoursePage/"+cp.id)
        }
        if (cp.isLecturerIn){
          theNewLesson = new mapper.Lesson().coursePage(cp.id).date(new java.util.Date())
          "#date *" #> theNewLesson.date.toForm &
          "#course *" #> cp.course.name &
          "#term *" #> cp.term.name &
          "#create *" #> SHtml.button(loc("create"),()=>create,("type","submit")) &
          "#back *" #> <a href={"/CoursePage/"+cp.id}>loc("backToCoursePage")</a>

        }else{
          "#row  " #> Text("not allowed")
        }
      }
      case _ =>  "*" #> <span>{loc("noCoursePage")}</span>

    }
  }
}
