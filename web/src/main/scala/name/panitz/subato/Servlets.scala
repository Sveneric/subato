package name.panitz.subato
import net.liftweb.common.{Box,Full,Empty}
import model.MapperModelDependencies
import scala.xml.Node
import model._

trait Servlets  extends  MapperModelDependencies {
  def register() = {
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveUploadedSolution", exId, uid, timestmp, fileName,ext), _, _) => 
        () => ServeUploadedSolution.response(exId, uid, timestmp, fileName,ext) 
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveResource", exId, fileName), _, _) => 
        () => ServeResource.response(exId,fileName) 
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveResourceForCourse", exId, fileName), _, _) => 
        () => ServeResourceForCourse.response(exId,fileName) 
    }

    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveHiddenResource", exId, fileName), _, _) => 
        () => ServeHiddenResource.response(exId,fileName) 
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveHiddenCourseResource", exId, fileName), _, _) => 
        () => ServeHiddenCourseResource.response(exId,fileName) 
    }

    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveJPlagResult", exId, sheet), _, _) => 
        () => ServeJPlagResult.response(exId,sheet) 
    }

    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveTGZResult", exId, sheet), _, _) => 
        () => ServeTGZResult.response(exId,sheet) 
    }

    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("serveTemplate", exId, "history"), _, _) => 
        () => ServeTemplate.response(exId) 
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("exportExercise", exId, "history"), _, _) => 
        () =>
          if (currentUser.isLecturer) ExportExercise.response(exId)
          else Empty
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("exportCardbox", cbId, "history"), _, _) => 
        () =>
          if (currentUser.isLecturer) ExportCardbox.response(cbId)
          else Empty
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("exportQTI", cbId, "history"), _, _) => 
        () =>
          if (currentUser.isLecturer) ExportQti.response(cbId)
          else Empty
    }

    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("exportAllExercises", courseId, "history"), _, _) => 
        () =>
          if (currentUser.isLecturerIn(courseId.toLong)) ExportAllExercises.response(courseId)
          else Empty
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("exportSlsCourseForCats", slsCourseId, "history"), _, _) =>
        () => if (currentUser.isAuthorOfSlsCourse(slsCourseId.toLong)) ExportSlsCourseForCats.response(slsCourseId)
        else Empty
    }


    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("exportSlsCourse", slsCourseId, "history"), _, _) =>
        () => if (currentUser.isAuthorOfSlsCourse(slsCourseId.toLong)) ExportSlsCourse.response(slsCourseId)
        else Empty
    }
    net.liftweb.http.LiftRules.dispatch.append {
      case net.liftweb.http.Req(List("exportSlsCourseAsCardPDF", slsCourseId, "history"), _, _) =>
        () => ExportSlsCourseAsCardPDF.response(slsCourseId)
        
    }
  }
}

import net.liftweb.http.InMemoryResponse
import net.liftweb.http.LiftResponse
import java.nio.file.{Files, Paths}
import model.mapper.ResourceFile
import model.mapper.ResourceFileForCourse

object ServeResource extends XmlConfigDependency {
  def response(exNr: String,fileId:String): Box[LiftResponse] = {
    val Full(exercise) = MapperExercises.find(exNr.toLong)
    val Full(fileName) = ResourceFile.find(fileId).map(_.name.get)
    val Full(isSrcFile) = ResourceFile.find(fileId).map(_.isSrcFile.get)
    var content: Array[Byte] = null

    if (isSrcFile) {
      content = (exercise.getTestFiles() \ "class").find(x => ((x \@ "name") + "." + (x \@ "lang")).equals(fileName)).map(_.text).get.getBytes
    } else {
      content = Files.readAllBytes(Paths.get(config.exBaseDir + "/" + exNr +"/" + fileName))
    }

    Full(InMemoryResponse(
      data = content,
      headers = "Content-Type" -> "text; charset=utf-8" ::
          "Content-Disposition" -> ("attachment; filename="+fileName) ::
          Nil,
      cookies = Nil,
      code = 200
    ))
  }
}

object ServeResourceForCourse extends XmlConfigDependency {
  def response(courseNr: String,fileId:String): Box[LiftResponse] = {
    val Full(fileName) = ResourceFileForCourse.find(fileId).map(_.name.get)
    val fqn = config.courseBaseDir+"/"+courseNr+"/"+fileName
    val shortName = fqn.substring(fqn.lastIndexOf('/')+1)
    val path = new java.io.File(fileName).toPath();
    val mimeType = java.nio.file.Files.probeContentType(path);
    Full(InMemoryResponse(
      data = Files.readAllBytes(Paths.get(fqn)),
      headers = "Content-Type" ->   mimeType :: // "text; charset=utf-8" ::
        //  "Content-Disposition" -> ("attachment; filename="+shortName) ::
          Nil,
      cookies = Nil,
      code = 200
    ))
  }
}


object ServeHiddenResource extends XmlConfigDependency  with model.MapperModelDependencies {
  def response(exNr: String,fileN:String): Box[LiftResponse] = {
    if (currentUser.isTutorIn(exercises.find(exNr.toLong).openOrThrowException("").course.id)){
      val Full(exercise) = MapperExercises.find(exNr.toLong)

      val fileName = fileN.replace("___", ".")

      val content = (exercise.getTestFiles() \ "class").find(x => ((x \@ "name") + "." + (x \@ "lang")).equals(fileName)).map(_.text)

      Full(InMemoryResponse(
        data = content.get.getBytes(),
        headers = "Content-Type" -> "text; charset=utf-8" ::
          "Content-Disposition" -> ("attachment; filename="+fileName) ::
          Nil,
        cookies = Nil,
        code = 200
      ))
    }else throw new Exception("")
  }
}
object ServeHiddenCourseResource extends XmlConfigDependency  with model.MapperModelDependencies {
  def response(courseNr: String,fileN:String): Box[LiftResponse] = {
    val fileName = fileN.replace("____",".")
    val fqn = config.courseBaseDir+"/"+courseNr+"/"+fileName.replaceAll(":","/")
    val shortName = fqn.substring(fqn.lastIndexOf('/')+1)
    import name.panitz.subato.model._
    if (currentUser.isTutorIn(courseNr.toLong)){
      Full(InMemoryResponse(
        data = Files.readAllBytes(Paths.get(fqn)),
        headers = "Content-Type" -> "text; charset=utf-8" ::
          "Content-Disposition" -> ("attachment; filename="+shortName) ::
          Nil,
        cookies = Nil,
        code = 200
      ))
    }else throw new Exception("")
  }
}


object ServeJPlagResult extends XmlConfigDependency  with model.MapperModelDependencies {
  def response(exNr: String,sheetN:String): Box[LiftResponse] = {
    val fqn = config.exBaseDir+"/"+exNr+"/sheet"+sheetN+"/jplag_result.tar"
    val shortName = fqn.substring(fqn.lastIndexOf('/')+1)
    import name.panitz.subato.model._
    if (currentUser.isTutorIn(exercises.find(exNr.toLong).openOrThrowException("").course.id)){
      Full(InMemoryResponse(
        data = Files.readAllBytes(Paths.get(fqn)),
        headers = "Content-Type" -> "application/gzip" ::
          "Content-Disposition" -> ("attachment; filename="+shortName) ::
          Nil,
        cookies = Nil,
        code = 200
      ))
    }else throw new Exception("User isn't a tutor for this course")
  }
}

object ServeTGZResult extends XmlConfigDependency  with model.MapperModelDependencies {
  def response(exNr: String,sheetN:String): Box[LiftResponse] = {
    val fqn = config.exBaseDir+"/"+exNr+"/sheet"+sheetN+"/tgzResult/submissions.tgz"
    val shortName = fqn.substring(fqn.lastIndexOf('/')+1)
    import name.panitz.subato.model._
    if (currentUser.isTutorIn(exercises.find(exNr.toLong).openOrThrowException("").course.id)){
      val result =
        Full(InMemoryResponse(
          data = Files.readAllBytes(Paths.get(fqn)),
          headers = "Content-Type" -> "application/gzip" ::
            "Content-Disposition" -> ("attachment; filename="+shortName) ::
            Nil,
          cookies = Nil,
          code = 200
        ))
      new java.io.File(fqn).delete;
      result 
    }else throw new Exception("")
  }
}


object ServeUploadedSolution extends XmlConfigDependency with model.MapperModelDependencies {
  def response(exId:String, uid:String, timestmp:String, fileName:String,ext:String): Box[LiftResponse] = {
    val fqn = config.usersBaseDir+"/"+uid+"/"+exId+"/"+timestmp+"/"+fileName+"."+ext
    val shortName = fqn.substring(fqn.lastIndexOf('/')+1)
    val me = currentUser.uid.openOrThrowException("")

    if (me == uid || currentUser.isTutorIn(1)){
    Full(InMemoryResponse(
      data = Files.readAllBytes(Paths.get(fqn)),
      headers = "Content-Type" -> "text; charset=utf-8" ::
          "Content-Disposition" -> ("attachment; filename="+shortName) ::
          Nil,
      cookies = Nil,
      code = 200
    ))
    }else Empty
  }
}


object ServeTemplate  extends MapperModelDependencies{
  def response(exNr: String): Box[LiftResponse] = {
    val Full(ex) = exercises.find(exNr.toLong)
    val shortName = ex.solutionFQCN+"."+ex.fileExtention

    Full(InMemoryResponse(
      data = ex.solutionTemplate.getBytes(java.nio.charset.StandardCharsets.UTF_8),
      headers = "Content-Type" -> "text; charset=utf-8" ::
          "Content-Disposition" -> ("attachment; filename="+shortName) ::
          Nil,
      cookies = Nil,
      code = 200
    ))
  }
}

import model.MapperModelDependencies
object ExportExercise  extends MapperModelDependencies{
  def response(exNr: String): Box[LiftResponse] = {
    val Full(ex) = exercises.find(exNr.toLong)
    val shortName = "ex"+ex.id+".xml"

    Full(InMemoryResponse(
      data = (ex.asXML+"").getBytes(java.nio.charset.StandardCharsets.UTF_8),
      headers = "Content-Type" -> "text/xml; charset=utf-8" ::
          "Content-Disposition" -> ("attachment; filename="+shortName) ::
          Nil,
      cookies = Nil,
      code = 200
    ))
  }
}


object ExportCardbox  extends MapperModelDependencies{
  def response(cbNr: String): Box[LiftResponse] = {
    val Full(cb) = cardBoxes.find(cbNr.toLong)
    val shortName = "cb"+cb.id+".xml"

    val xml = cb.asXML
    val p = new scala.xml.PrettyPrinter(80, 2)
    val d = p.format(xml)
    Full(InMemoryResponse(
        data = d.getBytes(java.nio.charset.StandardCharsets.UTF_8),
        headers = "Content-Type" -> "text/xml; charset=utf-8" ::
            "Content-Disposition" -> ("attachment; filename="+shortName) ::
            Nil,
        cookies = Nil,
        code = 200
      ))
  }
}

object ExportSlsCourse extends MapperModelDependencies {
  def response(courseId: String): Box[LiftResponse] = {
    val Full(fcc) = flashcardCourses.find(courseId.toLong)
    val shortName = s"slsCourse_${fcc.id}_.xml"

    val xml = fcc.asXML
    //dont use. Mixed text gets terrible mixed up....
    //val printer = new scala.xml.PrettyPrinter(80, 2)
    //(val formatedXML = printer.format(xml)
    Full(InMemoryResponse(
        data = xml.toString.getBytes(java.nio.charset.StandardCharsets.UTF_8),
        headers = "Content-Type" -> "text/xml; charset=utf-8" ::
            "Content-Disposition" -> ("attachment; filename="+shortName) ::
            Nil,
        cookies = Nil,
        code = 200
    ))
  }
}

object ExportSlsCourseForCats extends MapperModelDependencies {
  def response(courseId: String): Box[LiftResponse] = {
    val Full(fcc) = flashcardCourses.find(courseId.toLong)
    val shortName = s"slsCourse_${fcc.id}_.xml"

    val xml = fcc.asCatsXML
    //dont use. Mixed text gets terrible mixed up....
    //val printer = new scala.xml.PrettyPrinter(80, 2)
    //(val formatedXML = printer.format(xml)
    Full(InMemoryResponse(
        data = xml.toString.getBytes(java.nio.charset.StandardCharsets.UTF_8),
        headers = "Content-Type" -> "text/xml; charset=utf-8" ::
            "Content-Disposition" -> ("attachment; filename="+shortName) ::
            Nil,
        cookies = Nil,
        code = 200
    ))
  }
}


object ExportSlsCourseAsCardPDF extends MapperModelDependencies with XmlConfigDependency{
  def response(courseId: String): Box[LiftResponse] = {
    val Full(fcc) = flashcardCourses.find(courseId.toLong)
    val shortName = s"slsCourse_${fcc.id}_.xml"

    val xml = fcc.asXML
    import java.io._
    import javax.xml.transform.Source
    import javax.xml.transform.stream.StreamSource
    import javax.xml.transform.TransformerFactory
    import javax.xml.transform.stream.StreamResult

    def transform(xslt: Source ,doc:Source, writer: Writer  )= {
      val t = TransformerFactory.newInstance().newTransformer(xslt)
      t.transform(doc,new StreamResult(writer))
      writer.close()
    }
    val texFile = File.createTempFile("cardBox"+fcc.id+"-" ,".tex")
    val texFileLocalName = texFile.getName()
    val dotIndex = texFileLocalName.lastIndexOf(".")
    val nakedName = if (dotIndex>=0)texFileLocalName.substring(0,dotIndex)else texFileLocalName
    val dir  = texFile.getParent()

    val pdfFileName = dir+"/"+nakedName+".pdf"
    transform(new StreamSource(new java.io.File(config.baseDir+"/xsl/cardsforbox.xsl"))
      ,new StreamSource(new java.io.StringReader(xml.toString))
      ,new OutputStreamWriter(new FileOutputStream(texFile),"utf8"))

    import sys.process._
    Process("xelatex -shell-escape -interaction=nonstopmode "+texFile.getAbsolutePath()+" -output-directory="+dir,texFile.getParentFile).!
      texFile.delete()
    val pdf=Files.readAllBytes(Paths.get(pdfFileName))
    try {
      new File(pdfFileName).delete()
      new File(dir+"/"+nakedName+".log").delete()
      new File(dir+"/"+nakedName+".aux").delete()
    }catch{
       case _:Exception =>
    }
  
    Full(InMemoryResponse(
        data = pdf,
        headers = "Content-Type" -> "application/pdf" ::
            "Content-Disposition" -> ("attachment; filename="+nakedName+".pdf") ::
            Nil,
        cookies = Nil,
        code = 200
    ))
  }
}

import model.MapperModelDependencies
object ExportQti
    extends MapperModelDependencies{
  def response(cbNr: String): Box[LiftResponse] = {
    val Full(cb) = cardBoxes.find(cbNr.toLong)
    val shortName = "cb"+cb.id+".xml"

    Full(InMemoryResponse(
        data = (cb.toQTI+"").getBytes(java.nio.charset.StandardCharsets.UTF_8),
        headers = "Content-Type" -> "text/xml; charset=utf-8" ::
            "Content-Disposition" -> ("attachment; filename="+shortName) ::
            Nil,
        cookies = Nil,
        code = 200
      ))
  }
}



object ExportAllExercises
    extends MapperModelDependencies{
  def response(cId: String): Box[LiftResponse] = {
    val Full(c) = courses.find(cId.toLong)
    val shortName = c.name+".xml"

    Full(InMemoryResponse(
      data = (c.asXML+"").getBytes(java.nio.charset.StandardCharsets.UTF_8),
      headers = "Content-Type" -> "text/xml; charset=utf-8" ::
          "Content-Disposition" -> ("attachment; filename="+shortName) ::
          Nil,
      cookies = Nil,
      code = 200
    ))
  }
}


