package bootstrap.liftweb

import name.panitz.subato.TheSiteMap
import net.liftmodules.{FoBo, JQueryModule}
import net.liftweb._
import net.liftweb.common._
import net.liftweb.http._
import net.liftweb.http.js.jquery._
import net.liftweb.ldap.SimpleLDAPVendor
import net.liftweb.mapper.{DB, DefaultConnectionIdentifier, StandardDBVendor}
import net.liftweb.sitemap.Loc._
import net.liftweb.sitemap._
import net.liftweb.util._

/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 */
class Boot  extends Logger
    with name.panitz.subato.model.mapper.Schemifier
    with name.panitz.subato.Servlets
    with name.panitz.subato.XmlConfigDependency{
  def boot {
/*    LiftRules.securityRules = () => {
      SecurityRules(content = Some(ContentSecurityPolicy(
        scriptSources = List(ContentSourceRestriction.Self,
          ContentSourceRestriction.UnsafeInline,
          ContentSourceRestriction.UnsafeEval)
      )))
    }
 */
    LiftRules.stdRequestTimeout = Full(30)

    // where to search snippet
    LiftRules.addToPackages("name.panitz.subato")

    LiftRules.ajaxPostTimeout=20000 

    FoBo.Toolkit.Init=FoBo.Toolkit.JQuery310
    FoBo.Toolkit.Init=FoBo.Toolkit.Bootstrap337
    FoBo.Toolkit.Init=FoBo.Toolkit.FontAwesome463

    SimpleLDAPVendor.parameters =  Map("ldap.url" -> config.ldapUrl,
      "ldap.base" -> "dc=inf,dc=hs-rm,dc=de") //"ou=Users,dc=cs,dc=fh-wiesbaden,dc=de")

    if (!DB.jndiJdbcConnAvailable_?) {
      println("db.user="+ Props.get("db.user"))
      println("db.password="+ Props.get("db.password"))
      val vendor =
	new StandardDBVendor(Props.get("db.driver") openOr "org.h2.Driver",
					Props.get("db.url") openOr
					"jdbc:h2:/var/opt/subato/db/lift_proto_live.db;AUTO_SERVER=FALSE;ACCESS_MODE_DATA=rws", //TODO read from config
    Full(""), Full(""))

      LiftRules.unloadHooks.append(vendor.closeAllConnections_! _)
      DB.defineConnectionManager(DefaultConnectionIdentifier, vendor)
    }

    schemify
    register

    // where to search snippet
    LiftRules.addToPackages("name.panitz.subato")
    
    // Build SiteMap
    val entries = List(
      Menu.i("Home") / "index", // the simple way to declare a menu

      // more complex because this menu allows anything in the
      // /static path to be visible
      Menu(Loc("Static", Link(List("static"), true, "/static/index"),
	       "Static Content")))


    // set the sitemap.  Note if you don't want access control for
    // each page, just comment this line out.
    //LiftRules.setSiteMap(SiteMap(entries:_*))

    // Build SiteMap
    LiftRules.setSiteMapFunc(TheSiteMap.siteMap _)


    //Show the spinny image when an Ajax call starts
    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)

    // Make the spinny image go away when it ends
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))


    //Init the jQuery module, see http://liftweb.net/jquery for more information.
    LiftRules.jsArtifacts = JQueryArtifacts
    JQueryModule.InitParam.JQuery=JQueryModule.JQuery1113
    JQueryModule.init()
/*
    LiftRules.securityRules = () => {
      SecurityRules(content = Some(ContentSecurityPolicy(
        scriptSources = List(
            ContentSourceRestriction.Self),
        styleSources = List(
            ContentSourceRestriction.Self)
      )))}
 */    
    LiftRules.securityRules = () => {
      SecurityRules(content = Some(ContentSecurityPolicy(
        defaultSources = List(ContentSourceRestriction.All,ContentSourceRestriction.Self,ContentSourceRestriction.UnsafeInline,ContentSourceRestriction.UnsafeEval),
        scriptSources =  List(ContentSourceRestriction.All,ContentSourceRestriction.Self,ContentSourceRestriction.UnsafeInline,ContentSourceRestriction.UnsafeEval,ContentSourceRestriction.Scheme("blob")),
          //styleSources = List(ContentSourceRestriction.All))
        imageSources = List(ContentSourceRestriction.All,ContentSourceRestriction.Scheme("data"))
      )))
    }

  }

}
