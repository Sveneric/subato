
const SELECTABLE_STATUS = "selectable";
const SELECTED_STATUS = "selected";
let localCardId = -1;
let answerId = 1;

function placeLoadingIcon(containerId, iconId) {
    if ($(`#${iconId}`).id === undefined) {
        const icon = `<div style="text-align: center;" id="${iconId}"><i style="font-size: 30px;" class="fa fa-spinner fa-spin"></i></div>`;
        $(`#${containerId}`).prepend(icon);
    }
}

function removeLoadingIcon(iconId) {
    $(`#${iconId}`).remove();
}

function genNewCardId() {
    return localCardId--;
}

function genNewAnswerId() {
    return answerId++;
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    let cardId = ev.target.getAttribute("card-id");
    ev.dataTransfer.setData("text", cardId);
}

function drop(ev) {
    ev.preventDefault();
    const cardId = ev.dataTransfer.getData("text");
    if (isSelectableCard(cardId)) {
        changeCardStatus(cardId, SELECTED_STATUS);
    } else if (isSelectedCard(cardId)) {
        changeCardStatus(cardId, SELECTABLE_STATUS);
    }
    updateNumberOfSelectedCards();
    showCards();
}

function isSelectableCard(id) {
    let qualifiedCard = extractCardById(id);
    return defined(qualifiedCard) && (qualifiedCard.status === SELECTABLE_STATUS);
}

function isSelectedCard(id) {
    let qualifiedCard = extractCardById(id);
    return defined(qualifiedCard) && (qualifiedCard.status === SELECTED_STATUS);
}

function changeCardStatus(cardId, newStatus) {
    let card = extractCardById(cardId);
    if (defined(card)) {
        card.status = newStatus;
        return card;
    }
    return null;
}

function updateNumberOfSelectedCards() {
    $("#number-choosed-cards").text(`Number: ${getSelectedCards().length}`);
}

function buildStatisticBar(statisticJson) {
    if(! defined(statisticJson)) {
        return `<div id="progressBarRoot" data-toggle="tooltip" data-placement="top" class="progress" style="width: 300px; 
                margin-left: 18%; height: 18px;" title="Time Average: No data available.">`
    }
    let time = ("" + statisticJson.avrTime).replace(".", ",");
    return `
      <div id="progressBarRoot" data-toggle="tooltip" data-placement="top" class="progress" style="width: 300px; 
        margin-left: 18%; height: 18px;" title="Time Average: ${time} Sec.">
        ${toStatisticBarItem(statisticJson.goodAns, "goodAns", "success")}
        ${toStatisticBarItem(statisticJson.almostGoodAns, "almostGoodAns", "warning")}
        ${toStatisticBarItem(statisticJson.wrongAns, "badAns", "danger")}
     </div>
    `;
}

function toStatisticBarItem(value, id, severity) {
    value = value.toFixed(1);
    const minLen = value === 0 ? 0 : 34;
    return `
      <div style="width: ${value}%; min-width:${minLen}px;" role="progressbar" id="${id}" class="progress-bar 
        bg-${severity}">${("" + value).replace(".", ",")}%</div>
    `
}

function extractCardById(cardId) {
    for (let i = 0; i < allCards.length; i++) {
        if (allCards[i].id.toString() === cardId.toString()) return allCards[i];
    }
}

function definedAndNonEmpty(container) {
    return defined(container) && (container.length !== 0)
}

function defined(item) {
    return (item !== undefined) && (item !== null)
}

function refreshSearchOptions() {
    let cardOptions = cardTopics.reduce((result, next) => `${result}<option value="${next}">`, "");
    const courseOptions = myCourses.reduce((result, next) => `${result}<option value="${next.name}(id: ${next.id})">`, "");
    $("#topicsDB").html(cardOptions);
    $("#my-course-names").html(courseOptions);
}

function getSelectedCards() {
    return allCards
        .filter(x => x.status.trim().toLowerCase() === SELECTED_STATUS)
        .sort((a, b) => a - b); // TODO: Sort by name
}

function getSelectableCards() {
    return allCards.filter(x => x.status.trim().toLowerCase() === SELECTABLE_STATUS)// TODO: Sort by name
}

function refreshCardsContainers(newSelectablesCards, newSearchableTopics) {
    cardTopics = newSearchableTopics.filter(x => definedAndNonEmpty(x.trim()));
    let selectedCards = getSelectedCards();
    let selectedCardIds = selectedCards.map(x => x.id);
    let selectableCards = newSelectablesCards.filter(x => !selectedCardIds.includes(x.id));
    allCards = selectedCards.concat(selectableCards);
    refreshSearchOptions();
}

function jsonifyCourseToBeCreated() {
    return {
        "name": $("#courseName").val(),
        "description": $("#courseDescription").val(),
        "privacy": getCoursePrivacy(),
        "topics": $("#courseTopics").val(),
        "id": $('#create-course-btn').attr('course-id'),
        "cards": getSelectedCards()
    };
}

function addOrReplaceIfExist(newElem) {
    let replacementOccurred = false;
    for (let i = 0; i < allCards.length; i++) {
        if (`${allCards[i].id}` === `${newElem.id}`) {
            allCards[i] = newElem;
            replacementOccurred = true;
            break;
        }
    }
    if (! replacementOccurred) { allCards.unshift(newElem); }
}

function addNewCardIfValid() {
    const newCard = createNewCard();
    const checkResult = isCardValid(newCard);
    if (checkResult.isSuccessfully) {
        newCard.status = definedAndNonEmpty(newCard.status) ? newCard.status : SELECTED_STATUS;
        addOrReplaceIfExist(newCard);
        resetEditorsContent();
        resetCardCreationButton();
        deleteAnswers();
        showCards();
        swal({"title": "Operation success!", "timer": 750, "icon": "success"});
    } else {
        const errors = checkResult.msgContainer.reduce((result, next) => `${result}<li>${next}</li>`, '');
        swal({
            "title": "Can't create new card",
            "closeOnClickOutside": false,
            "icon": "error",
            "content":  $.parseHTML(`<ul style="text-align: left; margin-top: 15px;">${errors}</ul>`)[0],
            "buttons": {
                "confirm": {
                    "text": "OK",
                    "value": true
                },
            }
        });
    }
}

function editCard(ev) {
    const cardId = ev.target.getAttribute("card-id");
    let card = extractCardById(cardId);
    if (defined(card) && card.customizable) {
        switch (card.type) {
            case 'programming':/*setupEditorForProgrammingCard(card);*/ break;
            case 'clozetext': break;
            case 'freetext': setupEditorForFreetextCard(card); break;
            default: setupEditorForSimpleCard(card);
        }
    }
}

function setupEditorForProgrammingCard(card) {
    commonEditorSetupForCard(card);
    $(`#${programmingLanguageId}`).val(card.programmingLanguage);
    const code = `${card.initialSnippet}\n${card.tests}\n${card.answers.correctAnswers[0]}`;
    resetEditorsContent(card.question, "", card.explanation, code);
}

function shouldCleanOtherCard(course) {
    let alertContent = "";
    let buttons = "";
    if (getSelectedCards().length === 0) {
        alertContent = `<p><b><em>${course.title}</em></b> successfully imported!</p>`;
        buttons = "confirm";
    } else {
        alertContent =  `<div>You have just imported the cards of course <b><em>${course.title}</em></b>. What should happen to those <b>already selected</b>?</div>`
        buttons = {
            "confirm": {
                "text": "keep",
                "value": true
            },
            "discard": {
                "text": "discard",
                "value": false,
                "className": "red-alert-btn"
            }
        }
    }
    swal({
        "title": `Import success`,
        "closeOnClickOutside": false,
        "icon": "success",
        "content":  $.parseHTML(alertContent)[0],
        "buttons": buttons
    }).then(
        value => {
            if (!value) {
                course.cards.forEach(x => x.status = SELECTED_STATUS);
                allCards = allCards.filter(x => x.status === SELECTABLE_STATUS);
                course.cards.forEach(x => allCards.push(x));
                $("#courseName").val(course.title);
                $("#courseDescription").val(course.description);
                $("#courseTopics").val(course.topics.reduce((result, next) => `${result}, ${next}`, ""));
                $('#create-course-btn').text("Update course");
                $('#create-course-btn').attr("course-id", `${course.id}`)
            } else {
                course.cards.forEach(x => x.status = SELECTED_STATUS);
                course.cards.forEach(x => { if (allCards.filter(y => y.id === x.id).length === 0) allCards.push(x)});
                $('#create-course-btn').attr("course-id", "")
            }
            showCards();
            removeLoadingIcon("loading-course-icon");
        }
    );
}



function searchedCourseId() {
    const selectedValue = $("#research-in").val();
    const idStart = selectedValue.indexOf("id: ");
    const idEnd = selectedValue.lastIndexOf(")");
    if (idStart >= 0 && idEnd >= 0){
        placeLoadingIcon("nav-choosed-cards", "loading-course-icon");
        return selectedValue.substring(idStart + 4, idEnd);
    }
    return "-1"
}

function searchedCardTopic() {
    placeLoadingIcon("nav-all-cards", "loading-card-icon");
    return extractElementValueById("topicSearch");
}

function setupEditorForFreetextCard(card) {
    commonEditorSetupForCard(card);
    resetEditorsContent(card.question, card.answers.correctAnswers[0], card.explanation, "");
}

function setupEditorForSimpleCard(card) {
    commonEditorSetupForCard(card);
    resetEditorsContent(card.question, "", card.explanation, "");
    const answersHtml = $(`#${answersId}`);
    card.answers.correctAnswers.forEach(x => answersHtml.append(createAnswerFrom($($.parseHTML(x)), true)));
    card.answers.incorrectAnswers.forEach(x => answersHtml.append(createAnswerFrom($($.parseHTML(x)))));
}

function commonEditorSetupForCard(card) {
    $(`#${cardtypeId}`).val(card.type);
    clickOnCardCreationTab();
    changeCardCreationButtonToUpdate(card);
    $(`#card-title`).val(card.title);
    $(`#card-topics`).val(card.topics.reduce((result, next) => `${result}, ${next}`, ""));
}

function clickOnCardCreationTab() {
    const cardCreationTab = $('#nav-create-cards-tab');
    cardCreationTab.trigger("click");
}

function destroyCard(ev) {
    const cardId = ev.target.getAttribute("card-id");
    const containerId = `card-${cardId}`;
    $(`#${containerId}`).remove();
    allCards = allCards.filter(x => `${x.id}` !== `${cardId}`);
    showCards();
}

function changeCardCreationButtonToUpdate(card) {
    const btn = $("#create-card-btn");
    btn.text("Update card");
    btn.attr("cId", `${card.id}`);
}

function resetCardCreationButton() {
    const btn = $("#create-card-btn");
    btn.text("Create Card");
    btn.attr("cId", "");
}

function isCardValid(card) {
    let errorMsgContainer = [];
    check(card.title, 'Empty card title is not allowed', errorMsgContainer);
    check(card.topics.length,'The new card should cover at least one topic', errorMsgContainer);
    check($(card.question).text(), 'Empty card question is not allowed', errorMsgContainer);
    check($(card.explanation).text(), 'Empty card explanation is not allowed', errorMsgContainer);
    check(card.answers.correctAnswers, 'The new card should contains at least one correct answer', errorMsgContainer);
    if (card.type === 'programming') {
        check(card.tests, 'A programming card should contains at least one test', errorMsgContainer);
    }
    return {"isSuccessfully": ! definedAndNonEmpty(errorMsgContainer), "msgContainer": errorMsgContainer};
}

function check(value, errMsgIfEmptyOrUndefined, msgBuffer) {
    if (!definedAndNonEmpty(value)) msgBuffer.push(errMsgIfEmptyOrUndefined);
}

const cardTitleInputId = "card-title";
const defaultBloomsLevel = "Remember";
const cardCreationButton = "create-card-btn";

function createNewCard() {
    const hiddenId = $(`#${cardCreationButton}`).attr('cId');
    const id = definedAndNonEmpty(hiddenId) ? hiddenId.trim() :  genNewCardId();
    let cardJson = {
        "id": id,
        "newCard": id < 0,
        "customizable": true,
        "title": extractCardTitle(),
        "question": extractQuestion(),
        "explanation": extractExplanation(),
        "bloomslevel": defaultBloomsLevel,
        "topics": extractCardTopics(),
        "type": extractCardType(),
        "answers": extractAnswers()
    };
    if (cardJson.type === "programming") {
        cardJson.initialSnippet = extractInitialSnippet();
        cardJson.programmingLanguage = textContentOfElementById(programmingLanguageId);
        cardJson.tests = extractTestCode()
    }
    return cardJson;
}

function jsonToVisibleCard(jsonCard) {
    const parentId = jsonCard.status === SELECTED_STATUS ? "#selected-cards" : "#selectable-cards";
    return `
    <div class="card" id="card-${jsonCard.id}">
        <div class="card-header" id="heading${jsonCard.id}">
            <h5 class="mb-0">
                <button id="card${jsonCard.id}" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse${jsonCard.id}" aria-expanded="false" aria-controls="collapse${jsonCard.id}">
                     ${jsonCard.title}   
                </button>
            </h5>
        </div>
        <div id="collapse${jsonCard.id}" class="collapse" aria-labelledby="heading${jsonCard.id}" data-parent="${parentId}">
            <div class="card-body">
                <div class="card mb-3 sls-studio-sub-card border-info" card-id="${jsonCard.id}" draggable="true" ondragstart="drag(event)">
                    <div style="clear:left;" id="hidden"></div>
                    <div class="card-header sls-studio-card-header" style="border-color: rgba(23, 162, 184, 0.4);">
                        <div><h3 class="sls-card-name" style="text-align: center;" id="name">${jsonCard.title}</h3></div>
                        <div>
                            <i style="position: absolute; right: 19%; color: red; cursor: pointer;" card-id="${jsonCard.id}" class="far fa-window-close" id="selectable-destroy-icon" onclick="destroyCard(event);" data-toggle="tooltip" title="Destroy card"></i>
                            <i onclick="editCard(event);" id="selectable-edit-icon" card-id="${jsonCard.id}" class="fas fa-edit" style="position: absolute; right: 14%;  cursor: pointer; color: #0056b3;" ></i>
                            <span style="position: absolute; right: 2%;" class="badge badge-pill badge-warning" id="card-topics" data-toggle="tooltip" title="${jsonCard.topics}">Topics</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="question" style="clear:left;" id="question">${jsonCard.question}</div>
                    </div>
                    <div id="statisticBar" class="card-footer sls-studio-card-footer">${buildStatisticBar(jsonCard.statistic)}</div>
                </div>
            </div>
        </div>
    </div>
    `;
}

function showCards() {
    const selectableCardsContainer = $(`#selectable-cards`);
    const selectedCardsContainer = $(`#selected-cards`);
    selectableCardsContainer.html("");
    selectedCardsContainer.html("");
    getSelectableCards().forEach(x => selectableCardsContainer.append(jsonToVisibleCard(x)));
    getSelectedCards().forEach(x => selectedCardsContainer.append(jsonToVisibleCard(x)));
    updateNumberOfSelectedCards();
}

function reduceSizeIfPossible(card) {
    if (! isNewCard(card)) {
        return {"newCard": false, "id": card.id};
    }
    return card;
}

function isNewCard(card) {
    return defined(card.newCard) && card.newCard;
}

function getCoursePrivacy() {
    if ($("#coursePrivacy").val().trim().toLowerCase().includes("private")) return "private";
    return "public";
}

refreshSearchOptions();
