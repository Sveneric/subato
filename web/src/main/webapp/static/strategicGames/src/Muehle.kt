interface Move {
    fun move(m: Muehle): Muehle
}

class Position(var triblet: Byte, var pos: Byte) {
    override fun toString(): String {
        return "($triblet,$pos)"
    }

    fun connectedPositions(): List<Position> {
        val result = mutableListOf<Position>()
        if (pos % 2 == 1) {
            if (triblet.toInt() == 0 || triblet.toInt() == 2)
                result.add(Position(1.toByte(), pos))
            else {
                result.add(Position(0.toByte(), pos))
                result.add(Position(2.toByte(), pos))
            }
        }
        result.add(Position(triblet, ((pos + 7) % 8).toByte()))
        result.add(Position(triblet, ((pos + 1) % 8).toByte()))

        return result
    }

    fun rows(): List<Array<Position>> {
        val result = mutableListOf<Array<Position>>()
        if (pos % 2 == 1) {
            val r1 = arrayOf(Position(0.toByte(), pos), Position(1.toByte(), pos), Position(2.toByte(), pos))

            val r2 = arrayOf(Position(triblet, ((pos + 7) % 8).toByte()), Position(triblet, pos), Position(triblet, ((pos + 1) % 8).toByte()))
            result.add(r1)
            result.add(r2)
        } else {
            val r1 = arrayOf(Position(triblet, ((pos + 1) % 8).toByte()), Position(triblet, pos), Position(triblet, ((pos + 2) % 8).toByte()))

            val r2 = arrayOf(Position(triblet, ((pos + 7) % 8).toByte()), Position(triblet, pos), Position(triblet, ((pos + 6) % 8).toByte()))
            result.add(r1)
            result.add(r2)
        }

        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class.js != other::class.js) return false

        other as Position

        if (triblet != other.triblet) return false
        if (pos != other.pos) return false

        return true
    }

    override fun hashCode(): Int {
        var result = triblet.toInt()
        result = 31 * result + pos
        return result
    }

}

internal interface SetStone : Move {
    val position: Position
}

internal class MoveStone(var from: Position, override var position: Position) : SetStone {


    override fun move(m: Muehle): Muehle {
        val result = m.copy()
        //result.lastColumn=to.triblet;
        //result.lastRow=to.pos;
        result.playerStartStones[m.player % 2] = m.playerStartStones[m.player % 2]
        result.playerStartStones[m.nextPlayer() % 2] = m.playerStartStones[m.nextPlayer() % 2]
        result.b[position.triblet.toInt()][position.pos.toInt()] = m.player
        result.b[from.triblet.toInt()][from.pos.toInt()] = m.playerNone
        if(result.inMuehle(position)) {
            result.removing = true
            println("REMOVING WEGEN MÜHLE")
            return result;
        }
        result.movesDone = m.movesDone + 1
        result.player = m.nextPlayer()
        return result
    }

    override fun toString(): String {
        return "[$from -> $position]"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class.js != other::class.js) return false

        other as MoveStone

        if (from != other.from) return false
        if (position != other.position) return false

        return true
    }

    override fun hashCode(): Int {
        var result = from.hashCode()
        result = 31 * result + position.hashCode()
        return result
    }

}

internal class Set : SetStone {
    override var position: Position

    constructor(pos: Position) {
        this.position = pos
    }

    constructor(tr: Byte, p: Byte) {
        this.position = Position(tr, p)
    }

    override fun move(m: Muehle): Muehle {
        val result = m.copy()
        result.playerStartStones[m.player % 2] = (m.playerStartStones[m.player % 2] - 1) .toByte()
        result.playerStartStones[m.nextPlayer() % 2] = m.playerStartStones[m.nextPlayer() % 2]
        result.b[position.triblet.toInt()][position.pos.toInt()] = m.player

        if(result.inMuehle(position)) {
            result.removing = true
            return result;
        }
        result.movesDone = m.movesDone + 1
        result.playerStartStones[m.player % 2] = (m.playerStartStones[m.player % 2] - 1) .toByte()
        result.playerStartStones[m.nextPlayer() % 2] = m.playerStartStones[m.nextPlayer() % 2]
        result.b[position.triblet.toInt()][position.pos.toInt()] = m.player
        result.player = m.nextPlayer()
        return result
    }

    override fun toString(): String {
        return "->" + position
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || other !is Move) return false

//        other as Move
        if (other is Remove){
//            other as Remove
            return this.equals(other.myMove)
        }
        if (other !is SetStone) return false
//        other as SetStone
        if (position != other.position) return false

        return true
    }

    override fun hashCode(): Int {
        return position.hashCode()
    }


}

internal class Remove(var myMove: SetStone, pos: Position) : Move {
    var rem: RemoveStone

    init {
        this.rem = RemoveStone(pos)
    }

    override fun move(m: Muehle): Muehle {
        val result = myMove.move(m)
        rem.move(result)
        return result
    }

    
    override fun toString(): String {
        return "+$myMove-$rem"
    }
}

internal class RemoveStone : SetStone {
    override var position: Position

    constructor(pos: Position) {
        this.position = pos
    }

    constructor(tr: Byte, p: Byte) {
        this.position = Position(tr, p)
    }

    override fun move(m: Muehle): Muehle {
        val result = m.copy()
        m.b[position.triblet.toInt()][position.pos.toInt()] = m.playerNone
        m.playerStones[m.player % 2] =(m.playerStones[m.player % 2] - 1).toByte()
        result.removing = false
        result.player = result.nextPlayer()
        return result
    }

    override fun toString(): String {
        return "" + position
    }
}


class Muehle : AbstractRegularGame<Move>(3, 8) {
    override fun createMove(column: Byte, row: Byte): Move? {
        println("createMove($column,$row)")
        if (!removing) {
            println("!removing")
            println("movesDone: $movesDone")
            if (movesDone < 18) return Set(Position(column, row))
            if (marked == null) {
                marked = Position(column, row)
                return null
            }
            val from = marked!!
            marked=null
            return MoveStone(from, Position(column, row))
        }
        return RemoveStone(Position(column,row))
    }

    var playerStartStones = byteArrayOf(9, 9)
    var playerStones = byteArrayOf(9, 9)
    var marked: Position? = null
    var removing = false



    var prevState: Muehle? = null

    override fun getAtPosition(column: Byte, row: Byte): Byte {
        return b[column.toInt()][row.toInt()]
    }

    fun atPos(p: Position): Byte {
        return b[p.triblet.toInt()][p.pos.toInt()]
    }

    override fun legalMoves(): List<Move> {
        val result = mutableListOf<Move>()
        //first move
/*        if (movesDone == 0) {
            result.add(Set(1.toByte(), 0.toByte()))
            result.add(Set(2.toByte(), 0.toByte()))
            result.add(Set(1.toByte(), 1.toByte()))
            result.add(Set(2.toByte(), 1.toByte()))
            return result
        }*/

        val myStones = mutableListOf<SetStone>()
        //StartGame
        if (playerStartStones[player % 2] > 0) {
            for (trs in 0 until columns) {
                for (pos in 0 until rows) {
                    if (b[trs][pos] == playerNone) {
                        myStones.add(Set(trs.toByte(), pos.toByte()))
                //        movesDone += 1
                    }
                }
            }
            //MiddleGame
        } else if (playerStones[player % 2] > 3) {
            for (trs in 0 until columns) {
                for (pos in 0 until rows) {
                    if (b[trs][pos] == player) {
                        val from = Position(trs.toByte(), pos.toByte())
                        for (to in from.connectedPositions()) {
                            if (b[to.triblet.toInt()][to.pos.toInt()] == AbstractRegularGame.NONE) {
                                myStones.add(MoveStone(from, to))
                            }
                        }
                    }
                }
            }
            //Endgame
        } else {
            var trs: Byte = 0
            while (trs < columns) {
                var pos: Byte = 0
                while (pos < rows) {
                    val from = Position(trs, pos)
                    if (player == atPos(from)) {
                        val trs2: Byte = 0
                        while (trs < columns) {
                            val pos2: Byte = 0
                            while (pos < rows) {
                                val to = Position(trs2, pos2)
                                if (playerNone == atPos(to)) {
                                    myStones.add(MoveStone(from, to))
                                }
                                pos++
                            }
                            trs++
                        }
                    }
                    pos++
                }
                trs++
            }
        }
        //Remove if Muehle!
        for (myStone in myStones) {
            val p = myStone.position
            if (!this.doMove(myStone).inMuehle(p))
                result.add(myStone)
            else {
                for (take in stonesToTake(nextPlayer())) {
                    result.add(Remove(myStone, take))
                }
            }
        }

        return result
    }

    fun inMuehle(p: Position): Boolean {
        val c = atPos(p)
        for (row in p.rows()) {
            val sum = atPos(row[0]).toInt() + atPos(row[1]).toInt() + atPos(row[2]).toInt()
            if (sum == 3 * c) return true
        }
        return false
    }

    fun stonesToTake(pl: Byte): List<Position> {
        val result = mutableListOf<Position>()
        for (c in 0 until columns) {
            for (r in 0 until rows) {
                val p = Position(c.toByte(), r.toByte())
                if (b[c][r] == pl && !inMuehle(p))
                    result.add(p)
            }
        }
        return result
    }

    fun copy(): Muehle {
        val result = Muehle()
        result.lastColumn = lastColumn;
        result.lastRow=lastRow
        result.movesDone=movesDone
        result.player=player

        result.marked = if (marked== null) null else Position(marked!!.triblet,marked!!.pos)
        result.removing = removing


        for (c in 0 until columns)
            for (r in 0 until rows)
                result.b[c][r] = b[c][r]

        result.playerStartStones[0] = playerStartStones[0]
        result.playerStartStones[1] = playerStartStones[1]
        result.playerStones[0] = playerStones[0]
        result.playerStones[1] = playerStones[1]
        return result
    }

    override fun doMove(m: Move): Muehle {
        val result = m.move(this)
        result.prevState = this
        return result
    }

    fun sum(a1: Int, a2: Int, b1: Int, b2: Int, c1: Int, c2: Int): Int {
        return b[a1][a2].toInt() + b[b1][b2].toInt() + b[c1][c2].toInt()
    }

    fun evalRow(a1: Int, a2: Int, b1: Int, b2: Int, c1: Int, c2: Int, player: Byte): Int {
        val s = sum(a1, a2, b1, b2, c1, c2)
        if (s == 2 * player + AbstractRegularGame.NONE) return 10
        return if (s == 2 * otherPlayer(player) + AbstractRegularGame.NONE) -10 else 0
    }

    override fun evalState(player: Byte): Int {
        var result = (playerStones[player % 2] - playerStones[otherPlayer(player) % 2]) * 1000
        result = result + evalRow(0, 0, 0, 1, 0, 2, player)
        result = result + evalRow(0, 2, 0, 3, 0, 4, player)
        result = result + evalRow(0, 4, 0, 5, 0, 6, player)
        result = result + evalRow(0, 6, 0, 7, 0, 0, player)

        result = result + evalRow(1, 0, 1, 1, 1, 2, player)
        result = result + evalRow(1, 2, 1, 3, 1, 4, player)
        result = result + evalRow(1, 4, 1, 5, 1, 6, player)
        result = result + evalRow(1, 6, 1, 7, 1, 0, player)

        result = result + evalRow(2, 0, 2, 1, 2, 2, player)
        result = result + evalRow(2, 2, 2, 3, 2, 4, player)
        result = result + evalRow(2, 4, 2, 5, 2, 6, player)
        result = result + evalRow(2, 6, 2, 7, 2, 0, player)

        result = result + evalRow(0, 1, 1, 1, 2, 1, player)
        result = result + evalRow(0, 3, 1, 3, 2, 3, player)
        result = result + evalRow(0, 5, 1, 5, 2, 5, player)
        result = result + evalRow(0, 7, 1, 7, 2, 7, player)
        return result
    }

    override fun wins(player: Byte): Boolean {
        return playerStones[player % 2].toInt() == 2
    }

    override fun noMoreMove(): Boolean {
        return legalMoves().isEmpty()
    }

    override fun toString(): String {
        val result = "" //StringBuffer()
        for (r in rows - 1 downTo 0) {
            for (c in 0 until columns) result + ("" + b[c][r])
            result +("\n")
        }
        return result + ""
    }
}