import org.w3c.dom.*
import kotlin.browser.document
import kotlin.math.PI

interface  Game<M> {
    fun evalState(player:Byte): Int
    fun ended(): Boolean
    fun legalMoves(): List<M>
    fun interestingMoves(): List<M>
    fun doMove(m:M): Game<M>
    fun currentPlayer():Byte
    fun wins(player: Byte): Boolean
    fun noMoreMove(): Boolean
}

interface RegularGame<M> : Game<M> {
    val rows: Int
    val columns: Int
    val playerOne: Byte
    val playerTwo: Byte
    val playerNone: Byte
    fun getAtPosition(column: Byte, row: Byte): Byte
    fun createMove(column: Byte, row: Byte): M?
}

abstract class AbstractRegularGame<M>(override val columns: Int, override val rows: Int) : RegularGame<M> {
    override val playerOne: Byte = ONE
    override val playerTwo: Byte = TWO
    override val playerNone: Byte= NONE

    var player = ONE

    var b: Array<ByteArray> = Array<ByteArray>(columns, {_ -> ByteArray(rows)})

    var movesDone = 0

    var lastColumn: Byte = -1
    var lastRow: Byte = -1

    var winsLast: Boolean = false

    override fun currentPlayer(): Byte {
        return player
    }

    override fun interestingMoves(): List<M> {
        return legalMoves()
    }

    fun otherPlayer(p: Byte): Byte {
        return if (p == ONE) TWO else ONE
    }


    fun nextPlayer(): Byte {
        return otherPlayer(player)
    }

    fun lastPlayer(): Byte {
        return otherPlayer(player)
    }

    override fun getAtPosition(column: Byte, row: Byte): Byte {
        return b[column.toInt()][row.toInt()]
    }

    fun wins(): Boolean {
        winsLast = wins(lastPlayer())
        return winsLast
    }

    override fun ended(): Boolean {
        return noMoreMove() || wins()
    }

    companion object {
        val NONE: Byte = 0
        val ONE: Byte = 1
        val TWO: Byte = 2
    }
}