fun <M> negaMax(g: Game<M>, tiefe: Int, alpha: Int, beta: Int): Int {
    var a = alpha;
    if (tiefe == 0 || g.ended())
        return g.evalState(g.currentPlayer())

    for (i in g.interestingMoves()) {
        val wert = -negaMax(g.doMove(i), tiefe - 1, -beta, -a)

        if (wert >= beta) return beta

        if (wert > a) a = wert
    } // end of for

    return a
}

fun <M>bestMove(g: Game<M>, depth: Int): M? {
    var `val` = -Int.MAX_VALUE
    var result: M? = null
    for (m in g.interestingMoves()) {
        val s = g.doMove(m)
        val eval = -negaMax(s, depth, -Int.MAX_VALUE, -`val`)
        if (eval > `val`) {
            `val` = eval
            result = m
        } else if (result == null) result = m
    }
    return result
}

