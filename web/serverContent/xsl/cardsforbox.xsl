<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" indent="yes"  encoding="utf8"/>

<xsl:template match="cardBox">\documentclass[8pt,a6paper,oneside,landscape]{scrartcl}
\usepackage[margin=0.5cm]{geometry}

%\cardfrontstyle[\large\slshape]{headings}

%\cardbackstyle{empty}
%\usepackage{lmodern}
%\usepackage{soul}
\usepackage[utf8]{inputenc} 
\usepackage[ngerman]{babel}
%\usepackage[T1]{fontenc}
%\usepackage{lmodern}
%\usepackage{libertine}
\usepackage{microtype}
\usepackage{fontspec}
\usepackage[babel,german=guillemets]{csquotes}
\usepackage{amssymb}
\usepackage{unicode-math}

\usepackage{color }



\definecolor{pblue}{rgb}{0.13,0.13,1}
\definecolor{pgreen}{rgb}{0,0.5,0}
\definecolor{pred}{rgb}{0.9,0,0}
\definecolor{pgrey}{rgb}{0.46,0.45,0.48}

\usepackage{listings}

\lstnewenvironment{Java}
 {\lstset{language=Java,
  aboveskip=0pt,
  belowskip=0pt,
  texcl=true,
  frame=single,
  showspaces=false,
  showtabs=false,
  breaklines=true,
  showstringspaces=false,
  breakatwhitespace=true,
  commentstyle=\color{green},
  keywordstyle=\color{blue},
  stringstyle=\color{red},
  basicstyle=\ttfamily,
  moredelim=[il][\textcolor{grey}]{$$},
  moredelim=[is][\textcolor{grey}]{\%\%}{\%\%}
  }}
  {}

\lstnewenvironment{Haskell}
 {\lstset{language=Haskell,
  aboveskip=0pt,
  belowskip=0pt,
  texcl=true,
  frame=single,
  showspaces=false,
  showtabs=false,
  breaklines=true,
  showstringspaces=false,
  breakatwhitespace=true,
  commentstyle=\color{green},
  keywordstyle=\color{blue},
  stringstyle=\color{red},
  basicstyle=\ttfamily,
  moredelim=[il][\textcolor{grey}]{$$},
  moredelim=[is][\textcolor{grey}]{\%\%}{\%\%}
  }}
  {}

  

%\usepackage{fancyvrb}

\renewcommand{\familydefault}{\sfdefault}
%\usepackage[encapsulated]{CJK}
%\newcommand{\cntext}[1]{\begin{CJK}{UTF8}{cyberbit}#1\end{CJK}}

\setlength{\parindent}{0pt}
\setlength{\parskip}{0.5ex plus 0.5ex minus 0.2ex}
\usepackage{etoolbox}
\makeatletter
\preto{\@verbatim}{\topsep=0pt \partopsep=0pt }
\makeatother

\usepackage{tcolorbox}
\usepackage{minted}
\tcbuselibrary{minted}


\newenvironment{itemize*}%
{\begin{itemize}%
\setlength{\itemsep}{0pt}%
\setlength{\parskip}{0pt}}%
{\end{itemize}}


\definecolor{light-gray}{gray}{0.95}

\pagestyle{empty}

\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\begin{document}
\pagenumbering{roman}
\pagestyle{empty}


\begin{center}
\begin{minipage}[t]{\linewidth} 
\begin{tcolorbox}[height=8.9cm,colback=gray!5,colframe=red!40!black,title=Lernkartenkurs]

\begin{center}{\Huge <xsl:value-of select="./boxName"/>}
\end{center}

\vfill
\centerline{<xsl:value-of select="./author"/>}
~\\~\\~
\vfill

\begin{tcolorbox}[colback=gray!5,colframe=yellow!40!black,title=Zusammenfassung]
<xsl:apply-templates select="description"/>
\end{tcolorbox}
\end{tcolorbox} 
~\hfill {\footnotesize subato.org}
\end{minipage}
\end{center}
~\eject

\setcounter{page}{1}
\pagenumbering{arabic}



<xsl:apply-templates select="cards"/>
\end{document}
</xsl:template>



<xsl:template match="cards">
  <xsl:for-each select="card">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="pre[contains(./@class,'java')]">
\begin{tcblisting}{listing engine=minted,minted style=trac,minted language=Java,colback=red!5!white,colframe=red!75!black,listing only}
<xsl:apply-templates/>
\end{tcblisting}
</xsl:template>

<xsl:template match="pre[contains(./@class,'hs')]">
\begin{tcblisting}{listing engine=minted,minted style=trac,minted language=Haskell,colback=red!5!white,colframe=red!75!black,listing only}
<xsl:apply-templates/>
\end{tcblisting}
</xsl:template>



<xsl:template match="pre">
\begin{verbatim}<xsl:apply-templates/>\end{verbatim}
</xsl:template>

<xsl:template match="span">~~~\hspace{20mm}</xsl:template>

<xsl:template match="card[@type='Programming']" >

\begin{minipage}[t]{\linewidth} 
\begin{tcolorbox}[height=9cm,colback=gray!5,colframe=blue!40!black,title=Frage: <xsl:apply-templates select="name"/>]
  
<xsl:apply-templates select="exercise"/>


\eject
\begin{minipage}[t]{\linewidth} 
\begin{tcolorbox}[height=9cm,colback=gray!5,colframe=green!40!black,title=Lösung]
%\begin{<xsl:apply-templates select="exercise/lang"/>}
\begin{tcblisting}{listing engine=minted,minted style=trac,minted language=<xsl:apply-templates select="exercise/lang"/>,colback=red!5!white,colframe=red!75!black,listing only}
<xsl:apply-templates select="exercise/solution" />
\end{tcblisting}
%\end{<xsl:apply-templates select="exercise/lang"/>}

\tcblower

<xsl:apply-templates select="explanation"/>
\vfill
~
\end{tcolorbox}
~\hfill {\footnotesize subato.org}
\end{minipage}  

\eject
</xsl:template>

<xsl:template match="exercise" >

<xsl:apply-templates select="text"/>
%\begin{<xsl:apply-templates select="lang"/>}
\begin{tcblisting}{listing engine=minted,minted style=trac,minted language=<xsl:apply-templates select="lang"/>,colback=red!5!white,colframe=red!75!black,listing only}
<xsl:apply-templates select="solutionTemplate"/>
\end{tcblisting}
%\end{<xsl:apply-templates select="lang"/>}
\vfill
~
\end{tcolorbox} 
~\hfill {\footnotesize subato.org}
\end{minipage}  
</xsl:template>

<xsl:template match="topic[1]"><xsl:apply-templates /></xsl:template>
<xsl:template match="topic">, <xsl:apply-templates /></xsl:template>

<xsl:template match="card[@type='ClozeText' or @type='FreeText']"  >
</xsl:template>

<xsl:template match="card[@type='ValueList']" >
\begin{minipage}[t]{\linewidth} 
\begin{tcolorbox}[height=9cm,colback=gray!5,colframe=blue!40!black,title=Frage: <xsl:apply-templates select="name"/>]  
<xsl:apply-templates select="question"/>

<xsl:for-each select="answers/answer">

\colorbox{white!30}{\fbox{\color{white}AAA<xsl:value-of select="."/><xsl:value-of select="."/>}}
  </xsl:for-each>
\vfill
~
  \end{tcolorbox} 
~\hfill {\footnotesize subato.org}
  \end{minipage}   

\vfill\eject

\begin{minipage}[t]{\linewidth} 
\begin{tcolorbox}[height=9cm,colback=gray!5,colframe=green!40!black,title=Korrekte Antworten]
\begin{itemize*}

<xsl:for-each select="answers/answer[@correct]">
  <xsl:apply-templates select="." />
</xsl:for-each>
  \end{itemize*}
\tcblower

<xsl:apply-templates select="explanation"/>
~
\vfill

\end{tcolorbox} 
~\hfill {\footnotesize subato.org}

\end{minipage}  


\eject
</xsl:template>

<xsl:template match="testclasses">
<!--\subsubsection*{Testklassen}
<xsl:apply-templates />-->
</xsl:template>

<xsl:template match="class">
\paragraph*{<xsl:value-of select="@name"/>.<xsl:value-of select="@lang"/> }~
\begin{small}
\begin{lstlisting}
<xsl:apply-templates />
\end{lstlisting}
\end{small}
</xsl:template>

<xsl:template match="question">
  <xsl:apply-templates />
\tcblower
</xsl:template>

<xsl:template match="statistic">
</xsl:template>

<xsl:template match="card">
\begin{minipage}[t]{\linewidth} 
\begin{tcolorbox}[height=9cm,colback=gray!5,colframe=blue!40!black,title=Frage: <xsl:apply-templates select="name"/>]  
<xsl:apply-templates select="question"/>
  <xsl:text>

  </xsl:text>
  <xsl:choose>
  <xsl:when test="@type='MultipleChoice'">
{\em (Eine Antwortmöglichkeit.)}
  </xsl:when>
  <xsl:when test="@type='ValueSelection'">
{\em (Mehrere Antwortmöglichkeiten).}
  </xsl:when>
</xsl:choose> 

\begin{itemize*}
  <xsl:for-each select="answers/answer">
     <xsl:apply-templates select="."/>
  </xsl:for-each>
  \end{itemize*}
  \end{tcolorbox}
~\hfill {\footnotesize subato.org}
\end{minipage}  
\vfill\eject

\begin{minipage}[t]{\linewidth} 
\begin{tcolorbox}[height=9cm,colback=gray!5,colframe=green!40!black,title=Korrekte Antworten]
\begin{itemize*}
  <xsl:for-each select="answers/answer[@correct]">
 <xsl:apply-templates select="." />
  </xsl:for-each>
  \end{itemize*}


\tcblower  
<xsl:apply-templates select="explanation"/>
\end{tcolorbox}
~\hfill {\footnotesize subato.org}

\end{minipage}  

\eject
</xsl:template>

<xsl:template match="explanation">
\subsubsection*{Erläuterung}
<xsl:apply-templates/>


</xsl:template>

<xsl:template match="answer">

\item <xsl:apply-templates/>

</xsl:template>


<xsl:template match="comment">
  \paragraph*{Kommentar}
  <xsl:apply-templates/>
</xsl:template>



<xsl:template match="tt">\verb^<xsl:apply-templates/>^</xsl:template>

<xsl:template match="em">{\em <xsl:apply-templates/>}</xsl:template>

<xsl:template match="b">{\bf <xsl:apply-templates/>}</xsl:template>

<xsl:template match="ul">
  \begin{itemize*}
   <xsl:apply-templates/>
  \end{itemize*}
</xsl:template>

<xsl:template match="li">
  \item    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="code">
\begin{Verbatim}[frame=single]
<xsl:apply-templates/>
\end{Verbatim}
</xsl:template>
<xsl:template match="br">\\[0.01em]</xsl:template>
<xsl:template match="notwin">\section{}~\eject
</xsl:template>

<xsl:template match="eject">~\eject
</xsl:template>

<xsl:template match="variableBindings">
</xsl:template>

<xsl:template match="var">
  <xsl:variable name="name" select="./@name"/>
  \item $<xsl:value-of select="./@min"/> \le <xsl:value-of select="./@name"/> \le <xsl:value-of select="./@max"/> $ mit $ <xsl:value-of select="./@name"/> = <xsl:value-of select="ancestor::card/env/entry[@k = $name]/@v"/>$
</xsl:template>

<xsl:template match="result">
  \item $<xsl:value-of select="./@name"/> = $ \verb^<xsl:value-of select="./@definition"/>^
</xsl:template>

<xsl:template match="v"><xsl:variable name="name" select="./@name"/><xsl:value-of select="ancestor::card/env/entry[@k = $name]/@v"/></xsl:template>

<xsl:template match="div">
<xsl:apply-templates/>

</xsl:template>

<xsl:template match="env">
  Beispielbelegung: <xsl:apply-templates/>
    
</xsl:template>

<xsl:template match="entry[1]"> $<xsl:value-of select="@k"/> = <xsl:value-of select="@v"/>$</xsl:template>

<xsl:template match="entry">, $<xsl:value-of select="@k"/> = <xsl:value-of select="@v"/>$</xsl:template>
</xsl:stylesheet>
